<?php

namespace console\actions\cron;

use common\models\Experiment;
use Yii;
use yii\base\Action;

/**
 * Set status collecting for expired experiments
 *
 * Class CollectingExpiredExperimentsAction
 */
class CollectingExpiredExperimentsAction extends Action
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $expiredExperiment = Experiment::find()
            ->where(['status' => [Experiment::STATUS_RUNNING]])
            ->andWhere(['<', 'finish_time', date('Y-m-d H:i:s')]);

        foreach ($expiredExperiment->each(100) as $experiment) {
            /** @var Experiment $experiment */
            $experiment->status = Experiment::STATUS_COLLECTING;

            if (!$experiment->save()) {
                Yii::warning(
                    'Experiment has not collecting. Errors: ' . json_encode($experiment->getErrors()),
                    'console.cron.CollectingExpiredExperimentsAction'
                );
            }
        }
    }
}
