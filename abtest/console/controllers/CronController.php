<?php

namespace console\controllers;

use console\actions\cron\CollectingExpiredExperimentsAction;
use yii\console\Controller;

/**
 * Commands for cron
 *
 * Class CronController
 */
class CronController extends Controller
{
    /**
     * @return array|string[]
     */
    public function actions(): array
    {
        return [
            'collecting-expired-experiments' => CollectingExpiredExperimentsAction::class,
        ];
    }
}
