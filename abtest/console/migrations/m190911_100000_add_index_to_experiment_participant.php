<?php

use yii\db\Migration;

class m190911_100000_add_index_to_experiment_participant extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            '
                CREATE INDEX experiment_participant_experiment_id_variant_index
	            ON experiment_participant (experiment_id, variant);
        '
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP INDEX experiment_participant_experiment_id_variant_index ON experiment_participant;');
    }
}
