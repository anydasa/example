<?php

use yii\db\Migration;

class m191103_100000_remove_view_experiment_participant extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('DROP VIEW experiment_participant_view;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('
                CREATE VIEW experiment_participant_view AS
                SELECT `ep`.*,
                       MAX(participant_contact.time) last_contact,
                       count(*) count
                FROM `experiment_participant` `ep`
                         LEFT JOIN `participant_contact` ON `ep`.`id` = `participant_contact`.`experiment_participant_id`
                GROUP BY ep.id;
        ');
    }
}
