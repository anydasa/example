<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project}}`.
 */
class m200317_153803_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->execute('
               CREATE TABLE project ( 
                id     TINYINT(1) UNSIGNED NOT NULL PRIMARY KEY,
                code   VARCHAR(2) NOT NULL,
                name   VARCHAR(32)  NULL
            ) COLLATE = utf8_unicode_ci ENGINE=InnoDB;
        ');

        $this->execute("INSERT INTO project (`id`, `code`,`name`) 
                VALUES 
                (1, 'vp', 'Vulkan Platinum'),
                (2, 'vr', 'Vulkan Russia'),
                (3, 'sc', 'Spin City'),
                (4, 'mb', 'Mr.Bet'),
                (5, 'db', 'Dr.Bet'),
                (6, 'cb', 'Captain Bet')
        ");

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->execute('DROP TABLE project');

        return true;
    }
}