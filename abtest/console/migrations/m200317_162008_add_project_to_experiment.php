<?php

use ys\migration\db\Migration;

/**
 * Class m200317_162008_add_project_to_experiment
 */
class m200317_162008_add_project_to_experiment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->posChange(
            'experiment',
            'ADD project_id TINYINT(1) UNSIGNED
                 AFTER experiment_category_id;'
        );

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->posChange('experiment', 'DROP COLUMN project_id');

        return true;
    }
}
