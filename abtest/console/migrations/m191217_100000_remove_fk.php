<?php

use yii\db\Migration;

class m191217_100000_remove_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('experiment_experiment_category_id_fk', 'experiment');
        $this->dropForeignKey('experiment_participant_experiment_id_fk', 'experiment_participant');
        $this->dropForeignKey('participant_contact_experiment_participant_id_fk', 'participant_contact');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
