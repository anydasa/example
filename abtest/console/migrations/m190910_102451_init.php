<?php

use yii\db\Migration;

/**
 * Class m190731_102451_init
 */
class m190910_102451_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE experiment_category
            (
                id   BIGINT UNSIGNED NOT NULL PRIMARY KEY,
                name VARCHAR(255)    NOT NULL,
                CONSTRAINT experiment_category_name_uindex UNIQUE (name)
            ) COLLATE = utf8_unicode_ci ENGINE=InnoDB;
        ');

        $this->execute('
            CREATE TABLE experiment
            (
                id                      BIGINT UNSIGNED NOT NULL PRIMARY KEY,
                experiment_category_id  BIGINT UNSIGNED NOT NULL,
                code                    VARCHAR(255)    NOT NULL,
                name                    VARCHAR(255)    NULL,
                description             TEXT            NULL,
                jira_issue              VARCHAR(255)    NULL,
                start_time              DATETIME        NULL,
                finish_time             DATETIME        NULL,
                participant_count_limit INT UNSIGNED    NULL,
                allow_add_participant   TINYINT         NOT NULL,
                status                  TINYINT         NOT NULL,
                variants                TEXT            NOT NULL,
                CONSTRAINT experiment_experiment_category_id_code_uindex UNIQUE (experiment_category_id, code),
                CONSTRAINT experiment_experiment_category_id_fk FOREIGN KEY (experiment_category_id) REFERENCES experiment_category (id)
            ) COLLATE = utf8_unicode_ci ENGINE=InnoDB;
        ');

        $this->execute('
            CREATE TABLE experiment_participant
            (
                id             BIGINT UNSIGNED                    NOT NULL PRIMARY KEY,
                experiment_id  BIGINT UNSIGNED                    NOT NULL,
                participant_id VARCHAR(32)                        NULL,
                user_id        BIGINT UNSIGNED                    NULL,
                time           DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                variant        VARCHAR(32)                        NOT NULL,
                CONSTRAINT experiment_participant_experiment_id_participant_id_uindex UNIQUE (experiment_id, participant_id),
                CONSTRAINT experiment_participant_experiment_id_user_id_uindex UNIQUE (experiment_id, user_id),
                CONSTRAINT experiment_participant_experiment_id_fk FOREIGN KEY (experiment_id) REFERENCES experiment (id)
            ) COLLATE = utf8_unicode_ci ENGINE=InnoDB;
        ');

        $this->execute('
            CREATE TABLE participant_contact
            (
                experiment_participant_id BIGINT UNSIGNED                     NOT NULL,
                time                      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                CONSTRAINT participant_contact_experiment_participant_id_fk FOREIGN KEY (experiment_participant_id) REFERENCES experiment_participant (id)
            ) ENGINE=InnoDB;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP TABLE participant_contact');
        $this->execute('DROP TABLE experiment_participant');
        $this->execute('DROP TABLE experiment');
        $this->execute('DROP TABLE experiment_category');

        return false;
    }
}
