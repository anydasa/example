<?php

use yii\db\Migration;

class m191003_100000_add_view_experiment_participant extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
                CREATE INDEX participant_time_index 
                ON participant_contact (experiment_participant_id, time);
        ');

        $this->execute('
                CREATE VIEW experiment_participant_view AS
                SELECT `ep`.*,
                       MAX(participant_contact.time) last_contact,
                       count(*) count
                FROM `experiment_participant` `ep`
                         LEFT JOIN `participant_contact` ON `ep`.`id` = `participant_contact`.`experiment_participant_id`
                GROUP BY ep.id;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP INDEX participant_time_index ON experiment_participant;');
        $this->execute('DROP VIEW experiment_participant_view;');
    }
}
