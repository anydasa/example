<?php

declare(strict_types=1);

use yii\log\FileTarget;
use yii\web\JsonParser;

$params = array_merge(
    require __DIR__.'/../../common/config/params.php',
    require __DIR__.'/../../common/config/params-local.php',
    require __DIR__.'/params.php',
    require __DIR__.'/params-local.php'
);

$routes = require __DIR__.'/routes.php';

return [
    'id' => 'app-api',
//    'on '.Application::EVENT_BEFORE_REQUEST => [ApiTokenChecker::class, 'check'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'ys-mp'],
    'controllerNamespace' => 'api\http\controllers',
    'modules' => [
        'ys-mp' => [
            'class' => ys\mp\Module::class,
        ],
    ],
    'components' => [
        'request' => [
            'baseUrl' => '/',
            'enableCsrfValidation' => false,
            'enableCookieValidation' => false,
            'enableCsrfCookie' => false,
            'csrfParam' => null,
            'parsers' => [
                'application/json' => JsonParser::class,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $routes,
        ],
    ],
    'params' => $params,
];
