<?php

declare(strict_types=1);

return [
    '/docs' => 'open-api/ui',
    '/api/resource.json' => 'open-api/index',

    ['pattern' => 'api/v1/experiment-category/list', 'route' => 'v1/experiment-category/list', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment-category/item', 'route' => 'v1/experiment-category/item', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment-category/create', 'route' => 'v1/experiment-category/create', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment-category/delete', 'route' => 'v1/experiment-category/delete', 'verb' => 'POST'],

    ['pattern' => 'api/v1/experiment/list', 'route' => 'v1/experiment/list', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/status-list', 'route' => 'v1/experiment/status-list', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/item', 'route' => 'v1/experiment/item', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/create', 'route' => 'v1/experiment/create', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/update', 'route' => 'v1/experiment/update', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/delete', 'route' => 'v1/experiment/delete', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/enable', 'route' => 'v1/experiment/enable', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/disable', 'route' => 'v1/experiment/disable', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/add-participant-list', 'route' => 'v1/experiment/add-participant-list', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/participant', 'route' => 'v1/experiment/participant', 'verb' => 'POST'],
    ['pattern' => 'api/v1/experiment/contact', 'route' => 'v1/experiment/contact', 'verb' => 'POST'],

    ['pattern' => 'api/v1/report/experiment', 'route' => 'v1/report/experiment', 'verb' => 'POST'],
    ['pattern' => 'api/v1/report/experiment/participant', 'route' => 'v1/report/experiment-participant', 'verb' => 'POST'],

    ['pattern' => 'api/v1/project/list', 'route' => 'v1/project/list', 'verb' => 'POST'],

    ['pattern' => 'api/v1/config/get', 'route' => 'v1/config/get', 'verb' => 'POST'],
];
