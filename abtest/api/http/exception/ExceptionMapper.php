<?php

declare(strict_types=1);

namespace api\http\exception;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\InvalidAttributeException;
use api\http\response\ErrorResponse;
use Throwable;

/**
 * Class ExceptionMapper.
 */
class ExceptionMapper
{
    /**
     * @param Throwable $exception
     *
     * @return ErrorResponse
     */
    public static function resolveExceptionResponse(Throwable $exception): ErrorResponse
    {
        $response = new ErrorResponse();

        if ($exception instanceof InvalidAttributeException) {
            foreach ($exception->getErrors() as $error) {
                $response->addError($exception->getMessage(), null, $error);
            }
        } elseif ($exception instanceof ApplicationException) {
            $response->addError($exception->getMessage(), $exception->getCode());
        } else {
            $response->addError('Unknown error', 500);
        }

        return $response;
    }
}
