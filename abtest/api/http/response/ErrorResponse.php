<?php

declare(strict_types=1);

namespace api\http\response;

class ErrorResponse extends BaseResponse
{
    public function addError($title, $status = null, $detail = null): void
    {
        $errors = $this->data['errors'];

        $errors[] = array_filter([
            'status' => $status,
            'title' => $title,
            'detail' => $detail,
        ]);

        $this->data = [
            'errors' => $errors,
        ];
    }
}
