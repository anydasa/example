<?php

declare(strict_types=1);

namespace api\http\response;

/**
 * Class SuccessResponse.
 */
class SuccessResponse extends BaseResponse
{
    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data['data'] = $data;
    }

    /**
     * @param array $meta
     */
    public function setMeta(array $meta): void
    {
        $this->data['meta'] = $meta;
    }
}
