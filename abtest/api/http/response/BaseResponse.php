<?php

declare(strict_types=1);

namespace api\http\response;

use yii\web\Response;

abstract class BaseResponse extends Response
{
    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->format = Response::FORMAT_JSON;
        $this->headers->add('Content-Type', 'application/vnd.api+json');
    }
}
