<?php

declare(strict_types=1);

namespace api\http\behavior;

use yii\base\Behavior;
use yii\web\UnauthorizedHttpException;

class ApiTokenChecker extends Behavior
{
    public static function check(): void
    {
        $request = \Yii::$app->getRequest();

        if (!preg_match('/^\/api\/v1/', $request->getUrl())) {
            return;
        }

        if ('user' !== $request->getAuthUser()) {
            throw new UnauthorizedHttpException();
        }

        if ('pass' !== $request->getAuthPassword()) {
            throw new UnauthorizedHttpException();
        }
    }
}
