<?php

declare(strict_types=1);

namespace api\http\request;

use yii\base\Model;

abstract class BaseRequest extends Model
{
    /**
     * BaseRequest constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct();

        $this->load($data, '');
    }
}
