<?php

declare(strict_types=1);

namespace api\http\request;

use yii\web\Request;

class RequestFactory
{
    public static function make(Request $request, string $requestClass)
    {
        if (!is_subclass_of($requestClass, BaseRequest::class)) {
            throw new \RuntimeException('BaseRequest::class');
        }

        if ($request->getIsPost() || $request->getIsDelete() || $request->getIsPut()) {
            return new $requestClass($request->post());
        }

        if ($request->getIsGet()) {
            return new $requestClass($request->get());
        }

        throw new \RuntimeException('Err');
    }
}
