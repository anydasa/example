<?php

declare(strict_types=1);

namespace api\http\controllers;

use api\application\Common\Exception\ApplicationException;
use api\http\exception\ExceptionMapper;
use Yii;
use yii\log\Logger;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ErrorController.
 */
class ErrorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actionIndex(): Response
    {
        $exception = Yii::$app->errorHandler->exception;

        if (YII_DEBUG && !$exception instanceof ApplicationException) {
            throw $exception;
        }

        Yii::getLogger()->log($exception->getMessage(), Logger::LEVEL_ERROR);

        return ExceptionMapper::resolveExceptionResponse($exception);
    }
}
