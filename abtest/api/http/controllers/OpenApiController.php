<?php

declare(strict_types=1);

namespace api\http\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class OpenApiController extends Controller
{
    private const AUTH_USER = 'abtest';
    private const AUTH_PASS = 'pass';



    public $layout = false;

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $openApi = \OpenApi\scan(Yii::getAlias('@api'));

        return $openApi->toJson();
    }

    /**
     * @return string
     */
    public function actionUi()
    {
        
        if (!$this->isAuthorized()) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            exit;
        }

        return $this->render('index', [
            'restUrl' => Url::to('/api/resource.json'),
        ]);
    }

    private function isAuthorized(): bool
    {
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return false;
        }

        if (self::AUTH_USER === $_SERVER['PHP_AUTH_USER'] || self::AUTH_PASS === $_SERVER['PHP_AUTH_PW']) {
            return true;
        }

        return false;
    }
}
