<?php

declare(strict_types=1);

namespace api\http\controllers\v1;

use yii\web\Controller as BaseController;

/**
 * Class ExperimentController.
 */
class ExperimentController extends BaseController
{
    public function actions()
    {
        return [
            'create' => Experiment\CreateAction::class,
            'update' => Experiment\EditAction::class,
            'list' => Experiment\ListAction::class,
            'status-list' => Experiment\StatusListAction::class,
            'item' => Experiment\ItemAction::class,
            'delete' => Experiment\DeleteAction::class,
            'enable' => Experiment\EnableAction::class,
            'disable' => Experiment\DisableAction::class,
            'participant' => Experiment\ParticipantAction::class,
            'add-participant-list' => Experiment\AddParticipantListAction::class,
            'contact' => Experiment\ContactAction::class,
        ];
    }
}
