<?php

declare(strict_types=1);

namespace api\http\controllers\v1\ExperimentCategory;

use api\application\Common\Exception\InvalidAttributeException;
use api\application\ExperimentCategory\Query\ExperimentCategoryListHandler;
use api\application\ExperimentCategory\Query\ExperimentCategoryListQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment-category/list",
 *     tags={"Experiment Category"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={}
 *          )
 *     ),
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class ListAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ExperimentCategoryListQuery($data);
        $handler = new ExperimentCategoryListHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse(
            [
                'list' => $result->getList(),
            ],
            [
                'totalCount' => $result->getTotalCount(),
                'count' => $result->getCount(),
                'page' => $result->getPage(),
            ]
        );
    }
}
