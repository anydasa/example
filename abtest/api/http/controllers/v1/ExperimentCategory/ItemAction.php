<?php

declare(strict_types=1);

namespace api\http\controllers\v1\ExperimentCategory;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\ExperimentCategory\Query\ExperimentCategoryItemHandler;
use api\application\ExperimentCategory\Query\ExperimentCategoryItemQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment-category/item",
 *     tags={"Experiment Category"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={"id": 123456}
 *          )
 *     ),
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class ItemAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws ApplicationException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ExperimentCategoryItemQuery($data);
        $handler = new ExperimentCategoryItemHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse([
            'id' => $result->getId(),
            'name' => $result->getName(),
        ]);
    }
}
