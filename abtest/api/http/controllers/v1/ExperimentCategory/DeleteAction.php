<?php

declare(strict_types=1);

namespace api\http\controllers\v1\ExperimentCategory;

use api\application\Common\Exception\EntityDeleteFailedException;
use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\ExperimentCategory\Command\DeleteExperimentCategoryCommand;
use api\application\ExperimentCategory\Command\DeleteExperimentCategoryHandler;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment-category/delete",
 *     tags={"Experiment Category"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class DeleteAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     * @throws EntityDeleteFailedException
     * @throws EntityNotFoundException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();

        $command = new DeleteExperimentCategoryCommand($commandData);
        $handler = new DeleteExperimentCategoryHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse();
    }
}
