<?php

declare(strict_types=1);

namespace api\http\controllers\v1\ExperimentCategory;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\ExperimentCategory\Command\CreateExperimentCategoryCommand;
use api\application\ExperimentCategory\Command\CreateExperimentCategoryHandler;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment-category/create",
 *     tags={"Experiment Category"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              ref="#/components/schemas/CreateCategory"
 *          )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Created"
 *    ),
 *    @OA\Response(
 *         response="400",
 *         description="Error"
 *    )
 * )
 * @OA\Schema(
 *     schema="CreateCategory",
 *     required={"name"},
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         example="Category 1"
 *   ),
 * )
 */
class CreateAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     * @throws ApplicationException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();
        $commandData['id'] = $commandData['id'] ?? $this->getUid();

        $command = new CreateExperimentCategoryCommand($commandData);
        $handler = new CreateExperimentCategoryHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse(
            [
                'id' => $commandData['id'],
            ]
        );
    }
}
