<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Report;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Report\Query\ReportExperimentHandler;
use api\application\Report\Query\ReportExperimentQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/report/experiment",
 *     tags={"Report"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={}
 *          )
 *     ),
 *     @OA\Response(
 *        response="default",
 *        description="This API call has no documentated response (yet)"
 *     )
 * )
 */
class ExperimentAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     * @throws EntityNotFoundException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $handler = new ReportExperimentHandler();
        $result = $handler->handle(new ReportExperimentQuery($data));

        return $this->getJsonSuccessResponse($result->toArray());
    }
}
