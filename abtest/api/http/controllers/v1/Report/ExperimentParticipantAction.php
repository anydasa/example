<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Report;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Report\Query\ReportExperimentParticipant\Handler;
use api\application\Report\Query\ReportExperimentParticipant\Query;
use OpenApi\Annotations as OA;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/report/experiment/participant",
 *     tags={"Report"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={}
 *          )
 *     ),
 *     @OA\Response(
 *        response="default",
 *        description="This API call has no documentated response (yet)"
 *     )
 * )
 */
class ExperimentParticipantAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     * @throws ApplicationException
     * @throws InvalidConfigException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $handler = new Handler();
        $result = $handler->handle(new Query($data));

        $response = Yii::createObject([
            'class' => Response::class,
            'format' => Response::FORMAT_RAW,
            'stream' => $result->toCsvStream(),
        ]);

        $response->getHeaders()->set('Content-Type', 'text/csv');

        return $response;
    }
}
