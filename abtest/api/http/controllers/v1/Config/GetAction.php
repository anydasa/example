<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Config;

use api\application\Common\Exception\InvalidAttributeException;
use api\application\Config\Query\ConfigHandler;
use api\application\Config\Query\ConfigQuery;
use api\http\response\SuccessResponse;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/config/get",
 *     tags={"Experiment"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={}
 *          )
 *     ),
 *     @OA\Response(
 *        response="default",
 *        description="This API call has no documentated response (yet)"
 *     )
 * )
 */
class GetAction extends BaseAction
{
    /**
     * @return SuccessResponse
     *
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ConfigQuery($data);
        $handler = new ConfigHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse($result->getData());
    }
}
