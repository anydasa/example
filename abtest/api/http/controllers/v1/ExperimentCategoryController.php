<?php

declare(strict_types=1);

namespace api\http\controllers\v1;

use yii\web\Controller as BaseController;

/**
 * Class ExperimentCategoryController.
 */
class ExperimentCategoryController extends BaseController
{
    public function actions()
    {
        return [
            'create' => ExperimentCategory\CreateAction::class,
            'list' => ExperimentCategory\ListAction::class,
            'item' => ExperimentCategory\ItemAction::class,
            'delete' => ExperimentCategory\DeleteAction::class,
        ];
    }
}
