<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\EnableExperimentCommand;
use api\application\Experiment\Command\EnableExperimentHandler;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/enable",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class EnableAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();

        $command = new EnableExperimentCommand($commandData);
        $handler = new EnableExperimentHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse();
    }
}
