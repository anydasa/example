<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\http\response\SuccessResponse;
use common\models\Experiment;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/status-list",
 *     tags={"Experiment"},
 *     @OA\Response(
 *        response="default",
 *        description="This API call has no documentated response (yet)"
 *     )
 * )
 */
class StatusListAction extends BaseAction
{
    /**
     * @return SuccessResponse
     */
    public function run(): Response
    {
        return $this->getJsonSuccessResponse(
            [
                'list' => Experiment::STATUS_NAMES,
            ]
        );
    }
}
