<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\CreateExperimentCommand;
use api\application\Experiment\Command\CreateExperimentHandler;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/create",
 *     tags={"Experiment"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={
 *                  "category_id": 3515906048316741120,
 *                  "name": "Test 1",
 *                  "code": "test1",
 *                  "allow_add_participant": true
 *              }
 *          )
 *     ),
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class CreateAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     * @throws ApplicationException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();
        $command = new CreateExperimentCommand($commandData);

        $handler = new CreateExperimentHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse([
            'id' => $command->id
        ]);
    }
}
