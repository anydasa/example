<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\DisableExperimentCommand;
use api\application\Experiment\Command\DisableExperimentHandler;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/disable",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class DisableAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();

        $command = new DisableExperimentCommand($commandData);
        $handler = new DisableExperimentHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse();
    }
}
