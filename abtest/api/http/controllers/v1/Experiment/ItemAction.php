<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Query\ExperimentItemHandler;
use api\application\Experiment\Query\ExperimentItemQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/item",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class ItemAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ExperimentItemQuery($data);
        $handler = new ExperimentItemHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse($result->toArray());
    }
}
