<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Query\ExperimentListHandler;
use api\application\Experiment\Query\ExperimentListQuery;
use api\http\response\SuccessResponse;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/list",
 *     tags={"Experiment"},
 *     @OA\Response(
 *        response="default",
 *        description="This API call has no documentated response (yet)"
 *     )
 * )
 */
class ListAction extends BaseAction
{
    /**
     * @return SuccessResponse
     *
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ExperimentListQuery($data);
        $handler = new ExperimentListHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse(
            [
                'list' => $result->getList(),
            ],
            [
                'totalCount' => $result->getTotalCount(),
                'count' => $result->getCount(),
                'page' => $result->getPage(),
            ]
        );
    }
}
