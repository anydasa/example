<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\EditExperimentCommand;
use api\application\Experiment\Command\EditExperimentHandler;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/update",
 *     tags={"Experiment"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={
 *                  "id": 3834804020831561729,
 *                  "category_id": 3834803522207890688,
 *                  "name": "test11",
 *                  "project_code": "sc",
 *                  "start_time": "2020-03-03T00:00:01+00:00",
 *                  "finish_time": "2020-04-03T00:00:01+00:00"
 *              }
 *          )
 *     ),
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class EditAction extends BaseAction
{
    /**
     * @return Response
     * @throws InvalidAttributeException
     * @throws ApplicationException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();
        $command = new EditExperimentCommand($commandData);

        $handler = new EditExperimentHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse([
            'result' => $result,
        ]);
    }
}
