<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\AddParticipantListCommand as Command;
use api\application\Experiment\Command\AddParticipantListHandler as CommandHandler;
use api\application\Experiment\Query\AddParticipantList\Handler as QueryHandler;
use api\application\Experiment\Query\AddParticipantList\Query;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/add-participant-list",
 *     tags={"Experiment"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={
 *                  "experiment_id": 1,
 *                  "user_ids":{1,2,3}
 *              }
 *          )
 *     ),
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class AddParticipantListAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws InvalidAttributeException
     * @throws InvalidConfigException
     * @throws EntityNotFoundException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent() ?? [];

        $command = new Command($data);
        $commandHandler = new CommandHandler();
        $commandHandler->handle($command);

        $query = new Query($data);
        $queryHandler = new QueryHandler();
        $result = $queryHandler->handle($query);

        /** @var Response $response */
        $response = Yii::createObject([
            'class' => Response::class,
            'format' => Response::FORMAT_RAW,
            'stream' => $result->toCsvStream(),
        ]);

        $response->getHeaders()->set('Content-Type', 'text/csv');

        return $response;
    }
}
