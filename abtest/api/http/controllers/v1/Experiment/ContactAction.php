<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\ContactCommand;
use api\application\Experiment\Command\ContactHandler;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/cotnact",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class ContactAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     * @throws ApplicationException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ContactCommand($data);
        $handler = new ContactHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse([]);
    }
}
