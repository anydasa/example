<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityDeleteFailedException;
use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Command\DeleteExperimentCommand;
use api\application\Experiment\Command\DeleteExperimentHandler;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/delete",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class DeleteAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityDeleteFailedException
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $commandData = $this->getJsonRequestContent();

        $command = new DeleteExperimentCommand($commandData);
        $handler = new DeleteExperimentHandler();
        $handler->handle($command);

        return $this->getJsonSuccessResponse();
    }
}
