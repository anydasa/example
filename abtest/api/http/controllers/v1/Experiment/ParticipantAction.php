<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Experiment;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Common\Exception\InvalidAttributeException;
use api\application\Experiment\Query\ParticipantHandler;
use api\application\Experiment\Query\ParticipantQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * @OA\Post(
 *     path="/api/v1/experiment/participant",
 *     tags={"Experiment"},
 *     @OA\Response(
 *       response="default",
 *       description="This API call has no documentated response (yet)"
 *    )
 * )
 */
class ParticipantAction extends BaseAction
{
    /**
     * @return Response
     *
     * @throws EntityNotFoundException
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ParticipantQuery($data);
        $handler = new ParticipantHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse($result ? $result->toArray() : null);
    }
}
