<?php

declare(strict_types=1);

namespace api\http\controllers\v1;

use yii\web\Controller as BaseController;

/**
 * Class ConfigController.
 */
class ConfigController extends BaseController
{
    public function actions()
    {
        return [
            'get' => Config\GetAction::class,
        ];
    }
}
