<?php

declare(strict_types=1);

namespace api\http\controllers\v1;

use yii\web\Controller as BaseController;

class ReportController extends BaseController
{
    public function actions()
    {
        return [
            'experiment' => Report\ExperimentAction::class,
            'experiment-participant' => Report\ExperimentParticipantAction::class,
        ];
    }
}
