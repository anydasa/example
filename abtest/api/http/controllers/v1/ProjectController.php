<?php

declare(strict_types=1);

namespace api\http\controllers\v1;

use yii\web\Controller as BaseController;

/**
 * Class ProjectController
 */
class ProjectController extends BaseController
{
    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'list' => Project\ListAction::class,
        ];
    }
}
