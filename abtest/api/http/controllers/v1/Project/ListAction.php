<?php

declare(strict_types=1);

namespace api\http\controllers\v1\Project;

use api\application\Common\Exception\InvalidAttributeException;
use api\application\Project\Query\ProjectListHandler;
use api\application\Project\Query\ProjectListQuery;
use OpenApi\Annotations as OA;
use yii\web\Response;
use ys\mp\web\Action\BaseAction;

/**
 * Class ListAction
 */

/**
 * @OA\Post(
 *     path="/api/v1/project/list",
 *     tags={"Project"},
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              example={
 *              "page":1,
 *              "onPage":100,
 *               "filter":{}
 *             }
 *          )
 *     ),
 *     @OA\Response(
 *          response="default",
 *          description="Get list of projects",
 *          @OA\JsonContent(
 *              example={
 *                   "meta": {
 *                      "totalCount": 6,
 *                      "count": 6,
 *                      "page": 1
 *                   },
 *                   "data": {
 *                       "list": {
 *                          {
 *                              "id": 6,
 *                              "code": "cb",
 *                              "name": "Captain Bet"
 *                          },
 *                          {
 *                              "id": 5,
 *                              "code": "db",
 *                              "name": "Dr.Bet"
 *                          }
 *                       }
 *                  }
 *             }
 *       )
 *    )
 * )
 */
class ListAction extends BaseAction
{
    /**
     * @return Response
     * @throws InvalidAttributeException
     */
    public function run(): Response
    {
        $data = $this->getJsonRequestContent();

        $command = new ProjectListQuery($data);
        $handler = new ProjectListHandler();
        $result = $handler->handle($command);

        return $this->getJsonSuccessResponse(
            [
                'list' => $result->getList(),
            ],
            [
                'totalCount' => $result->getTotalCount(),
                'count' => $result->getCount(),
                'page' => $result->getPage(),
            ]
        );
    }
}
