<?php

declare(strict_types=1);

namespace api\http\controllers;

use yii\web\Controller;

/**
 * @OA\Info(title="YS AB Test API", version="0.1")
 */
class SiteController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return '<h1>Ab test</h1> <a href="/docs">open api doc</a>';
    }
}
