<?php

declare(strict_types=1);

namespace api\application\Common\Command;

use api\application\Common\Exception\InvalidAttributeException;
use yii\base\Model;

abstract class BaseCommand extends Model
{
    /**
     * BaseCommand constructor.
     *
     * @param array $data
     *
     * @throws InvalidAttributeException
     */
    public function __construct(array $data)
    {
        parent::__construct();

        $this->load($data, '');

        if (!$this->validate()) {
            throw new InvalidAttributeException($this->getErrors());
        }
    }
}
