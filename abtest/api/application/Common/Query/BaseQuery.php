<?php

declare(strict_types=1);

namespace api\application\Common\Query;

use api\application\Common\Exception\InvalidAttributeException;
use yii\base\Model;

/**
 * Class BaseQuery.
 */
class BaseQuery extends Model
{
    /**
     * BaseQuery constructor.
     *
     * @param array $data
     *
     * @throws InvalidAttributeException
     */
    public function __construct(array $data)
    {
        parent::__construct();

        $this->load($data, '');

        if (!$this->validate()) {
            throw new InvalidAttributeException($this->getErrors());
        }
    }
}
