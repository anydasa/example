<?php

declare(strict_types=1);

namespace api\application\Common\Exception;

class ApplicationException extends \Exception
{
}
