<?php

declare(strict_types=1);

namespace api\application\Common\Exception;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class InvalidAttributeException extends ApplicationException
{
    /**
     * @var array
     */
    private $errors;

    /**
     * ValidationRequestHttpException constructor.
     *
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct('Invalid Attribute', 422);

        $this->errors = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($errors)));
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
