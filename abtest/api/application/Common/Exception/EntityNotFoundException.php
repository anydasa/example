<?php

declare(strict_types=1);

namespace api\application\Common\Exception;

class EntityNotFoundException extends ApplicationException
{
    public function __construct($message)
    {
        parent::__construct($message, 404);
    }
}
