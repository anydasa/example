<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Query;

use api\application\Common\Query\BaseQuery;

class ExperimentCategoryItemQuery extends BaseQuery
{
    /**
     * @var int
     */
    public $id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
