<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Query;

use common\models\ExperimentCategory;

class ExperimentCategoryListHandler
{
    public function handle(ExperimentCategoryListQuery $query): ExperimentCategoryListResult
    {
        $dbQuery = ExperimentCategory::find();
        $dbQuery->limit($query->onPage);
        $dbQuery->offset($query->page * $query->onPage - $query->onPage);
        $dbQuery->orderBy('id DESC');

        $result = $dbQuery->all();
        $totalCount = (int) $dbQuery->count();

        $res = new ExperimentCategoryListResult($totalCount, $query->page);
        $res->setList($result);

        return $res;
    }
}
