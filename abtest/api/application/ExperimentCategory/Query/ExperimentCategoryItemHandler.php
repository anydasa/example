<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Query;

use api\application\Common\Exception\ApplicationException;
use common\models\ExperimentCategory;

class ExperimentCategoryItemHandler
{
    /**
     * @param ExperimentCategoryItemQuery $query
     *
     * @return ExperimentCategoryItemResult
     *
     * @throws ApplicationException
     */
    public function handle(ExperimentCategoryItemQuery $query): ExperimentCategoryItemResult
    {
        $item = ExperimentCategory::findOne(['id' => $query->id]);

        if (null === $item) {
            throw new ApplicationException(
                sprintf('Experiment "%s" not found', $query->id),
                404
            );
        }

        return new ExperimentCategoryItemResult((int) $item->id, $item->name);
    }
}
