<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Query;

use api\application\Common\Query\BaseQuery;

class ExperimentCategoryListQuery extends BaseQuery
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var int
     */
    public $onPage = 20;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['page', 'onPage'], 'integer'],
            [['page', 'onPage'], 'filter', 'filter' => 'intval'],
        ];
    }
}
