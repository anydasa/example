<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Command;

use api\application\Common\Command\BaseCommand;

/**
 * Class DeleteExperimentCategoryCommand.
 */
class DeleteExperimentCategoryCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
