<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Command;

use api\application\Common\Exception\ApplicationException;
use common\models\ExperimentCategory;
use Throwable;

/**
 * Class CreateExperimentCategoryHandler.
 */
class CreateExperimentCategoryHandler
{
    /**
     * @param CreateExperimentCategoryCommand $command
     *
     * @throws ApplicationException
     */
    public function handle(CreateExperimentCategoryCommand $command): void
    {
        $entity = ExperimentCategory::find()
            ->where(['id' => $command->id])
            ->orWhere(['name' => $command->name])
            ->limit(1)
            ->one();

        if ($entity) {
            throw new ApplicationException(
                sprintf('Category with name "%s" already exists', $command->name),
                400
            );
        }

        $model = new ExperimentCategory();
        $model->id = $command->id;
        $model->name = $command->name;

        try {
            $model->insert();
        } catch (Throwable $e) {
            throw new ApplicationException(
                sprintf('Cannot add ExperimentCategory "%s"', $command->name),
                400
            );
        }
    }
}
