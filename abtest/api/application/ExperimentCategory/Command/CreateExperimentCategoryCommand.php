<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Command;

use api\application\Common\Command\BaseCommand;

/**
 * Class CreateExperimentCategoryCommand.
 */
class CreateExperimentCategoryCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string'],
            [['id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
