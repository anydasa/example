<?php

declare(strict_types=1);

namespace api\application\ExperimentCategory\Command;

use api\application\Common\Exception\EntityDeleteFailedException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\ExperimentCategory;

class DeleteExperimentCategoryHandler
{
    /**
     * @param DeleteExperimentCategoryCommand $command
     *
     * @throws EntityDeleteFailedException
     * @throws EntityNotFoundException
     */
    public function handle(DeleteExperimentCategoryCommand $command): void
    {
        $experimentCategory = ExperimentCategory::findOne(['id' => $command->id]);

        if (null === $experimentCategory) {
            throw new EntityNotFoundException(sprintf('ExperimentCategory "%s" not found', $command->id));
        }

        try {
            $experimentCategory->delete();
        } catch (\Throwable $e) {
            throw new EntityDeleteFailedException(sprintf('Cannot delete ExperimentCategory "%s"', $command->id));
        }
    }
}
