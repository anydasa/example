<?php

declare(strict_types=1);

namespace api\application\Project\Query;

use api\application\Common\Query\BaseQuery;

class ProjectListQuery extends BaseQuery
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var int
     */
    public $onPage = 20;

    /**
     * @var array
     */
    public $filter = [];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['page', 'onPage'], 'number'],
            [['page', 'onPage'], 'filter', 'filter' => 'intval'],
            [['filter'], 'safe'],
        ];
    }
}
