<?php

declare(strict_types=1);

namespace api\application\Project\Query;

/**
 * Class ProjectListResult
 */
class ProjectListResult
{
    /**
     * @var int
     */
    private $totalCount;

    /**
     * @var int
     */
    private $page;

    /**
     * @var array
     */
    private $list = [];

    /**
     * ExperimentListResult constructor.
     *
     * @param $totalCount
     * @param $page
     */
    public function __construct(int $totalCount, int $page)
    {
        $this->totalCount = $totalCount;
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return count($this->list);
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param array $list
     */
    public function setList(array $list): void
    {
        $this->list = $list;
    }
}
