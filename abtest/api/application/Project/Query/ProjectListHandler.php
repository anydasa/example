<?php

declare(strict_types=1);

namespace api\application\Project\Query;

use common\models\Project;

/**
 * Class ProjectListHandler
 */
class ProjectListHandler
{
    /**
     * @param ProjectListQuery $query
     * @return ProjectListResult
     */
    public function handle(ProjectListQuery $query): ProjectListResult
    {
        $dbQuery = Project::find();
        $dbQuery->limit($query->onPage);
        $dbQuery->offset($query->page * $query->onPage - $query->onPage);
        $dbQuery->orderBy('id DESC');

        $result = $dbQuery->all();
        $totalCount = (int)$dbQuery->count();

        $res = new ProjectListResult($totalCount, $query->page);

        $res->setList($result);

        return $res;
    }
}
