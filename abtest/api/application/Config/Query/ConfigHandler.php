<?php

declare(strict_types=1);

namespace api\application\Config\Query;

use common\models\Experiment;

class ConfigHandler
{
    /**
     * @param ConfigQuery $query
     *
     * @return ConfigResult
     */
    public function handle(ConfigQuery $query): ConfigResult
    {
        /** @var Experiment[] $experiments */
        $experiments = Experiment::find()
            ->anyStatus([Experiment::STATUS_RUNNING, Experiment::STATUS_COLLECTING])
            ->all();

        $experimentsData = [];

        foreach ($experiments as $experiment) {
            $experimentsData[$experiment->id] = [
                'id' => $experiment->id,
                'code' => $experiment->code,
                'name' => $experiment->name,
                'status' => $experiment->getStatusName(),
                'allow_add_participant' => $experiment->allow_add_participant,
                'variants' => array_map(static function (Experiment\Variant $variant) {
                    return $variant->toArray();
                }, $experiment->getVariants()),
            ];
        }

        return new ConfigResult(array_values($experimentsData));
    }
}
