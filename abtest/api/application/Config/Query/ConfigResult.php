<?php

declare(strict_types=1);

namespace api\application\Config\Query;

/**
 * Class ConfigResult.
 */
class ConfigResult
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
