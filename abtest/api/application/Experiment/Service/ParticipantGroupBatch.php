<?php

declare(strict_types=1);

namespace api\application\Experiment\Service;

use common\models\ExperimentParticipant;
use Yii;

class ParticipantGroupBatch
{
    private const BATCH_BUFFER_MAX_SIZE = 10000;

    /**
     * @var array
     */
    private $batchBuffer = [];

    public function save($force = false): void
    {
        if ($force || count($this->batchBuffer) >= self::BATCH_BUFFER_MAX_SIZE) {
            $this->saveBatchBuffer();
        }
    }

    /**
     * @param ExperimentParticipant $experimentParticipant
     */
    public function prepareUserForSave(ExperimentParticipant $experimentParticipant): void
    {
        $this->batchBuffer[] = [
            'id' => $experimentParticipant->id,
            'variant' => $experimentParticipant->variant,
            'experiment_id' => $experimentParticipant->experiment_id,
            'participant_id' => $experimentParticipant->participant_id,
            'user_id' => $experimentParticipant->user_id,
        ];
    }

    /**
     * @throws \yii\db\Exception
     */
    private function saveBatchBuffer(): void
    {
        if (empty($this->batchBuffer)) {
            return;
        }

        $sql = Yii::$app->db
            ->getQueryBuilder()
            ->batchInsert(
                ExperimentParticipant::tableName(),
                ['id', 'variant', 'experiment_id', 'participant_id', 'user_id'],
                $this->batchBuffer
            );

        $sql .= ' ON DUPLICATE KEY UPDATE id=id';

        Yii::$app->db->createCommand($sql)->execute();

        $this->batchBuffer = [];
    }
}
