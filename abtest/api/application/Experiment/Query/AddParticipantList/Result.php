<?php

declare(strict_types=1);

namespace api\application\Experiment\Query\AddParticipantList;

use Iterator;

class Result
{
    /**
     * @var ResultItem[]
     */
    private $items = [];

    /**
     * @return Iterator|ResultItem[]
     */
    public function getIterator(): Iterator
    {
        foreach ($this->items as $item) {
            yield $item;
        }
    }

    /**
     * @param ResultItem $item
     */
    public function addItem(ResultItem $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return resource
     */
    public function toCsvStream()
    {
        $f = fopen('php://memory', 'w+b');

        fputcsv($f, ['experiment_variant', 'user_id']);

        foreach ($this->getIterator() as $item) {
            fputcsv($f, [
                $item->getVariant(),
                $item->getUserId(),
            ]);
        }

        rewind($f);

        return $f;
    }
}
