<?php

declare(strict_types=1);

namespace api\application\Experiment\Query\AddParticipantList;

use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;
use common\models\ExperimentParticipant;

/**
 * Class Handler.
 */
class Handler
{
    /**
     * @param Query $query
     *
     * @return Result
     *
     * @throws EntityNotFoundException
     */
    public function handle(Query $query): Result
    {
        $experiment = Experiment::findOne(['id' => $query->experiment_id]);

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        $dbQuery = ExperimentParticipant::find();
        $dbQuery->asArray();
        $dbQuery->alias('ep');
        $dbQuery->select([
            'ep.variant',
            'ep.user_id',
        ]);
        $dbQuery->andWhere([
            'ep.experiment_id' => $experiment->id,
            'ep.user_id' => $query->user_ids,
        ]);

        $result = new Result();

        foreach ($dbQuery->each(1000) as $participant) {
            $resultItem = new ResultItem(
                $participant['variant'],
                (int)$participant['user_id']
            );

            $result->addItem($resultItem);
        }

        return $result;
    }
}
