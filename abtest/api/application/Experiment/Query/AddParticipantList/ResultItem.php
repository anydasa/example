<?php

declare(strict_types=1);

namespace api\application\Experiment\Query\AddParticipantList;

class ResultItem
{
    /**
     * @var string
     */
    private $variant;

    /**
     * @var int
     */
    private $userId;

    /**
     * ResultItem constructor.
     *
     * @param string $variant
     * @param int $userId
     */
    public function __construct(string $variant, int $userId)
    {
        $this->variant = $variant;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getVariant(): string
    {
        return $this->variant;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}
