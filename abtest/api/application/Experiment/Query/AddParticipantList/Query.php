<?php

declare(strict_types=1);

namespace api\application\Experiment\Query\AddParticipantList;

use api\application\Common\Query\BaseQuery;

/**
 * Class Query.
 */
class Query extends BaseQuery
{
    /**
     * @var int
     */
    public $experiment_id;

    /**
     * @var int[]
     */
    public $user_ids = [];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['experiment_id', 'user_ids'], 'required'],
            [['experiment_id'], 'integer'],
            [['experiment_id'], 'filter', 'filter' => 'intval'],
            [['user_ids'], 'each', 'rule' => ['integer']],
            [['user_ids'], 'each', 'rule' => ['filter', 'filter' => 'intval']],
        ];
    }
}
