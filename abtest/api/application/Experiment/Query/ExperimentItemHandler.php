<?php

declare(strict_types=1);

namespace api\application\Experiment\Query;

use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;

class ExperimentItemHandler
{
    /**
     * @param ExperimentItemQuery $query
     *
     * @return ExperimentItemResult
     *
     * @throws EntityNotFoundException
     */
    public function handle(ExperimentItemQuery $query): ExperimentItemResult
    {
        $item = Experiment::find()
            ->id($query->id)
            ->one();

        if (null === $item) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        return new ExperimentItemResult($item);
    }
}
