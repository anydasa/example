<?php

declare(strict_types=1);

namespace api\application\Experiment\Query;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;
use common\models\ExperimentParticipant;
use ys\mp\Uid;

class ParticipantHandler
{
    /**
     * @param ParticipantQuery $query
     *
     * @return ExperimentParticipant|null
     *
     * @throws ApplicationException
     * @throws EntityNotFoundException
     */
    public function handle(ParticipantQuery $query): ?ExperimentParticipant
    {
        $experiment = Experiment::findOne(['id' => $query->experiment_id]);

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        $experimentParticipant = $this->findExperimentParticipant($experiment, $query->user_id, $query->participant_id);

        if ($experimentParticipant) {
            return $experimentParticipant;
        }

        if ($experiment->isStatus(Experiment::STATUS_COLLECTING)) {
            throw new ApplicationException('Cant add new Participant as experiment in collecting mode');
        }

        if (!$experiment->isStatus(Experiment::STATUS_RUNNING)) {
            throw new ApplicationException('Cant add new Participant as experiment is not running');
        }

        if ($experiment->isTimeOut() || $experiment->isParticipantCountReached()) {
            $experiment->setStatus(Experiment::STATUS_COLLECTING);
            $experiment->save();

            throw new ApplicationException('Cant add new Participant as experiment in collecting mode');
        }

        return $this->createExperimentParticipant($experiment, $query->user_id, $query->participant_id);
    }

    /**
     * @param Experiment $experiment
     * @param            $userId
     * @param            $participantId
     *
     * @return ExperimentParticipant|null
     *
     * @throws ApplicationException
     */
    private function findExperimentParticipant(Experiment $experiment, $userId, $participantId): ?ExperimentParticipant
    {
        $experimentParticipant = ExperimentParticipant::find()
            ->participantId($participantId)
            ->experiment($experiment)
            ->limit(1)
            ->one()
        ;

        if ($experimentParticipant) {
            return $experimentParticipant;
        }

        if (!$userId) {
            return null;
        }

        $experimentParticipant = ExperimentParticipant::find()
            ->userId($userId)
            ->experiment($experiment)
            ->limit(1)
            ->one()
        ;

        if ($experimentParticipant) {
            $experimentParticipant->participant_id = $participantId;

            try {
                $experimentParticipant->update();
            } catch (\Throwable $e) {
                throw new ApplicationException('Cant Update participant_id');
            }

            return $experimentParticipant;
        }

        return null;
    }

    /**
     * @param Experiment $experiment
     * @param            $userId
     * @param            $participantId
     *
     * @return ExperimentParticipant
     *
     * @throws ApplicationException
     */
    private function createExperimentParticipant(Experiment $experiment, $userId, $participantId): ExperimentParticipant
    {
        $experimentParticipant = new ExperimentParticipant();
        $experimentParticipant->id = Uid::generate();
        $experimentParticipant->variant = $experiment->chooseVariant()->getCode();
        $experimentParticipant->experiment_id = $experiment->id;
        $experimentParticipant->participant_id = $participantId;
        $experimentParticipant->user_id = $userId;

        try {
            $experimentParticipant->insert();
        } catch (\Throwable $e) {
            throw new ApplicationException('Cant Add experimentParticipant');
        }

        return $experimentParticipant;
    }
}
