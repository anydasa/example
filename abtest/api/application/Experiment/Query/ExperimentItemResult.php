<?php

declare(strict_types=1);

namespace api\application\Experiment\Query;

use common\models\Experiment;

/**
 * Class ExperimentItemResult.
 */
class ExperimentItemResult
{
    /**
     * @var Experiment
     */
    private $experiment;

    /**
     * ExperimentItemResult constructor.
     *
     * @param Experiment $experiment
     */
    public function __construct(Experiment $experiment)
    {
        $this->experiment = $experiment;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->experiment->id,
            'name' => $this->experiment->name,
            'category_id' => $this->experiment->experiment_category_id,
            'category_name' => $this->experiment->category->name,
            'project_code' => $this->experiment->project->code,
            'project_name' => $this->experiment->project->name,
            'code' => $this->experiment->code,
            'allow_add_participant' => $this->experiment->allow_add_participant,
            'participant_count_limit' => $this->experiment->participant_count_limit,
            'start_time' => $this->experiment->getFormattedStartTime(),
            'finish_time' => $this->experiment->getFormattedFinishTime(),
            'status_code' => $this->experiment->status,
            'status_name' => $this->experiment->getStatusName(),
            'variants' => array_map(static function (Experiment\Variant $variant) {
                return $variant->toArray();
            }, $this->experiment->getVariants()),
        ];
    }
}
