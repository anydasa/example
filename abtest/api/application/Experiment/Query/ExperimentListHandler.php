<?php

declare(strict_types=1);

namespace api\application\Experiment\Query;

use common\models\Experiment;
use common\models\Project;

class ExperimentListHandler
{
    public function handle(ExperimentListQuery $query): ExperimentListResult
    {
        $dbQuery = Experiment::find()
            ->select([
                Experiment::tableName() . '.*',
                'p.code AS project_code',
            ])
            ->leftJoin(['p' => Project::tableName()], 'p.id = ' . Experiment::tableName() . '.project_id');

        $dbQuery->filter($query->filter);
        $dbQuery->limit($query->onPage);
        $dbQuery->offset($query->page * $query->onPage - $query->onPage);
        $dbQuery->orderBy('id DESC');
        $result = $dbQuery->all();
        $totalCount = (int)$dbQuery->count();

        $res = new ExperimentListResult($totalCount, $query->page);

        $res->setList(
            array_map(static function (Experiment $experiment) {
                return [
                    'id' => $experiment->id,
                    'category_id' => $experiment->experiment_category_id,
                    'category_name' => $experiment->category->name,
                    'project_code' => $experiment->project->code,
                    'project_name' => $experiment->project->name,
                    'code' => $experiment->code,
                    'name' => $experiment->name,
                    'start_time' => $experiment->getFormattedStartTime(),
                    'finish_time' => $experiment->getFormattedFinishTime(),
                    'participant_count_limit' => $experiment->participant_count_limit,
                    'allow_add_participant' => $experiment->allow_add_participant,
                    'status_code' => $experiment->status,
                    'status_name' => $experiment->getStatusName(),
                    'variants' => array_map(static function (Experiment\Variant $variant) {
                        return $variant->toArray();
                    }, $experiment->getVariants()),
                ];
            }, $result)
        );

        return $res;
    }
}
