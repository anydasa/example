<?php

declare(strict_types=1);

namespace api\application\Experiment\Query;

use api\application\Common\Query\BaseQuery;

class ParticipantQuery extends BaseQuery
{
    /**
     * @var int
     */
    public $experiment_id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var string
     */
    public $participant_id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['experiment_id', 'participant_id'], 'required'],
            [['participant_id'], 'string', 'max' => 32, 'min' => 32],
            [['experiment_id'], 'filter', 'filter' => 'intval'],
            [['user_id'], 'filter', 'filter' => static function ($value) { return $value ? intval($value) : null; }],
        ];
    }
}
