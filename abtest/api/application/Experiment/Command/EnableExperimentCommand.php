<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Command\BaseCommand;

/**
 * Class EnableExperimentCommand.
 */
class EnableExperimentCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
