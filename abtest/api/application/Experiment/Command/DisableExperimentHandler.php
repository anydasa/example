<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;

/**
 * Class DisableExperimentHandler.
 */
class DisableExperimentHandler
{
    /**
     * @param DisableExperimentCommand $command
     *
     * @throws EntityNotFoundException
     */
    public function handle(DisableExperimentCommand $command): void
    {
        $experiment = Experiment::findOne(['id' => $command->id]);

        if (null === $experiment) {
            throw new EntityNotFoundException('Cannot delete Experiment as entity not found');
        }

        $experiment->status = Experiment::STATUS_DISABLED;

        $experiment->save();
    }
}
