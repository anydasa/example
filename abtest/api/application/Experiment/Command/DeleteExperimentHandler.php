<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\EntityDeleteFailedException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;

class DeleteExperimentHandler
{
    /**
     * @param DeleteExperimentCommand $command
     *
     * @throws EntityDeleteFailedException
     * @throws EntityNotFoundException
     */
    public function handle(DeleteExperimentCommand $command): void
    {
        $experimentCategory = Experiment::findOne(['id' => $command->id]);

        if (null === $experimentCategory) {
            throw new EntityNotFoundException('Cannot delete Experiment as entity not found');
        }

        try {
            $experimentCategory->delete();
        } catch (\Throwable $e) {
            throw new EntityDeleteFailedException(sprintf('Cannot delete Experiment "%s"', $command->id));
        }
    }
}
