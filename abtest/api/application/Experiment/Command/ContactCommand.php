<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Command\BaseCommand;

class ContactCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $experiment_id;

    /**
     * @var string
     */
    public $participant_id;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['experiment_id', 'participant_id'], 'required'],
            [['experiment_id'], 'integer'],
            [['participant_id'], 'string', 'max' => 32, 'min' => 32],
            [['experiment_id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
