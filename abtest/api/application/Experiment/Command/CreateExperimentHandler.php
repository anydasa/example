<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\ApplicationException;
use common\models\Experiment;
use common\models\ExperimentCategory;
use common\models\Project;
use Throwable;

class CreateExperimentHandler
{
    /**
     * @param CreateExperimentCommand $command
     *
     * @throws ApplicationException
     */
    public function handle(CreateExperimentCommand $command): void
    {
        $category = ExperimentCategory::findOne(['id' => $command->category_id]);

        if (!$category) {
            throw new ApplicationException(
                sprintf('Category "%s" not found', $command->category_id),
                400
            );
        }

        if (!empty($command->project_code)) {
            /** @var Project $project */
            $project = Project::find()->where(['code' => $command->project_code])->limit(1)->one();

            if (!$project) {
                throw new ApplicationException(
                    sprintf('Project with code "%s" not found', $command->project_code),
                    400
                );
            }
        }

        $experiment = Experiment::find()
            ->where(['id' => $command->id])
            ->orWhere(['experiment_category_id' => $command->category_id, 'code' => $command->code])
            ->limit(1)
            ->one();

        if ($experiment) {
            throw new ApplicationException(
                sprintf('Experiment with category "%s" and code "%s" already exists', $command->category_id, $command->name),
                400
            );
        }

        $model = new Experiment();
        $model->id = $command->id;
        $model->code = $command->code;
        $model->name = $command->name;
        $model->participant_count_limit = $command->participant_count_limit;
        $model->project_id = isset($project) ? $project->id : null;
        $model->experiment_category_id = $command->category_id;
        $model->allow_add_participant = $command->allow_add_participant;
        $model->start_time = $command->start_time;
        $model->finish_time = $command->finish_time;
        $model->status = Experiment::STATUS_DISABLED;
        $model->setVariants($command->variants);

        try {
            $model->insert();
        } catch (Throwable $e) {
            throw new ApplicationException(
                sprintf('Cannot add Experiment "%s"', $command->name),
                400
            );
        }
    }
}
