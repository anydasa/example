<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;
use common\models\ExperimentParticipant;
use common\models\ParticipantContact;

class ContactHandler
{
    /**
     * @param ContactCommand $command
     *
     * @throws ApplicationException
     * @throws EntityNotFoundException
     */
    public function handle(ContactCommand $command): void
    {
        $experiment = Experiment::find()->id($command->experiment_id)->one();

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment not found');
        }

        $experimentParticipant = ExperimentParticipant::find()
            ->experiment($experiment)
            ->participantId($command->participant_id)
            ->one();

        if (!$experimentParticipant) {
            throw new EntityNotFoundException('Experiment Participant not found');
        }

        $participantContact = new ParticipantContact();
        $participantContact->experiment_participant_id = $experimentParticipant->id;

        try {
            $participantContact->insert();
        } catch (\Throwable $e) {
            throw new ApplicationException('Cant add contact');
        }
    }
}
