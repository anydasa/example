<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Command\BaseCommand;
use DateTime;
use yii\base\DynamicModel;

/**
 * Class EditExperimentCommand
 *
 * @package api\application\Experiment\Command
 */
class EditExperimentCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $category_id;

    /**
     * @var string
     */
    public $project_code;

    /**
     * @var DateTime|null
     */
    public $start_time;

    /**
     * @var DateTime|null
     */
    public $finish_time;

    /**
     * @var array
     */
    public $variants;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name', 'category_id', 'variants'], 'required'],
            [['name', 'project_code'], 'string'],
            [['variants'], 'validateVariants'],
            [['id', 'category_id'], 'integer'],

            [['start_time', 'finish_time'], 'date', 'format' => 'php:' . DATE_ATOM],
            [
                ['start_time', 'finish_time'],
                'filter',
                'filter' => static function ($value) {
                    return $value ? new DateTime($value) : null;
                },
            ],

            [['id', 'category_id'], 'filter', 'filter' => 'intval'],
        ];
    }

    public function validateVariants(): void
    {
        if (!is_array($this->variants)) {
            $this->addError('variants', 'variants is not an array!');
        }

        foreach ($this->variants as $k => $variant) {
            if (!is_array($variant)) {
                $this->addError('variants', 'variant is not an array!');
            }

            $model = DynamicModel::validateData($this->variants[$k], [
                [['name'], 'required'],
            ]);

            if ($model->hasErrors()) {
                $this->addErrors($model->getErrors());
            }
        }

        if (!$this->hasErrors()) {
            $this->validateUniqueVariants();
        }
    }

    private function validateUniqueVariants(): void
    {
        $currentNames = [];

        foreach ($this->variants as $variant) {
            if (in_array($variant['name'], $currentNames, true)) {
                $this->addError('name', 'Not unique variant name:' . $variant['name']);
            }

            $currentNames[] = $variant['name'];
        }
    }
}