<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Experiment\Service\ParticipantGroupBatch;
use common\models\Experiment;
use common\models\ExperimentParticipant;
use ys\mp\Uid;

/**
 * Class AddParticipantListHandler.
 */
class AddParticipantListHandler
{
    /**
     * @param AddParticipantListCommand $command
     *
     * @throws EntityNotFoundException
     */
    public function handle(AddParticipantListCommand $command): void
    {
        $experiment = Experiment::findOne(['id' => $command->experiment_id]);

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        $this->split($command->user_ids, $experiment);
    }

    /**
     * @param int[] $userIds
     * @param Experiment $experiment
     */
    private function split(array $userIds, Experiment $experiment): void
    {
        $batchService = new ParticipantGroupBatch();

        foreach ($userIds as $userId) {
            $experimentParticipant = new ExperimentParticipant();
            $experimentParticipant->id = Uid::generate();
            $experimentParticipant->variant = $experiment->chooseVariant()->getCode();
            $experimentParticipant->experiment_id = $experiment->id;
            $experimentParticipant->user_id = $userId;

            $batchService->prepareUserForSave($experimentParticipant);
            $batchService->save();
        }

        $batchService->save(true);
    }
}
