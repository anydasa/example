<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\ApplicationException;
use common\models\Experiment;
use common\models\ExperimentCategory;
use common\models\Project;
use Throwable;

class EditExperimentHandler
{
    /**
     * @param EditExperimentCommand $command
     * @return bool
     * @throws ApplicationException
     */
    public function handle(EditExperimentCommand $command): bool
    {
        $experiment = Experiment::findOne(['id' => $command->id]);

        if (!$experiment) {
            throw new ApplicationException(
                sprintf('Experiment "%s" not found', $command->id),
                400
            );
        }

        $category = ExperimentCategory::findOne(['id' => $command->category_id]);

        if (!$category) {
            throw new ApplicationException(
                sprintf('Category "%s" not found', $command->category_id),
                400
            );
        }

        if (!empty($command->project_code)) {
            /** @var Project $project */
            $project = Project::find()->where(['code' => $command->project_code])->limit(1)->one();

            if (!$project) {
                throw new ApplicationException(
                    sprintf('Project with code "%s" not found', $command->project_code),
                    400
                );
            }
        }

        $experiment->name = $command->name;
        $experiment->experiment_category_id = $command->category_id;
        $experiment->project_id = isset($project) ? $project->id : null;
        $experiment->start_time = $command->start_time;
        $experiment->finish_time = $command->finish_time;
        $experiment->mergeVariants($command->variants);

        try {
            return 1 === $experiment->update();
        } catch (Throwable $e) {
            throw new ApplicationException(
                sprintf('Cannot update Experiment "%s"', $command->name),
                400
            );
        }
    }
}
