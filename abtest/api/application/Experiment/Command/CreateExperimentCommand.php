<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Command\BaseCommand;
use DateTime;
use yii\base\DynamicModel;
use ys\mp\Uid;

/**
 * Class CreateExperimentCommand
 *
 * @package api\application\Experiment\Command
 */
class CreateExperimentCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $category_id;

    /**
     * @var string
     */
    public $project_code;

    /**
     * @var bool
     */
    public $allow_add_participant = false;

    /**
     * @var int|null
     */
    public $participant_count_limit;

    /**
     * @var DateTime|null
     */
    public $start_time;

    /**
     * @var DateTime|null
     */
    public $finish_time;

    /**
     * @var array
     */
    public $variants;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name', 'category_id', 'code', 'variants'], 'required'],
            ['id', 'default', 'value' => Uid::generate()],
            [['name', 'code', 'project_code'], 'string'],
            [['variants'], 'validateVariants'],
            [['id', 'category_id', 'participant_count_limit'], 'integer'],
            [['allow_add_participant'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],

            [['start_time', 'finish_time'], 'date', 'format' => 'php:' . DATE_ATOM],
            [
                ['start_time', 'finish_time'],
                'filter',
                'filter' => static function ($value) {
                    return $value ? new DateTime($value) : null;
                },
            ],

            [['id', 'category_id'], 'filter', 'filter' => 'intval'],
            [
                ['participant_count_limit'],
                'filter',
                'filter' => static function ($value) {
                    return $value ? intval($value) : null;
                },
            ],
        ];
    }

    public function validateVariants(): void
    {
        if (!is_array($this->variants)) {
            $this->addError('variants', 'variants is not an array!');
        }

        foreach ($this->variants as $k => $variant) {
            if (!is_array($variant)) {
                $this->addError('variants', 'variant is not an array!');
            }

            if (!isset($variant['is_null'])) {
                $this->variants[$k]['is_null'] = false;
            }

            $model = DynamicModel::validateData($this->variants[$k], [
                [['name', 'code'], 'required'],
                ['code', 'string', 'max' => 32],
                ['code', 'match', 'pattern' => '/^[a-z0-9_-]+$/', 'message' => 'Code Contain only small alphabets and "-_" symbols'],
                ['is_null', 'boolean'],
            ]);

            if ($model->hasErrors()) {
                $this->addErrors($model->getErrors());
            }
        }

        if (!$this->hasErrors()) {
            $this->validateUniqueVariants();
        }
    }

    private function validateUniqueVariants(): void
    {
        $currentCodes = [];
        $currentNames = [];

        foreach ($this->variants as $variant) {
            if (in_array($variant['code'], $currentCodes, true)) {
                $this->addError('code', 'Not unique variant code:' . $variant['code']);
            }

            if (in_array($variant['name'], $currentNames, true)) {
                $this->addError('name', 'Not unique variant name:' . $variant['name']);
            }

            $currentCodes[] = $variant['code'];
            $currentNames[] = $variant['name'];
        }
    }
}