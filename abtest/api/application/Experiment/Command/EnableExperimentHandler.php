<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;

/**
 * Class EnableExperimentHandler.
 */
class EnableExperimentHandler
{
    /**
     * @param EnableExperimentCommand $command
     *
     * @throws EntityNotFoundException
     * @throws ApplicationException
     */
    public function handle(EnableExperimentCommand $command): void
    {
        $experiment = Experiment::findOne(['id' => $command->id]);

        if (null === $experiment) {
            throw new EntityNotFoundException('Cannot delete Experiment as entity not found');
        }

        $experiment->setStatus(Experiment::STATUS_RUNNING);

        //TODO add checks to possibility enable test

        $experiment->save();
    }
}
