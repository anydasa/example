<?php

declare(strict_types=1);

namespace api\application\Experiment\Command;

use api\application\Common\Command\BaseCommand;

/**
 * Class AddParticipantListCommand.
 */
class AddParticipantListCommand extends BaseCommand
{
    /**
     * @var int
     */
    public $experiment_id;

    /**
     * @var int[]
     */
    public $user_ids = [];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['experiment_id', 'user_ids'], 'required'],
            [['experiment_id'], 'integer'],
            [['experiment_id'], 'filter', 'filter' => 'intval'],
            [['user_ids'], 'each', 'rule' => ['integer']],
            [['user_ids'], 'each', 'rule' => ['filter', 'filter' => 'intval']],
        ];
    }
}
