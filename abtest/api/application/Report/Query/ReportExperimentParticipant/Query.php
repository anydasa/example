<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentParticipant;

use api\application\Common\Query\BaseQuery;

class Query extends BaseQuery
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $experiment_variant;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['experiment_variant'], 'string'],
            [['id'], 'filter', 'filter' => 'intval'],
        ];
    }
}
