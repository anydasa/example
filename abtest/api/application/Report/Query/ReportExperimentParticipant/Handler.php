<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentParticipant;

use api\application\Common\Exception\ApplicationException;
use api\application\Common\Exception\EntityNotFoundException;
use common\models\Experiment;
use common\models\ExperimentParticipant;
use common\models\ParticipantContact;
use DateTime;

class Handler
{
    /**
     * @param Query $query
     *
     * @return Result
     *
     * @throws ApplicationException
     * @throws EntityNotFoundException
     */
    public function handle(Query $query): Result
    {
        $experiment = Experiment::find()
            ->id($query->id)
            ->one();

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        $dbQuery = ExperimentParticipant::find();
        $dbQuery->asArray();
        $dbQuery->alias('ep');
        $dbQuery->leftJoin(['pc' => ParticipantContact::tableName()], 'ep.id = pc.experiment_participant_id');
        $dbQuery->select([
            'ep.variant',
            'ep.participant_id',
            'ep.user_id',
            'first_contact' => 'MIN(pc.time)',
            'last_contact' => 'MAX(pc.time)',
        ]);
        $dbQuery->andWhere(['ep.experiment_id' => $experiment->id]);

        if ($query->experiment_variant) {
            $dbQuery->andWhere(['ep.variant' => $query->experiment_variant]);
        }

        $dbQuery->groupBy('ep.participant_id');

        $result = new Result();

        /** @var ExperimentParticipant $experimentParticipant */
        foreach ($dbQuery->each(1000) as $experimentParticipant) {
            $result->addItem(
                new ResultItem(
                    $experimentParticipant['variant'],
                    $experimentParticipant['participant_id'],
                    $experimentParticipant['user_id'] ? (int)$experimentParticipant['user_id'] : null,
                    $experimentParticipant['first_contact'] ? new DateTime($experimentParticipant['first_contact']) : null,
                    $experimentParticipant['last_contact'] ? new DateTime($experimentParticipant['last_contact']) : null
                )
            );
        }

        return $result;
    }
}
