<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentParticipant;

use Iterator;

class Result
{
    /**
     * @var ResultItem[]
     */
    private $items = [];

    /**
     * @return Iterator|ResultItem[]
     */
    public function getIterator(): Iterator
    {
        foreach ($this->items as $item) {
            yield $item;
        }
    }

    /**
     * @param ResultItem $item
     */
    public function addItem(ResultItem $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return resource
     */
    public function toCsvStream()
    {
        $f = fopen('php://memory', 'w+b');

        fputcsv($f, ['experiment_variant', 'participant_id', 'user_id', 'first_contact', 'last_contact']);

        foreach ($this->getIterator() as $item) {
            fputcsv($f, [
                $item->getVariant(),
                $item->getParticipantId(),
                $item->getUserId(),
                $item->getFirstContact() ? $item->getFirstContact()->format(DATE_ATOM) : null,
                $item->getLastContact() ? $item->getLastContact()->format(DATE_ATOM) : null,
            ]);
        }

        rewind($f);

        return $f;
    }
}
