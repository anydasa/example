<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentParticipant;

use DateTime;

class ResultItem
{
    /**
     * @var string
     */
    private $variant;

    /**
     * @var string|null
     */
    private $participantId;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var DateTime|null
     */
    private $firstContact;

    /**
     * @var DateTime|null
     */
    private $lastContact;

    /**
     * ResultItem constructor.
     *
     * @param string $variant
     * @param string|null $participantId
     * @param int|null $userId
     * @param DateTime|null $lastContact
     * @param DateTime|null $firstContact
     */
    public function __construct(
        string $variant,
        ?string $participantId,
        ?int $userId,
        ?DateTime $firstContact,
        ?DateTime $lastContact
    ) {
        $this->variant = $variant;
        $this->participantId = $participantId;
        $this->userId = $userId;
        $this->firstContact = $firstContact;
        $this->lastContact = $lastContact;
    }

    /**
     * @return string
     */
    public function getVariant(): string
    {
        return $this->variant;
    }

    /**
     * @return string|null
     */
    public function getParticipantId(): ?string
    {
        return $this->participantId;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return DateTime|null
     */
    public function getFirstContact(): ?DateTime
    {
        return $this->firstContact;
    }

    /**
     * @return DateTime|null
     */
    public function getLastContact(): ?DateTime
    {
        return $this->lastContact;
    }
}
