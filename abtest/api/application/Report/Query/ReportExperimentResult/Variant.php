<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentResult;

class Variant
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var int
     */
    private $participantCount;

    /**
     * @var int
     */
    private $contactsCount;

    /**
     * @var int
     */
    private $uniqContactsCount;

    public function __construct(
        string $name,
        string $code,
        int $participantCount,
        int $contactsCount,
        int $uniqContactsCount
    )
    {
        $this->name = $name;
        $this->code = $code;
        $this->participantCount = $participantCount;
        $this->contactsCount = $contactsCount;
        $this->uniqContactsCount = $uniqContactsCount;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getParticipantCount(): int
    {
        return $this->participantCount;
    }

    /**
     * @return int
     */
    public function getContactsCount(): int
    {
        return $this->contactsCount;
    }

    /**
     * @return int
     */
    public function getUniqContactsCount(): int
    {
        return $this->uniqContactsCount;
    }
}
