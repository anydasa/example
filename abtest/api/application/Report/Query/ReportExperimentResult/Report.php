<?php

declare(strict_types=1);

namespace api\application\Report\Query\ReportExperimentResult;

class Report
{
    /**
     * @var int
     */
    private $participantCount;

    /**
     * @var Variant[]
     */
    private $variants = [];

    public function __construct(int $participantCount)
    {
        $this->participantCount = $participantCount;
    }

    /**
     * @return int
     */
    public function getParticipantCount(): int
    {
        return $this->participantCount;
    }

    public function addVariant(Variant $variant): void
    {
        $this->variants[] = $variant;
    }

    /**
     * @return Variant[]
     */
    public function getVariants(): array
    {
        return $this->variants;
    }

    public function toArray(): array
    {
        return [
            'participant_count' => $this->getParticipantCount(),
            'variants' => array_map(static function (Variant $variant) {
                return [
                    'code' => $variant->getCode(),
                    'name' => $variant->getName(),
                    'participant_count' => $variant->getParticipantCount(),
                    'contact_count' => $variant->getContactsCount(),
                    'uniq_contact_count' => $variant->getUniqContactsCount(),
                ];
            }, $this->getVariants()),
        ];
    }
}
