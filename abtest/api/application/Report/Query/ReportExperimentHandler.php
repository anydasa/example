<?php

declare(strict_types=1);

namespace api\application\Report\Query;

use api\application\Common\Exception\EntityNotFoundException;
use api\application\Report\Query\ReportExperimentResult\Report;
use api\application\Report\Query\ReportExperimentResult\Variant;
use common\models\Experiment;
use common\models\ExperimentParticipant;

class ReportExperimentHandler
{
    /**
     * @param ReportExperimentQuery $query
     *
     * @return Report
     *
     * @throws EntityNotFoundException
     */
    public function handle(ReportExperimentQuery $query): Report
    {
        $experiment = Experiment::find()
            ->id($query->id)
            ->one();

        if (null === $experiment) {
            throw new EntityNotFoundException('Experiment entity not found');
        }

        $result = new Report($experiment->getParticipantCount());

        foreach ($experiment->getVariants() as $variant) {
            $participantCount = ExperimentParticipant::find()
                ->experiment($experiment)
                ->variant($variant)
                ->count();

            $contactsCount = ExperimentParticipant::find()
                ->experiment($experiment)
                ->variant($variant)
                ->contactsCount();

            $uniqContactsCount = ExperimentParticipant::find()
                ->experiment($experiment)
                ->variant($variant)
                ->contactsCount(true);

            $variantReport = new Variant(
                $variant->getName(),
                $variant->getCode(),
                $participantCount,
                $contactsCount,
                $uniqContactsCount
            );

            $result->addVariant($variantReport);
        }

        return $result;
    }
}
