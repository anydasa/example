<?php

declare(strict_types=1);

namespace common\models;

use common\models\Experiment\Variant;
use yii\db\ActiveQuery;

/**
 * Class ExperimentParticipantQuery.
 *
 * @see ExperimentParticipant
 */
class ExperimentParticipantQuery extends ActiveQuery
{
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->alias('ep');
    }

    public function participantId(string $participantId)
    {
        return $this->andWhere(['ep.participant_id' => $participantId]);
    }

    public function userId(int $userId)
    {
        return $this->andWhere(['ep.user_id' => $userId]);
    }

    public function experiment(Experiment $experiment)
    {
        return $this->andWhere(['ep.experiment_id' => $experiment->id]);
    }

    public function variant(Variant $variant)
    {
        return $this->andWhere(['ep.variant' => $variant->getCode()]);
    }

    public function contactsCount(bool $uniq = false): int
    {
        $this->innerJoin(ParticipantContact::tableName(), 'ep.id = '.ParticipantContact::tableName().'.experiment_participant_id');

        if ($uniq) {
            $this->groupBy('ep.id');
        }

        return $this->count();
    }

    public function count($q = '*', $db = null): int
    {
        return (int) parent::count($q, $db);
    }

    public function one($db = null)
    {
        $this->limit(1);

        return parent::one($db);
    }
}
