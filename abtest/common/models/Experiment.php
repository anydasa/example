<?php

declare(strict_types=1);

namespace common\models;

use api\application\Common\Exception\ApplicationException;
use common\models\Experiment\Variant;
use DateTime;
use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Experiment.
 *
 * @property int $id                           [bigint(20) unsigned]
 * @property int $experiment_category_id       [bigint(20) unsigned]
 * @property bool $project_id [tinyint(1) unsigned]
 * @property string $code                         [varchar(255)]
 * @property string $name                         [varchar(255)]
 * @property string $description
 * @property string|null $jira_issue                   [varchar(255)]
 * @property DateTime|null $start_time                   [datetime]
 * @property DateTime|null $finish_time                  [datetime]
 * @property int $participant_count_limit [int(10) unsigned]
 * @property bool $allow_add_participant        [tinyint(4)]
 * @property int $status                       [tinyint(4)]
 * @property string $variants
 * @property ExperimentCategory $category
 * @property Project $project
 */
class Experiment extends ActiveRecord
{
    public const STATUS_DISABLED = 0;
    public const STATUS_RUNNING = 1;
    public const STATUS_COMPLETED = 2;
    public const STATUS_COLLECTING = 3;

    public const STATUS_NAMES = [
        self::STATUS_DISABLED => 'disabled',
        self::STATUS_RUNNING => 'running',
        self::STATUS_COLLECTING => 'collecting',
        self::STATUS_COMPLETED => 'completed',
    ];

    public function behaviors(): array
    {
        return [
            [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'experiment_category_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'project_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'status' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'participant_count_limit' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'allow_add_participant' => AttributeTypecastBehavior::TYPE_BOOLEAN,
                    'start_time' => static function ($value) {
                        return empty($value) ? null : new DateTime($value);
                    },
                    'finish_time' => static function ($value) {
                        return empty($value) ? null : new DateTime($value);
                    },
                ],
                'typecastAfterFind' => true,
                'typecastAfterValidate' => false,
            ],
            [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'start_time' => static function ($value) {
                        return $value instanceof DateTime ? $value->format('Y-m-d H:i:s') : $value;
                    },
                    'finish_time' => static function ($value) {
                        return $value instanceof DateTime ? $value->format('Y-m-d H:i:s') : $value;
                    },
                ],
                'typecastBeforeSave' => true,
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::OP_DELETE,
        ];
    }

    public static function find(): ExperimentQuery
    {
        return new ExperimentQuery(static::class);
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(ExperimentCategory::class, ['id' => 'experiment_category_id']);
    }

    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getFormattedStartTime(): ?string
    {
        return $this->start_time ? $this->start_time->format(DATE_ATOM) : null;
    }

    public function getFormattedFinishTime(): ?string
    {
        return $this->finish_time ? $this->finish_time->format(DATE_ATOM) : null;
    }

    public function getStatusName(): string
    {
        return self::STATUS_NAMES[$this->status];
    }

    public function chooseVariant(): Variant
    {
        $variants = $this->getVariants();
        $key = array_rand($variants);

        return $variants[$key];
    }

    public function isStatus($status): bool
    {
        return $status === $this->status;
    }

    /**
     * @return Variant[]
     */
    public function getVariants(): array
    {
        $variants = json_decode($this->variants, true);

        return array_map(static function ($variant) {
            return new Variant(
                $variant['code'],
                $variant['name'],
                isset($variant['is_null']) ? (0 < $variant['is_null']) : false
            );
        }, $variants);
    }

    public function setVariants(array $variants): void
    {
        $this->variants = json_encode($variants);
    }

    public function mergeVariants(array $variants): void
    {
        $oldVariants = json_decode($this->variants, true);

        foreach ($oldVariants as $i => $oldVariant) {
            $oldVariants[$i]['name'] = $variants[$i]['name'];
        }

        $this->variants = json_encode($oldVariants);
    }

    public function isTimeOut(): bool
    {
        if (null === $this->finish_time) {
            return false;
        }

        if ($this->finish_time < new DateTime()) {
            return true;
        }

        return false;
    }

    public function isParticipantCountReached(): bool
    {
        if (null === $this->participant_count_limit) {
            return false;
        }

        return $this->getParticipantCount() >= $this->participant_count_limit;
    }

    public function getParticipantCount(): int
    {
        return ExperimentParticipant::find()
            ->experiment($this)
            ->count();
    }

    /**
     * @param $status
     * @throws ApplicationException
     */
    public function setStatus($status): void
    {
        if (!array_key_exists($status, self::STATUS_NAMES)) {
            throw new ApplicationException(sprintf('Cant change experiment status to: %s', $status));
        }

        $this->status = $status;
    }
}
