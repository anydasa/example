<?php

declare(strict_types=1);

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class ExperimentCategory.
 *
 * @property int    $id   [bigint(20) unsigned]
 * @property string $name [varchar(255)]
 */
class ExperimentCategory extends ActiveRecord
{
}
