<?php

declare(strict_types=1);

namespace common\models;

use DateTime;
use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveRecord;

/**
 * Class ParticipantContact.
 *
 * @property int      $experiment_participant_id [bigint(20) unsigned]
 * @property DateTime $time                      [timestamp]
 */
class ParticipantContact extends ActiveRecord
{
    public function behaviors(): array
    {
        return [
            [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'experiment_participant_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'time' => static function ($value) {
                        return empty($value) ? null : new DateTime($value);
                    },
                ],
                'typecastAfterFind' => true,
                'typecastAfterValidate' => false,
            ],
        ];
    }
}
