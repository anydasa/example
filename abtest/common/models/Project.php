<?php

declare(strict_types=1);

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Project
 *
 * @property bool $id [tinyint(1) unsigned]
 * @property string $code [varchar(2)]
 * @property string $name [varchar(32)]
 */
class Project extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'project';
    }
}
