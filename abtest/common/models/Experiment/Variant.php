<?php

declare(strict_types=1);

namespace common\models\Experiment;

class Variant
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isNull;

    public function __construct(string $code, string $name, bool $isNull = false)
    {
        $this->code = $code;
        $this->name = $name;
        $this->isNull = $isNull;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isNull(): bool
    {
        return $this->isNull;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'is_null' => $this->isNull()
        ];
    }
}
