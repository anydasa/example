<?php

declare(strict_types=1);

namespace common\models;

use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class ExperimentParticipant.
 *
 * @property int                  $id                    [bigint(20) unsigned]
 * @property int                  $experiment_id         [bigint(20) unsigned]
 * @property string               $participant_id        [varchar(32)]
 * @property int                  $user_id               [bigint(20) unsigned]
 * @property string               $time                  [datetime]
 * @property string               $variant               [varchar(32)]
 * @property ParticipantContact[] $contacts
 * @property Experiment           $experiment
 */
class ExperimentParticipant extends ActiveRecord
{
    public function behaviors(): array
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'experiment_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'user_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                ],
                'typecastAfterFind' => true,
            ],
        ];
    }

    public static function find(): ExperimentParticipantQuery
    {
        return new ExperimentParticipantQuery(static::class);
    }

    public function getExperiment(): ActiveQuery
    {
        return $this->hasOne(Experiment::class, ['id' => 'experiment_id']);
    }

    public function getContacts()
    {
        return $this->hasMany(ParticipantContact::class, ['experiment_participant_id' => 'id']);
    }
}
