<?php

declare(strict_types=1);

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ExperimentQuery.
 *
 * @see Experiment
 */
class ExperimentQuery extends ActiveQuery
{
    /**
     * @var array
     */
    public $fieldsFilter = [
        'name' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'code' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'experiment_category_id' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'start_time' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'finish_time' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'status' => [
            'methodName' => 'andWhere',
            'tableAlias' => 'experiment',
        ],
        'project_code' => [
            'methodName' => 'andHaving',
            'tableAlias' => '',
        ],
    ];

    /**
     * @param int $id
     * @return ExperimentQuery
     */
    public function id(int $id)
    {
        return $this->andWhere(['experiment.id' => $id]);
    }

    /**
     * @param null|array $filter
     * @return $this
     */
    public function filter(?array $filter): self
    {
        if (!empty($filter['name'])) {
            $this->addFilter($filter['name'], 'name');
        }

        if (!empty($filter['code'])) {
            $this->addFilter($filter['code'], 'code');
        }

        if (!empty($filter['experiment_category_id'])) {
            $this->addFilter($filter['experiment_category_id'], 'experiment_category_id');
        }

        if (!empty($filter['start_time'])) {
            $this->addFilter($filter['start_time'], 'start_time');
        }

        if (!empty($filter['finish_time'])) {
            $this->addFilter($filter['finish_time'], 'finish_time');
        }

        if (!empty($filter['status'])) {
            $this->addFilter($filter['status'], 'status');
        }

        if (!empty($filter['project_code'])) {
            $this->addFilter($filter['project_code'], 'project_code');
        }

        return $this;
    }

    /**
     * @param $condition
     * @param $key
     */
    private function addFilter($condition, $key)
    {
        $field = $this->getFiled($key);

        if (!empty($field['tableAlias'])) {
            $key = $field['tableAlias'] . '.' . $key;
        }

        if (ArrayHelper::isIndexed($condition)) {
            foreach ($condition as $filter) {
                $this->addCondition($key, $filter, $field['methodName']);
            }
        } else {
            $this->addCondition($key, $condition, $field['methodName']);
        }
    }

    /**
     * @param $key
     * @param array $filter
     * @param string $nameMethod
     */
    private function addCondition($key, array $filter, string $nameMethod = 'andWhere')
    {
        if ($filter['method'] === 'like%') {
            $this->$nameMethod(['like', $key, "{$filter['value']}%", false]);
        } elseif ($filter['method'] === '%like') {
            $this->$nameMethod(['like', $key, "%{$filter['value']}", false]);
        } else {
            $this->$nameMethod([$filter['method'], $key, $filter['value']]);
        }
    }

    /**
     * @param int $status
     * @return $this
     */
    public function status(int $status): self
    {
        return $this->andWhere(['status' => $status]);
    }

    /**
     * @param array $statusList
     * @return $this
     */
    public function anyStatus(array $statusList): self
    {
        return $this->andWhere(['in', 'experiment.status', $statusList]);
    }

    /**
     * @param null $db
     * @return array|ActiveRecord|null
     */
    public function one($db = null)
    {
        $this->limit(1);

        return parent::one($db);
    }

    /**
     * @param string $field
     * @return array
     */
    private function getFiled(string $field): array
    {
        if (array_key_exists($field, $this->fieldsFilter)) {
            return $this->fieldsFilter[$field];
        }

        return [];
    }
}
