<?php

declare(strict_types=1);

use mito\sentry\Component as SentryComponent;
use mito\sentry\Target as TargetSentry;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'sentry' => [
            'class' => SentryComponent::class,
            'dsn' => '',
            'publicDsn' => '',
            'enabled' => false,
            'client' => [
                'class' => Raven_Client::class,
                'curl_method' => 'async',
                'verify_ssl' => false,

                'processorOptions' => [
                    'Raven_SanitizeDataProcessor' => [
                        'fields_re' => '/(cvv|authorization|password|passwd|secret|password_confirmation|card_number|auth_pw)/i'
                    ]
                ],
            ],
            'environment' => YII_ENV_PROD,
            // if not set, the default is `production`,
        ],
        'log' => [
            'traceLevel' => 0,
            'targets' => [
                [
                    'class' => TargetSentry::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'container' => [
        'singletons' => [
            LoggerInterface::class => static function () {
                $logger = new Logger('app');
                $logger->pushHandler(new NullHandler());

                return $logger;
            },
        ],
    ],
];
