#AB test api

##Setup locally in docker

clone project
```bash
git clone ssh://git@ssh.gitlab.sysenv.net:26/abtest/api.git
```
Go to project dir
```bash
cd api
```
Clone git submodules
```bash
git submodule update --init --recursive
```
Build docker images
```bash
make build
```
Using make commands install vendors
```bash
make composer-install
```
Init Yii application and choose development environment
```bash
make init
```
Up docker containers
```bash
docker-compose up -d
```
Migrate DB
```bash
make migrate-db
```
Open in browser
```
http://localhost:81/
```
