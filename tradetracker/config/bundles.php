<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Broadway\Bundle\BroadwayBundle\BroadwayBundle::class => ['all' => true],
    Enqueue\Bundle\EnqueueBundle::class => ['all' => true],
    League\Tactician\Bundle\TacticianBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    AdgoalCommon\SagaBundle\SagaBundle::class => ['all' => true],
    AdgoalCommon\AlertingBundle\AlertingBundle::class => ['all' => true],
    AdgoalCommon\ErrorReportingBundle\ErrorReportingBundle::class => ['all' => true],
    AdgoalCommon\TaskBundle\TaskBundle::class => ['all' => true],
    Liuggio\StatsDClientBundle\LiuggioStatsDClientBundle::class => ['all' => true],
];
