<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Migrations;

use AdgoalCommon\Saga\Storage\DBALSagaRepository;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190130001704 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var EntityManager */
    private $em;

    /** @var DBALSagaRepository */
    private $sagaRepository;

    /**
     * Migration description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Create table `sagas`';
    }

    public function up(Schema $schema): void
    {
        $this->sagaRepository->configureSchema($schema);
        $this->em->flush();
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable($this->sagaRepository->getTableName());
        $this->em->flush();
    }

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->sagaRepository = $container->get(DBALSagaRepository::class);
        /** @var EntityManager em */
        $this->em = $container->get('doctrine.orm.entity_manager');
    }
}
