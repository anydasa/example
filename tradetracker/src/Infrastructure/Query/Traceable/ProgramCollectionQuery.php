<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Query\Traceable;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Query\ProgramCollectionQueryInterface;

/**
 * Class ProgramCollectionQuery.
 */
class ProgramCollectionQuery implements ProgramCollectionQueryInterface
{
    private const CACHE_DIR = VAR_PATH . 'dataStorage/ProgramCollectionQuery/';

    /**
     * @var ProgramCollectionQueryInterface
     */
    private $programCollectionQuery;

    /**
     * ProgramCollectionQuery constructor.
     *
     * @param ProgramCollectionQueryInterface $programCollectionQuery
     */
    public function __construct(ProgramCollectionQueryInterface $programCollectionQuery)
    {
        $this->programCollectionQuery = $programCollectionQuery;

        if (!is_dir(self::CACHE_DIR) && !mkdir(self::CACHE_DIR, 0755, true)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', self::CACHE_DIR));
        }
    }

    /**
     * @param AccountConfigDto $accountConfig
     *
     * @return object[]
     */
    public function fetchPrograms(AccountConfigDto $accountConfig): array
    {
        $fileName = self::CACHE_DIR . '_' . __FUNCTION__ . '_' . $accountConfig->getCountry() . '.json';

        if (!file_exists($fileName)) {
            $response = $this->programCollectionQuery->fetchPrograms($accountConfig);
            file_put_contents($fileName, json_encode($response));
        }

        return json_decode(file_get_contents($fileName), true);
    }
}
