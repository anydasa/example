<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Query\Soap;

use SoapClient;
use SoapFault;

/**
 * Class ClientFactory.
 */
class ClientFactory
{
    /**
     * @param string  $url
     * @param mixed[] $options
     *
     * @return SoapClient
     *
     * @throws SoapFault
     */
    public function makeInstance(string $url, array $options): SoapClient
    {
        return new SoapClient($url, $options);
    }
}
