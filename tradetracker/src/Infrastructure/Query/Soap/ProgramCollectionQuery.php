<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Query\Soap;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Query\ProgramCollectionQueryInterface;
use SoapFault;
use stdClass;

/**
 * Class ProgramCollectionQuery.
 */
class ProgramCollectionQuery implements ProgramCollectionQueryInterface
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var ClientFactory
     */
    private $clientFactory;

    /**
     * ProgramCollectionQuery constructor.
     *
     * @param string        $url
     * @param ClientFactory $clientFactory
     */
    public function __construct(string $url, ClientFactory $clientFactory)
    {
        $this->url = $url;
        $this->clientFactory = $clientFactory;
    }

    /**
     * @param AccountConfigDto $accountConfig
     *
     * @return stdClass[]
     *
     * @throws SoapFault
     */
    public function fetchPrograms(AccountConfigDto $accountConfig): array
    {
        $client = $this->clientFactory->makeInstance($this->url, ['compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP]);

        $client->authenticate($accountConfig->getApiUser(), $accountConfig->getApiKey());

        $response = $client->getCampaigns($accountConfig->getSiteId());

        return json_decode(json_encode($response), true);
    }
}
