<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Repository\AccountCollection;

use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Repository\AccountCollectionRepositoryInterface;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionStore.
 */
class AccountCollectionStore extends EventSourcingRepository implements AccountCollectionRepositoryInterface
{
    /**
     * AccountCollectionStore constructor.
     *
     * {@inheritdoc}
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            AccountCollectionAggregate::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * {@inheritdoc}
     */
    public function store(AccountCollectionAggregate $program): void
    {
        $this->save($program);
    }

    /**
     * {@inheritdoc}
     */
    public function get(UuidInterface $uuid): AccountCollectionAggregate
    {
        $accountCollectionAggregate = $this->load($uuid->toString());

        if (!$accountCollectionAggregate instanceof AccountCollectionAggregate) {
            throw new InvalidArgumentException('Return object should implements PageCollectionAggregate');
        }

        return $accountCollectionAggregate;
    }
}
