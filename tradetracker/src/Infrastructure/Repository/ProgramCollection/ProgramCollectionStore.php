<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Repository\ProgramCollection;

use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository\ProgramCollectionRepositoryInterface;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionStore.
 */
class ProgramCollectionStore extends EventSourcingRepository implements ProgramCollectionRepositoryInterface
{
    /**
     * ProgramCollectionStore constructor.
     *
     * {@inheritdoc}
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            ProgramCollectionAggregate::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * {@inheritdoc}
     */
    public function store(ProgramCollectionAggregate $program): void
    {
        $this->save($program);
    }

    /**
     * {@inheritdoc}
     */
    public function get(UuidInterface $uuid): ProgramCollectionAggregate
    {
        $programCollectionAggregate = $this->load($uuid->toString());

        if (!$programCollectionAggregate instanceof ProgramCollectionAggregate) {
            throw new InvalidArgumentException('Return object should implements ProgramCollectionAggregate');
        }

        return $programCollectionAggregate;
    }
}
