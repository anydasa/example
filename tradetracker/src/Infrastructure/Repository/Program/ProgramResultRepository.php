<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Repository\Program;

use AdgoalCommon\Datagate\Domain\Dto\SerializableInterface;
use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramResultRepositoryInterface;
use Enqueue\Client\ProducerInterface;

/**
 * Class ProgramResultRepository.
 */
class ProgramResultRepository implements ProgramResultRepositoryInterface
{
    public const QUEUE_MASTER_DATA = 'master.data';

    /**
     * @var ProducerInterface
     */
    private $queueProducer;

    /**
     * ProgramResultRepository constructor.
     *
     * @param ProducerInterface $queueProducer
     */
    public function __construct(ProducerInterface $queueProducer)
    {
        $this->queueProducer = $queueProducer;
    }

    /**
     * {@inheritdoc}
     */
    public function add(SerializableInterface $data): void
    {
        $this->queueProducer->sendEvent(self::QUEUE_MASTER_DATA, $data->serialize());
    }
}
