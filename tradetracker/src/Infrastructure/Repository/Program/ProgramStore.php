<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Infrastructure\Repository\Program;

use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventSourcing\EventStreamDecorator;
use Broadway\EventStore\EventStore;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramRepositoryInterface;
use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionStore.
 */
class ProgramStore extends EventSourcingRepository implements ProgramRepositoryInterface
{
    /**
     * ProgramStore constructor.
     *
     * @param EventStore             $eventStore
     * @param EventBus               $eventBus
     * @param EventStreamDecorator[] $eventStreamDecorators
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus, array $eventStreamDecorators = [])
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            ProgramAggregate::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    /**
     * {@inheritdoc}
     */
    public function store(ProgramAggregate $program): void
    {
        $this->save($program);
    }

    /**
     * {@inheritdoc}
     */
    public function get(UuidInterface $uuid): ProgramAggregate
    {
        $programAggregate = $this->load($uuid->toString());

        if (!$programAggregate instanceof ProgramAggregate) {
            throw new InvalidArgumentException('Return object should implements ProgramAggregate');
        }

        return $programAggregate;
    }
}
