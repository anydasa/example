<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\AccountCollection;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitAccountCollectionCommand.
 */
final class InitAccountCollectionCommand implements CommandInterface
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * InitAccountCollectionCommand constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
