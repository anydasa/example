<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\AccountCollection;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use AdgoalCommon\Base\Application\Factory\CommandFactoryInterface;
use AdgoalCommon\Base\Domain\Exception\FactoryException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class CommandFactory.
 */
final class CommandFactory implements CommandFactoryInterface
{
    public const ACCOUNT_COLLECTION_INIT_COMMAND = InitAccountCollectionCommand::class;

    /**
     * Make CommandBus command instance by constant type.
     *
     * @param mixed... $args
     *
     * @return CommandInterface
     *
     * @throws FactoryException
     */
    public function makeCommandInstanceByType(...$args): CommandInterface
    {
        $commandType = array_shift($args);
        $args[0] = Uuid::fromString($args[0]);

        switch ($commandType) {
            case self::ACCOUNT_COLLECTION_INIT_COMMAND:
                $class = self::ACCOUNT_COLLECTION_INIT_COMMAND;

                return new $class(...$args);

            default:
                throw new FactoryException(sprintf('Command bus for type `%s` not found!', $commandType));
        }
    }

    /**
     * @param UuidInterface $uuid
     *
     * @return InitAccountCollectionCommand
     */
    public function makeInitAccountCollectionCommand(UuidInterface $uuid): InitAccountCollectionCommand
    {
        return new InitAccountCollectionCommand($uuid);
    }
}
