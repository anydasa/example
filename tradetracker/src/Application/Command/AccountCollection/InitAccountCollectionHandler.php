<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\AccountCollection;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Factory\AccountCollectionFactory;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Repository\AccountCollectionRepositoryInterface;

/**
 * Class InitAccountCollectionHandler.
 */
final class InitAccountCollectionHandler implements CommandHandlerInterface
{
    /**
     * @var AccountCollectionFactory
     */
    private $accountCollectionFactory;

    /**
     * @var AccountCollectionRepositoryInterface
     */
    private $accountCollectionRepository;

    /**
     * @var mixed[]
     */
    private $configCollection;

    /**
     * InitAccountCollectionHandler constructor.
     *
     * @param AccountCollectionFactory             $accountCollectionFactory
     * @param AccountCollectionRepositoryInterface $accountCollectionRepository
     * @param mixed[]                              $configCollection
     */
    public function __construct(
        AccountCollectionFactory $accountCollectionFactory,
        AccountCollectionRepositoryInterface $accountCollectionRepository,
        array $configCollection
    ) {
        $this->accountCollectionFactory = $accountCollectionFactory;
        $this->accountCollectionRepository = $accountCollectionRepository;
        $this->configCollection = $configCollection;
    }

    /**
     * Handle InitAccountCollectionCommand.
     *
     * @param InitAccountCollectionCommand $command
     */
    public function handle(InitAccountCollectionCommand $command): void
    {
        $program = $this->accountCollectionFactory->makeInstance($command->getUuid(), $this->configCollection);
        $this->accountCollectionRepository->store($program);
    }
}
