<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use AdgoalCommon\Base\Application\Factory\CommandFactoryInterface;
use AdgoalCommon\Base\Domain\Exception\FactoryException;
use Ramsey\Uuid\Uuid;

/**
 * Class CommandFactory.
 */
final class CommandFactory implements CommandFactoryInterface
{
    public const INIT_PROGRAM_COMMAND = InitProgramCommand::class;
    public const CONVERT_PROGRAM_COMMAND = ConvertProgramCommand::class;
    public const SEND_PROGRAM_COMMAND = SendProgramCommand::class;

    /**
     * Make CommandBus command instance by constant type.
     *
     * @param mixed... $args
     *
     * @return CommandInterface
     *
     * @throws FactoryException
     */
    public function makeCommandInstanceByType(...$args): CommandInterface
    {
        $commandType = array_shift($args);
        $args[0] = Uuid::fromString($args[0]);

        switch ($commandType) {
            case self::INIT_PROGRAM_COMMAND:
                $class = self::INIT_PROGRAM_COMMAND;

                break;
            case self::CONVERT_PROGRAM_COMMAND:
                $class = self::CONVERT_PROGRAM_COMMAND;

                break;
            case self::SEND_PROGRAM_COMMAND:
                $class = self::SEND_PROGRAM_COMMAND;

                break;
            default:
                throw new FactoryException(sprintf('Command `%s` not found!', $commandType));
        }

        return new $class(...$args);
    }
}
