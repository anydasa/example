<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramRepositoryInterface;

/**
 * Class ConvertProgramCommandHandler.
 */
final class ConvertProgramCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramRepositoryInterface
     */
    private $programRepository;

    /**
     * RequestProgramCollectionCommandHandler constructor.
     *
     * @param ProgramRepositoryInterface $programRepository
     */
    public function __construct(ProgramRepositoryInterface $programRepository)
    {
        $this->programRepository = $programRepository;
    }

    /**
     * Handle ConvertProgramCommand. Convert Raw data to Program ValueObject.
     *
     * @param ConvertProgramCommand $command
     */
    public function handle(ConvertProgramCommand $command): void
    {
        $program = $this->programRepository->get($command->getUuid());
        $program->convert();
        $this->programRepository->store($program);
    }
}
