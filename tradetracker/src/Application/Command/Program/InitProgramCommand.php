<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramCommand.
 */
final class InitProgramCommand implements CommandInterface
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var RawProgramData
     */
    private $rawProgramData;

    /**
     * @var string
     */
    private $country;

    /**
     * InitProgramCommand constructor.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $country
     *
     * @throws InvalidRawProgramDataException
     */
    public function __construct(UuidInterface $uuid, array $rawProgramData, string $country)
    {
        $this->uuid = $uuid;
        $this->rawProgramData = RawProgramData::fromArray($rawProgramData);
        $this->country = $country;
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * Return RawProgramData.
     *
     * @return RawProgramData
     */
    public function getRawProgramData(): RawProgramData
    {
        return $this->rawProgramData;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
