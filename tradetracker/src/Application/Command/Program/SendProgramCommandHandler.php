<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use DataGate\Program\Tradetracker\Domain\Program\Exception\ProgramAggregateMapException;
use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramRepositoryInterface;
use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramResultRepositoryInterface;
use DataGate\Program\Tradetracker\Domain\Program\Service\ProgramAggregateMapper;

/**
 * Class SendProgramCommandHandler.
 */
final class SendProgramCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramRepositoryInterface
     */
    private $programRepository;

    /**
     * @var ProgramResultRepositoryInterface
     */
    private $programResultRepository;

    /**
     * @var ProgramAggregateMapper
     */
    private $programAggregateMapper;

    /**
     * SendProgramCommandHandler constructor.
     *
     * @param ProgramRepositoryInterface       $programRepository
     * @param ProgramResultRepositoryInterface $programResultRepository
     * @param ProgramAggregateMapper           $programAggregateMapper
     */
    public function __construct(
        ProgramRepositoryInterface $programRepository,
        ProgramResultRepositoryInterface $programResultRepository,
        ProgramAggregateMapper $programAggregateMapper
    ) {
        $this->programRepository = $programRepository;
        $this->programResultRepository = $programResultRepository;
        $this->programAggregateMapper = $programAggregateMapper;
    }

    /**
     * Start process to send result ProgramDto.
     *
     * @param SendProgramCommand $command
     *
     * @throws ProgramAggregateMapException
     */
    public function handle(SendProgramCommand $command): void
    {
        $program = $this->programRepository->get($command->getUuid());
        $programResultDto = $this->programAggregateMapper->mapToProgramResultDto($program);
        $this->programResultRepository->add($programResultDto);
        $program->sendResult();
        $this->programRepository->store($program);
    }
}
