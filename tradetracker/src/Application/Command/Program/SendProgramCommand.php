<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class SendProgramCommand.
 *
 * @category Application\Command
 * @sub-package Program
 */
final class SendProgramCommand implements CommandInterface
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * RequestProgramCollectionCommand constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
