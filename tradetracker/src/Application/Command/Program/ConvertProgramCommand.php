<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ConvertProgramCommand.
 */
final class ConvertProgramCommand implements CommandInterface
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * ConvertProgramCommand constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
