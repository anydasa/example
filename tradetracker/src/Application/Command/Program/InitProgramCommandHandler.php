<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\Program;

use DataGate\Program\Tradetracker\Domain\Program\Factory\ProgramFactory;
use DataGate\Program\Tradetracker\Domain\Program\Repository\ProgramRepositoryInterface;

/**
 * Class InitProgramCommandHandler.
 */
final class InitProgramCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramFactory
     */
    private $programFactory;

    /**
     * @var ProgramRepositoryInterface
     */
    private $programRepository;

    /**
     * InitProgramCommandHandler constructor.
     *
     * @param ProgramFactory             $programFactory
     * @param ProgramRepositoryInterface $programRepository
     */
    public function __construct(ProgramFactory $programFactory, ProgramRepositoryInterface $programRepository)
    {
        $this->programFactory = $programFactory;
        $this->programRepository = $programRepository;
    }

    /**
     * Handle InitProgramCommand.
     *
     * @param InitProgramCommand $command
     */
    public function handle(InitProgramCommand $command): void
    {
        $program = $this->programFactory->makeInstance(
            $command->getUuid(),
            $command->getRawProgramData(),
            $command->getCountry()
        );
        $this->programRepository->store($program);
    }
}
