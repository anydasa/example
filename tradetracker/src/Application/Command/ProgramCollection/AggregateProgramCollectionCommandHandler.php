<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository\ProgramCollectionRepositoryInterface;

/**
 * Class AggregateProgramCollectionCommandHandler.
 */
final class AggregateProgramCollectionCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramCollectionRepositoryInterface
     */
    private $programCollectionRepository;

    /**
     * AggregateProgramCollectionCommandHandler constructor.
     *
     * @param ProgramCollectionRepositoryInterface $programCollectionRepository
     */
    public function __construct(ProgramCollectionRepositoryInterface $programCollectionRepository)
    {
        $this->programCollectionRepository = $programCollectionRepository;
    }

    /**
     * Handle AggregateProgramCollectionCommand.
     *
     * @param AggregateProgramCollectionCommand $command
     */
    public function handle(AggregateProgramCollectionCommand $command): void
    {
        $programCollection = $this->programCollectionRepository->get($command->getUuid());
        $programCollection->aggregate();
        $this->programCollectionRepository->store($programCollection);
    }
}
