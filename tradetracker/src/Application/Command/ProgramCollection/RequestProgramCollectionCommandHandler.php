<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Domain\ProgramCollection\Exception\InvalidResponsePayloadException;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Query\ProgramCollectionQueryInterface;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository\ProgramCollectionRepositoryInterface;

/**
 * Class RequestProgramCollectionCommandHandler.
 */
final class RequestProgramCollectionCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramCollectionRepositoryInterface
     */
    private $programCollectionRepository;

    /**
     * @var ProgramCollectionQueryInterface
     */
    private $programCollectionQuery;

    /**
     * RequestProgramCollectionCommandHandler constructor.
     *
     * @param ProgramCollectionRepositoryInterface $programCollectionRepository
     * @param ProgramCollectionQueryInterface      $programCollectionQuery
     */
    public function __construct(
        ProgramCollectionRepositoryInterface $programCollectionRepository,
        ProgramCollectionQueryInterface $programCollectionQuery
    ) {
        $this->programCollectionRepository = $programCollectionRepository;
        $this->programCollectionQuery = $programCollectionQuery;
    }

    /**
     * Handle RequestProgramCollectionCommand.
     *
     * @param RequestProgramCollectionCommand $command
     *
     * @throws InvalidResponsePayloadException
     */
    public function handle(RequestProgramCollectionCommand $command): void
    {
        $programCollection = $this->programCollectionRepository->get($command->getUuid());
        $data = $this->programCollectionQuery->fetchPrograms($programCollection->getAccountConfig());
        $programCollection->setRequestedData($data);
        $this->programCollectionRepository->store($programCollection);
    }
}
