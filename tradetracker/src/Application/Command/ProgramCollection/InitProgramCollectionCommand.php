<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramCollectionCommand.
 */
final class InitProgramCollectionCommand implements CommandInterface
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var AccountConfigDto
     */
    private $accountConfig;

    /**
     * InitProgramCollectionCommand constructor.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function __construct(UuidInterface $uuid, array $accountConfig)
    {
        $this->uuid = $uuid;
        $this->accountConfig = AccountConfigDto::fromArray($accountConfig);
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return AccountConfigDto
     */
    public function getAccountConfig(): AccountConfigDto
    {
        return $this->accountConfig;
    }
}
