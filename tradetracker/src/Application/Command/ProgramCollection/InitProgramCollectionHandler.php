<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Domain\ProgramCollection\Factory\ProgramCollectionFactory;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository\ProgramCollectionRepositoryInterface;

/**
 * Class InitProgramCommandHandler.
 *
 * @category    Application\Command
 */
final class InitProgramCollectionHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramCollectionFactory
     */
    private $programCollectionFactory;

    /**
     * @var ProgramCollectionRepositoryInterface
     */
    private $programRepository;

    /**
     * InitProgramCommandHandler constructor.
     *
     * @param ProgramCollectionFactory             $programCollectionFactory
     * @param ProgramCollectionRepositoryInterface $programRepository
     */
    public function __construct(ProgramCollectionFactory $programCollectionFactory, ProgramCollectionRepositoryInterface $programRepository)
    {
        $this->programCollectionFactory = $programCollectionFactory;
        $this->programRepository = $programRepository;
    }

    /**
     * Start affiliate network update process.
     *
     * @param InitProgramCollectionCommand $command
     */
    public function handle(InitProgramCollectionCommand $command): void
    {
        $programCollection = $this->programCollectionFactory->makeInstance(
            $command->getUuid(),
            $command->getAccountConfig()
        );

        $this->programRepository->store($programCollection);
    }
}
