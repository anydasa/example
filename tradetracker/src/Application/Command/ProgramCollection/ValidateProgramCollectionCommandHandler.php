<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Domain\ProgramCollection\Exception\InvalidResponsePayloadException;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository\ProgramCollectionRepositoryInterface;

/**
 * Class ValidateProgramCollectionCommandHandler.
 */
final class ValidateProgramCollectionCommandHandler implements CommandHandlerInterface
{
    /**
     * @var ProgramCollectionRepositoryInterface
     */
    private $programRepository;

    /**
     * ValidateProgramCollectionCommandHandler constructor.
     *
     * @param ProgramCollectionRepositoryInterface $programRepository
     */
    public function __construct(ProgramCollectionRepositoryInterface $programRepository)
    {
        $this->programRepository = $programRepository;
    }

    /**
     * Handle ValidateProgramCollectionCommand.
     *
     * @param ValidateProgramCollectionCommand $command
     *
     * @throws InvalidResponsePayloadException
     */
    public function handle(ValidateProgramCollectionCommand $command): void
    {
        $programCollection = $this->programRepository->get($command->getUuid());
        $programCollection->validate();
        $this->programRepository->store($programCollection);
    }
}
