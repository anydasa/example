<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application\Command\ProgramCollection;

use AdgoalCommon\Base\Application\Command\CommandInterface;
use AdgoalCommon\Base\Application\Factory\CommandFactoryInterface;
use AdgoalCommon\Base\Domain\Exception\FactoryException;
use Ramsey\Uuid\Uuid;

/**
 * Class CommandFactory.
 */
final class CommandFactory implements CommandFactoryInterface
{
    public const INIT_PROGRAM_COLLECTION_COMMAND = InitProgramCollectionCommand::class;
    public const VALIDATE_PROGRAM_COLLECTION_COMMAND = ValidateProgramCollectionCommand::class;
    public const AGGREGATE_PROGRAM_COLLECTION_COMMAND = AggregateProgramCollectionCommand::class;
    public const REQUEST_PROGRAM_COLLECTION_COMMAND = RequestProgramCollectionCommand::class;

    /**
     * Make CommandBus command instance by constant type.
     *
     * @param mixed... $args
     *
     * @return CommandInterface
     *
     * @throws FactoryException
     */
    public function makeCommandInstanceByType(...$args): CommandInterface
    {
        $commandType = array_shift($args);
        $args[0] = Uuid::fromString($args[0]);

        switch ($commandType) {
            case self::INIT_PROGRAM_COLLECTION_COMMAND:
                $class = self::INIT_PROGRAM_COLLECTION_COMMAND;

                break;
            case self::VALIDATE_PROGRAM_COLLECTION_COMMAND:
                $class = self::VALIDATE_PROGRAM_COLLECTION_COMMAND;

                break;
            case self::AGGREGATE_PROGRAM_COLLECTION_COMMAND:
                $class = self::AGGREGATE_PROGRAM_COLLECTION_COMMAND;

                break;
            case self::REQUEST_PROGRAM_COLLECTION_COMMAND:
                $class = self::REQUEST_PROGRAM_COLLECTION_COMMAND;

                break;
            default:
                throw new FactoryException(sprintf('Command `%s` not found!', $commandType));
        }

        return new $class(...$args);
    }
}
