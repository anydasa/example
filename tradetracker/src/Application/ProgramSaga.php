<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application;

use AdgoalCommon\Datagate\Domain\Monitoring\MonitoringSagaInterface;
use AdgoalCommon\Monitoring\StatsdTrait;
use AdgoalCommon\Saga\AbstractSaga;
use Broadway\Saga\Metadata\StaticallyConfiguredSagaInterface;
use Broadway\Saga\State;
use Broadway\Saga\State\Criteria;
use Closure;
use DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand;
use DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasSentEvent;
use League\Tactician\CommandBus;
use Throwable;

/**
 * Class ProgramSaga.
 *
 * @SuppressWarnings(PHPMD)
 */
class ProgramSaga extends AbstractSaga implements StaticallyConfiguredSagaInterface
{
    use StatsdTrait;

    private const STATE_CRITERIA_KEY = 'programId';

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ProgramSaga constructor.
     *
     * @param CommandBus $programBus
     */
    public function __construct(CommandBus $programBus)
    {
        $this->commandBus = $programBus;
    }

    /**
     * Return saga configuration with events that need to be applied.
     *
     * @return Closure[]
     */
    public static function configuration(): array
    {
        return [
            'ProgramWasInitEvent' => static function () {
                return null; // no criteria, start of a new saga
            },

            'ProgramWasConvertedEvent' => static function (ProgramWasConvertedEvent $event): Criteria {
                return new Criteria([self::STATE_CRITERIA_KEY => $event->getUuid()]);
            },

            'ProgramWasSentEvent' => static function (ProgramWasSentEvent $event): Criteria {
                return new Criteria([self::STATE_CRITERIA_KEY => $event->getUuid()]);
            },
        ];
    }

    /**
     * @param ProgramWasInitEvent $event
     * @param State $state
     *
     * @return State
     */
    public function handleProgramWasInitEvent(
        State $state,
        ProgramWasInitEvent $event
    ): State {
        $uuid = $event->getUuid();

        $state->set(self::STATE_CRITERIA_KEY, $uuid);
        $this->statsd->increment(MonitoringSagaInterface::MONITORING_SAGA_PROGRAM_STARTED);
        $command = new ConvertProgramCommand($uuid);

        try {
            $this->commandBus->handle($command);
        } catch (Throwable $e) {
            $this->statsd->increment(MonitoringSagaInterface::MONITORING_SAGA_PROGRAM_FAILED);
            $state->setDone();
        }

        return $state;
    }

    /**
     * @param ProgramWasConvertedEvent $event
     * @param State $state
     *
     * @return State
     */
    public function handleProgramWasConvertedEvent(
        State $state,
        ProgramWasConvertedEvent $event
    ): State {
        $uuid = $event->getUuid();

        $command = new SendProgramCommand($uuid);

        try {
            $this->commandBus->handle($command);
        } catch (Throwable $e) {
            $this->statsd->increment(MonitoringSagaInterface::MONITORING_SAGA_PROGRAM_FAILED);
            $state->setDone();
        }

        return $state;
    }

    /**
     * @param State $state
     *
     * @return State
     */
    public function handleProgramWasSentEvent(State $state): State {
        $this->statsd->increment(MonitoringSagaInterface::MONITORING_SAGA_PROGRAM_SUCCEEDED);
        $state->setDone();

        return $state;
    }
}
