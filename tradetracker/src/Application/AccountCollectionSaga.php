<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application;

use AdgoalCommon\Saga\AbstractSaga;
use Broadway\Saga\Metadata\StaticallyConfiguredSagaInterface;
use Broadway\Saga\State;
use Closure;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use Exception;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;

/**
 * Class AccountCollectionSaga.
 *
 * @SuppressWarnings(PHPMD)
 */
class AccountCollectionSaga extends AbstractSaga implements StaticallyConfiguredSagaInterface
{
    private const STATE_ACCOUNT_COLLECTION_KEY = 'accountCollectionId';

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * AccountCollectionSaga constructor.
     *
     * @param CommandBus $programCollectionBus
     */
    public function __construct(CommandBus $programCollectionBus)
    {
        $this->commandBus = $programCollectionBus;
    }

    /**
     * Return saga configuration with events that need to be applied.
     *
     * @return Closure[]
     */
    public static function configuration(): array
    {
        return [
            'AccountCollectionWasStartedEvent' => static function () {
                return null; // no criteria, start of a new saga
            },
        ];
    }

    /**
     * @param AccountCollectionWasStartedEvent $event
     * @param State $state
     *
     * @return State
     *
     * @throws Exception
     */
    public function handleAccountCollectionWasStartedEvent(
        State $state,
        AccountCollectionWasStartedEvent $event
    ): State {
        $state->set(self::STATE_ACCOUNT_COLLECTION_KEY, $event->getUuid());

        foreach ($event->getConfigCollection() as $accountConfig) {
            $command = new InitProgramCollectionCommand(
                Uuid::uuid4(),
                $accountConfig
            );
            $this->commandBus->handle($command);
        }

        $state->setDone();

        return $state;
    }
}
