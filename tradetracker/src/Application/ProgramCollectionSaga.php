<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Application;

use AdgoalCommon\Datagate\Domain\Monitoring\MonitoringSagaInterface;
use AdgoalCommon\Monitoring\StatsdTrait;
use AdgoalCommon\Saga\AbstractSaga;
use Broadway\Saga\Metadata\StaticallyConfiguredSagaInterface;
use Broadway\Saga\State;
use Broadway\Saga\State\Criteria;
use Closure;
use DataGate\Program\Tradetracker\Application\Command\Program\InitProgramCommand;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use Exception;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * Class ProgramCollectionSaga.
 *
 * @SuppressWarnings(PHPMD)
 */
final class ProgramCollectionSaga extends AbstractSaga implements StaticallyConfiguredSagaInterface
{
    use StatsdTrait;

    private const STATE_CRITERIA_KEY = 'programCollectionId';

    /**
     * @var CommandBus
     */
    private $programCollectionBus;

    /**
     * @var CommandBus
     */
    private $programBus;

    /**
     * ProgramCollectionSaga constructor.
     *
     * @param CommandBus $programCollectionBus
     * @param CommandBus $programBus
     */
    public function __construct(CommandBus $programCollectionBus, CommandBus $programBus)
    {
        $this->programCollectionBus = $programCollectionBus;
        $this->programBus = $programBus;
    }

    /**
     * Return saga configuration with events that need to be applied.
     *
     * @return Closure[]
     */
    public static function configuration(): array
    {
        return [
            'ProgramCollectionWasInitEvent'          => static function () {
                return null; // no criteria, start of a new saga
            },
            'ProgramCollectionDataWasRequestedEvent' => static function (ProgramCollectionDataWasRequestedEvent $event
            ): Criteria {
                return new Criteria(
                    [
                        self::STATE_CRITERIA_KEY => $event->getUuid()->toString(),
                    ]
                );
            },
            'ProgramCollectionDataWasValidatedEvent' => static function (ProgramCollectionDataWasValidatedEvent $event
            ): Criteria {
                return new Criteria(
                    [
                        self::STATE_CRITERIA_KEY => $event->getUuid()->toString(),
                    ]
                );
            },
            'ProgramCollectionWasAggregatedEvent'    => static function (ProgramCollectionWasAggregatedEvent $event
            ): Criteria {
                return new Criteria(
                    [
                        self::STATE_CRITERIA_KEY => $event->getUuid()->toString(),
                    ]
                );
            },
        ];
    }

    /**
     * Handle ProgramCollectionWasInitEvent event, initialize and handle RequestProgramCollectionCommand.
     *
     * @param ProgramCollectionWasInitEvent $event
     * @param State $state
     *
     * @return State
     */
    public function handleProgramCollectionWasInitEvent(
        State $state,
        ProgramCollectionWasInitEvent $event
    ): State {
        $state->set(self::STATE_CRITERIA_KEY, $event->getUuid()->toString());

        $command = new RequestProgramCollectionCommand($event->getUuid());

        try {
            $this->programCollectionBus->handle($command);
        } catch (Throwable $e) {
            $state->setDone();
        }

        return $state;
    }

    /**
     * Handle ProgramCollectionWasRequested event, initialize and handle ValidateProgramCollectionCommand.
     *
     * @param ProgramCollectionDataWasRequestedEvent $event
     * @param State $state
     *
     * @return State
     */
    public function handleProgramCollectionDataWasRequestedEvent(
        State $state,
        ProgramCollectionDataWasRequestedEvent $event
    ): State {
        $command = new ValidateProgramCollectionCommand($event->getUuid());

        try {
            $this->programCollectionBus->handle($command);
        } catch (Throwable $e) {
            $state->setDone();
        }

        return $state;
    }

    /**
     * Handle ProgramCollectionWasValidated event, initialize and handle AggregateProgramCollectionCommand.
     *
     * @param ProgramCollectionDataWasValidatedEvent $event
     * @param State $state
     *
     * @return State
     */
    public function handleProgramCollectionDataWasValidatedEvent(
        State $state,
        ProgramCollectionDataWasValidatedEvent $event
    ): State {
        $command = new AggregateProgramCollectionCommand($event->getUuid());

        try {
            $this->programCollectionBus->handle($command);
        } catch (Throwable $e) {
            $state->setDone();
        }

        return $state;
    }

    /**
     * Handle ProgramCollectionWasAggregated event, initialize and handle AggregateProgramCollectionCommand.
     *
     * @param ProgramCollectionWasAggregatedEvent $event
     * @param State $state
     *
     * @return State
     *
     * @throws Exception
     * @throws InvalidRawProgramDataException
     */
    public function handleProgramCollectionWasAggregatedEvent(
        State $state,
        ProgramCollectionWasAggregatedEvent $event
    ): State {
        $data = $event->getRequestedData();
        $this->statsd->count(MonitoringSagaInterface::MONITORING_SAGA_PROGRAM_RECEIVED, count($data));

        foreach ($data as $rawProgramData) {
            $uuid = Uuid::uuid4();
            $command = new InitProgramCommand($uuid, $rawProgramData, $event->getCountry());
            $this->programBus->handle($command);
        }

        $state->setDone();

        return $state;
    }
}
