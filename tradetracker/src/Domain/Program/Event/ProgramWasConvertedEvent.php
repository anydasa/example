<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramWasConvertedEvent.
 */
class ProgramWasConvertedEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * ProgramWasConvertedEvent constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramWasConvertedEvent
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');

        return new static(
            Uuid::fromString($data['uuid'])
        );
    }

    /**
     * Serialize Event.
     *
     * @return string[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
