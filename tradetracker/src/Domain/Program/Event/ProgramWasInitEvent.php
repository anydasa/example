<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramWasInitEvent.
 */
class ProgramWasInitEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var RawProgramData
     */
    private $rawProgramData;

    /**
     * @var string
     */
    private $country;

    /**
     * ProgramWasInitEvent constructor.
     *
     * @param UuidInterface  $uuid
     * @param RawProgramData $rawProgramData
     * @param string         $country
     */
    public function __construct(UuidInterface $uuid, RawProgramData $rawProgramData, string $country)
    {
        $this->uuid = $uuid;
        $this->rawProgramData = $rawProgramData;
        $this->country = $country;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramWasInitEvent
     *
     * @throws InvalidRawProgramDataException
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'rawProgramData');
        Assertion::keyExists($data, 'country');

        return new static(
            Uuid::fromString($data['uuid']),
            RawProgramData::fromArray($data['rawProgramData']),
            $data['country']
        );
    }

    /**
     * Serialize Event.
     *
     * @return mixed[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
            'rawProgramData' => $this->getRawProgramData()->toArray(),
            'country' => $this->country,
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return RawProgramData
     */
    public function getRawProgramData(): RawProgramData
    {
        return $this->rawProgramData;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
