<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramWasSentEvent.
 */
final class ProgramWasSentEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * ProgramWasSentEvent constructor.
     *
     * @param UuidInterface $uuid
     */
    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramWasSentEvent
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');

        $uuid = Uuid::fromString($data['uuid']);

        return new static($uuid);
    }

    /**
     * Serialize Event.
     *
     * @return mixed[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
