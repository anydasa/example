<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Exception;

use AdgoalCommon\Base\Domain\Exception\ErrorException;

/**
 * Class ProgramValidationException.
 */
class ProgramValidationException extends ErrorException
{
}
