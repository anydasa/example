<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Entity;

use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasSentEvent;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\Program;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramAggregate.
 */
class ProgramAggregate extends EventSourcedAggregateRoot
{
    public const RELATIONSHIP_STATUS_ACCEPTED = 'accepted';
    public const RELATIONSHIP_STATUS_REJECTED = 'rejected';
    public const RELATIONSHIP_STATUS_ON_HOLD = 'onhold';
    public const RELATIONSHIP_STATUS_NOT_SIGNED_UP = 'notsignedup';
    public const RELATIONSHIP_STATUS_PENDING = 'pending';

    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var RawProgramData
     */
    private $rawData;

    /**
     * @var Program
     */
    private $program;

    /**
     * @var string
     */
    private $country;

    /**
     * Create ProgramAggregate.
     *
     * @param UuidInterface  $uuid
     * @param RawProgramData $rawProgramData
     * @param string         $county
     *
     * @return ProgramAggregate
     */
    public static function create(UuidInterface $uuid, RawProgramData $rawProgramData, string $county): self
    {
        $program = new self();
        $program->apply(new ProgramWasInitEvent($uuid, $rawProgramData, $county));

        return $program;
    }

    /**
     * Get Aggregate Root Id.
     *
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    /**
     * Convert raw data to program ValueObject.
     */
    public function convert(): void
    {
        $this->apply(new ProgramWasConvertedEvent($this->uuid));
    }

    /**
     * Send result.
     */
    public function sendResult(): void
    {
        $this->apply(new ProgramWasSentEvent($this->uuid));
    }

    /**
     * Get uuid.
     *
     * @return string
     */
    public function uuid(): string
    {
        return $this->uuid->toString();
    }

    /**
     * Get Raw data as RawProgramData.
     *
     * @return RawProgramData
     */
    public function getRawData(): RawProgramData
    {
        return $this->rawData;
    }

    /**
     * @return Program
     */
    public function getProgram(): Program
    {
        return $this->program;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Apply ProgramWasInitEvent.
     *
     * @param ProgramWasInitEvent $event
     */
    protected function applyProgramWasInitEvent(ProgramWasInitEvent $event): void
    {
        $this->uuid = $event->getUuid();
        $this->rawData = $event->getRawProgramData();
        $this->country = $event->getCountry();
    }

    /**
     * Apply ProgramWasConvertedEvent.
     */
    protected function applyProgramWasConvertedEvent(): void
    {
        $this->program = Program::fromArray($this->rawData->toArray());
    }
}
