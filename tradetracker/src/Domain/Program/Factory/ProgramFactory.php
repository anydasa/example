<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Factory;

use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramFactory.
 */
class ProgramFactory
{
    /**
     * Make Program.
     *
     * @param UuidInterface  $uuid
     * @param RawProgramData $rawProgramData
     * @param string         $country
     *
     * @return ProgramAggregate
     */
    public function makeInstance(UuidInterface $uuid, RawProgramData $rawProgramData, string $country): ProgramAggregate
    {
        return ProgramAggregate::create($uuid, $rawProgramData, $country);
    }
}
