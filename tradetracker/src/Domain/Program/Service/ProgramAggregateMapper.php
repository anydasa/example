<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Service;

use AdgoalCommon\Datagate\Domain\Dto\ProgramResultDto;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Exception\ProgramAggregateMapException;

/**
 * Class ProgramAggregateMapper.
 */
class ProgramAggregateMapper
{
    private const RELATIONSHIP_STATUS_MAP = [
        ProgramAggregate::RELATIONSHIP_STATUS_ACCEPTED => ProgramResultDto::RELATIONSHIP_STATUS_JOINED,
        ProgramAggregate::RELATIONSHIP_STATUS_REJECTED => ProgramResultDto::RELATIONSHIP_STATUS_DECLINED,
        ProgramAggregate::RELATIONSHIP_STATUS_PENDING => ProgramResultDto::RELATIONSHIP_STATUS_PENDING,
        ProgramAggregate::RELATIONSHIP_STATUS_ON_HOLD => ProgramResultDto::RELATIONSHIP_STATUS_NOT_JOINED,
        ProgramAggregate::RELATIONSHIP_STATUS_NOT_SIGNED_UP => ProgramResultDto::RELATIONSHIP_STATUS_NOT_JOINED,
    ];

    /**
     * @param ProgramAggregate $programAggregate
     *
     * @return ProgramResultDto
     *
     * @throws ProgramAggregateMapException
     */
    public function mapToProgramResultDto(ProgramAggregate $programAggregate): ProgramResultDto
    {
        $program = $programAggregate->getProgram();

        $programResultDto = new ProgramResultDto(
            (string) $program->getNetwork(),
            (string) $program->getId(),
            (string) $program->getName(),
            'active',
            $this->getMappedRelationshipStatus((string) $program->getAssignmentStatus())
        );

        $programResultDto->setProgramUrl((string) $program->getUrl());
        $programResultDto->setShopDeeplink($program->getTrackingUrl()->toNative());
        $programResultDto->setProgramLogo((string) $program->getImageUrl());
        $programResultDto->setSupportedCountries([$programAggregate->getCountry()]);

        return $programResultDto;
    }

    /**
     * @param string $originRelationshipStatus
     *
     * @return string
     *
     * @throws ProgramAggregateMapException
     */
    private function getMappedRelationshipStatus(string $originRelationshipStatus): string
    {
        if (array_key_exists($originRelationshipStatus, self::RELATIONSHIP_STATUS_MAP)) {
            return self::RELATIONSHIP_STATUS_MAP[$originRelationshipStatus];
        }

        throw new ProgramAggregateMapException(
            "Unknown Relationship Status from Tradetracker Network $originRelationshipStatus"
        );
    }
}
