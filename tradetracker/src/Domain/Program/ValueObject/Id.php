<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

/**
 * Class Id.
 */
class Id
{
    /**
     * @var string
     */
    private $id;

    /**
     * Id constructor.
     *
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * Create Id object from string.
     *
     * @param string $id
     *
     * @return Id
     */
    public static function fromString(string $id): self
    {
        return new self($id);
    }

    /**
     * Convert Id object to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->id;
    }
}
