<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

/**
 * Class Name.
 *
 * @SuppressWarnings(PHPMD)
 */
class Name
{
    /**
     * @var string
     */
    private $name;

    /**
     * Url constructor.
     *
     * @param string $name
     */
    private function __construct(string $name)
    {
        $this->name = trim($name);
    }

    /**
     * Validate, correct and build Url object from string.
     *
     *
     * @param string $name
     *
     * @return Name
     */
    public static function fromString(string $name): self
    {
        return new self($name);
    }

    /**
     * Convert Url object to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
