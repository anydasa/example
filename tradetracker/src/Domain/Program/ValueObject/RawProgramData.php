<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

use DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException;
use stdClass;

/**
 * Class RawProgramData.
 */
final class RawProgramData
{
    /**
     * @var stdClass
     */
    private $object;

    /**
     * Collection constructor.
     *
     * @param stdClass $object
     */
    private function __construct(stdClass $object)
    {
        $this->object = $object;
    }

    /**
     * Build Collection object from array.
     *
     * @param mixed[] $data
     *
     * @return RawProgramData
     *
     * @throws InvalidRawProgramDataException
     */
    public static function fromArray(array $data): self
    {
        $jsonData = json_encode($data);

        if (false === $jsonData) {
            throw new InvalidRawProgramDataException('Json encode return false!');
        }

        $stdClassObject = json_decode($jsonData);

        return new static($stdClassObject);
    }

    /**
     * Convert RawProgramData object to array.
     *
     * @return mixed[]
     */
    public function toArray(): array
    {
        return json_decode((string) json_encode($this->object), true);
    }
}
