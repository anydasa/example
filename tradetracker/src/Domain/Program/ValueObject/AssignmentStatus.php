<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

use Assert\Assertion;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;

/**
 * Class AssignmentStatus.
 */
class AssignmentStatus
{
    private const ALLOWED_STATUSES = [
        ProgramAggregate::RELATIONSHIP_STATUS_ACCEPTED,
        ProgramAggregate::RELATIONSHIP_STATUS_REJECTED,
        ProgramAggregate::RELATIONSHIP_STATUS_NOT_SIGNED_UP,
        ProgramAggregate::RELATIONSHIP_STATUS_ON_HOLD,
        ProgramAggregate::RELATIONSHIP_STATUS_PENDING,
    ];

    /**
     * @var string
     */
    private $assignmentStatus;

    /**
     * AssignmentStatus constructor.
     *
     * @param string $assignmentStatus
     */
    private function __construct(string $assignmentStatus)
    {
        Assertion::inArray(
            $assignmentStatus,
            self::ALLOWED_STATUSES,
            sprintf('Assignment Status %s Not allowed', $assignmentStatus)
        );

        $this->assignmentStatus = $assignmentStatus;
    }

    /**
     * Create AssignmentStatus object from string.
     *
     * @param string $assignmentStatus
     *
     * @return AssignmentStatus
     */
    public static function fromString(string $assignmentStatus): self
    {
        return new self($assignmentStatus);
    }

    /**
     * Convert AssignmentStatus object to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->assignmentStatus;
    }
}
