<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

/**
 * Class Network.
 */
class Network
{
    private const NETWORK = 'TRADETRACKER';

    /**
     * @var string
     */
    private $network;

    /**
     * Network constructor.
     *
     * @param string $network
     */
    private function __construct(string $network)
    {
        $this->network = $network;
    }

    /**
     * @return Network
     */
    public static function init(): self
    {
        return new self(self::NETWORK);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->network;
    }
}
