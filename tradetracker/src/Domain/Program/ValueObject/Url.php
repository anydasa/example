<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

/**
 * Class Url.
 */
class Url
{
    /**
     * @var string
     */
    private $url;

    /**
     * Url constructor.
     *
     * @param string $url
     */
    private function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Validate, correct and build Url object from string.
     *
     * @param string $url
     *
     * @return Url
     */
    public static function fromString(string $url): self
    {
        $correctedUrl = self::correctUrl($url);

        return new self($correctedUrl);
    }

    /**
     * Add slashes to end of domain in URL.
     *
     * @param string $url
     *
     * @return string
     */
    private static function correctUrl(string $url): string
    {
        $replacements = [
            '.de?' => '.de/?',
            '.at?' => '.at/?',
            '.ch?' => '.ch/?',
            '.nl?' => '.nl/?',
            '.fr?' => '.fr/?',
            '.it?' => '.it/?',
            '.dk?' => '.dk/?',
            '.es?' => '.es/?',
            '.no?' => '.no/?',
            '.se?' => '.se/?',
            '.fi?' => '.fi/?',
            '.pt?' => '.pt/?',
            '.pl?' => '.pl/?',
            '.hu?' => '.hu/?',
            '.cz?' => '.cz/?',
            '.eu?' => '.eu/?',
            '.co.uk?' => '.co.uk/?',
            '.net?' => '.net/?',
            '.com?' => '.com/?',
            '.org?' => '.org/?',
            '.info?' => '.info/?',
            '.us?' => '.us/?',
        ];

        return str_replace(array_keys($replacements), array_values($replacements), $url);
    }

    /**
     * Convert Url object to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->url;
    }
}
