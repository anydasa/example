<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

use Assert\Assertion;

/**
 * Class Program.
 */
final class Program
{
    /**
     * @var Network
     */
    private $network;

    /**
     * @var Id
     */
    private $id;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var Name
     */
    private $name;

    /**
     * @var AssignmentStatus
     */
    private $assignmentStatus;

    /**
     * @var TrackingUrl
     */
    private $trackingUrl;

    /**
     * @var ImageUrl
     */
    private $imageUrl;

    /**
     * Program constructor.
     *
     * @param Network          $network
     * @param Id               $id
     * @param Url              $url
     * @param Name             $name
     * @param AssignmentStatus $assignmentStatus
     * @param TrackingUrl      $trackingUrl
     * @param ImageUrl         $imageUrl
     */
    private function __construct(
        Network $network,
        Id $id,
        Url $url,
        Name $name,
        AssignmentStatus $assignmentStatus,
        TrackingUrl $trackingUrl,
        ImageUrl $imageUrl
    ) {
        $this->network = $network;
        $this->url = $url;
        $this->name = $name;
        $this->id = $id;
        $this->assignmentStatus = $assignmentStatus;
        $this->trackingUrl = $trackingUrl;
        $this->imageUrl = $imageUrl;
    }

    /**
     * Make Program from array.
     *
     * @param mixed[] $data
     *
     * @return Program
     */
    public static function fromArray(array $data): self
    {
        Assertion::keyExists($data, 'ID');
        Assertion::keyExists($data, 'URL');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'info');
        Assertion::isArray($data['info']);
        Assertion::keyExists($data['info'], 'assignmentStatus');
        Assertion::keyExists($data['info'], 'trackingURL');
        Assertion::keyExists($data['info'], 'imageURL');

        return new self(
            Network::init(),
            Id::fromString((string) $data['ID']),
            Url::fromString($data['URL']),
            Name::fromString($data['name']),
            AssignmentStatus::fromString($data['info']['assignmentStatus']),
            TrackingUrl::fromNative($data['info']['trackingURL']),
            ImageUrl::fromString($data['info']['imageURL'])
        );
    }

    /**
     * Get Url.
     *
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * Get Name.
     *
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * Get Network code.
     *
     * @return Network
     */
    public function getNetwork(): Network
    {
        return $this->network;
    }

    /**
     * Get Id.
     *
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Get Assignment status.
     *
     * @return AssignmentStatus
     */
    public function getAssignmentStatus(): AssignmentStatus
    {
        return $this->assignmentStatus;
    }

    /**
     * @return TrackingUrl
     */
    public function getTrackingUrl(): TrackingUrl
    {
        return $this->trackingUrl;
    }

    /**
     * @return ImageUrl
     */
    public function getImageUrl(): ImageUrl
    {
        return $this->imageUrl;
    }
}
