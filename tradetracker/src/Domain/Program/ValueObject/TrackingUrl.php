<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

use Assert\Assertion;

/**
 * Class TrackingUrl.
 */
class TrackingUrl
{
    /**
     * @var string|null
     */
    private $url;

    /**
     * TrackingUrl constructor.
     *
     * @param string|null $url
     */
    private function __construct(?string $url)
    {
        $this->url = $url;
    }

    /**
     * Validate, correct and build TrackingUrl object from string.
     *
     * @param mixed $url
     *
     * @return TrackingUrl
     */
    public static function fromNative($url): self
    {
        if (null !== $url) {
            Assertion::url($url, 'Not a valid TrackingUrl');
        }

        return new self($url);
    }

    /**
     * @return string|null
     */
    public function toNative(): ?string
    {
        return $this->url;
    }
}
