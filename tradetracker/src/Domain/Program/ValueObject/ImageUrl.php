<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\ValueObject;

use Assert\Assertion;

/**
 * Class ImageUrl.
 */
class ImageUrl
{
    /**
     * @var string
     */
    private $url;

    /**
     * Url constructor.
     *
     * @param string $url
     */
    private function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Validate, correct and build ImageUrl object from string.
     *
     * @param string $url
     *
     * @return ImageUrl
     */
    public static function fromString(string $url): self
    {
        Assertion::url($url, 'Not a valid ImageUrl');

        return new self($url);
    }

    /**
     * Convert Url object to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->url;
    }
}
