<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Repository;

use AdgoalCommon\Datagate\Domain\Dto\SerializableInterface;

/**
 * Interface ProgramResultRepositoryInterface.
 */
interface ProgramResultRepositoryInterface
{
    /**
     * Add Program Result to queue.
     *
     * @param SerializableInterface $data
     */
    public function add(SerializableInterface $data): void;
}
