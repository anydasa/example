<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\Program\Repository;

use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface ProgramRepositoryInterface.
 */
interface ProgramRepositoryInterface
{
    /**
     * Get ProgramAggregate with current state.
     *
     * @param UuidInterface $uuid
     *
     * @return ProgramAggregate
     */
    public function get(UuidInterface $uuid): ProgramAggregate;

    /**
     * Store Events for ProgramAggregate.
     *
     * @param ProgramAggregate $program
     */
    public function store(ProgramAggregate $program): void;
}
