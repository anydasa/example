<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\AccountCollection\Entity;

use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionAggregate.
 *
 * @SuppressWarnings(PHPMD)
 */
class AccountCollectionAggregate extends EventSourcedAggregateRoot
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var mixed[]
     */
    private $configCollection;

    /**
     * @param UuidInterface $uuid
     * @param mixed[]       $data
     *
     * @return AccountCollectionAggregate
     */
    public static function create(UuidInterface $uuid, array $data): self
    {
        $program = new self();
        $program->apply(new AccountCollectionWasStartedEvent($uuid, $data));

        return $program;
    }

    /**
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    /**
     * @return string
     */
    public function uuid(): string
    {
        return $this->uuid->toString();
    }

    /**
     * @return mixed[]
     */
    public function getConfigCollection(): array
    {
        return $this->configCollection;
    }

    /**
     * @param AccountCollectionWasStartedEvent $event
     */
    protected function applyAccountCollectionWasStartedEvent(AccountCollectionWasStartedEvent $event): void
    {
        $this->uuid = $event->getUuid();
        $this->configCollection = $event->getConfigCollection();
    }
}
