<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\AccountCollection\Factory;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionFactory.
 */
class AccountCollectionFactory
{
    /**
     * @param UuidInterface $uuid
     * @param mixed[]       $configCollection
     *
     * @return AccountCollectionAggregate
     */
    public function makeInstance(UuidInterface $uuid, array $configCollection): AccountCollectionAggregate
    {
        return AccountCollectionAggregate::create($uuid, $configCollection);
    }
}
