<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\AccountCollection\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionWasStartedEvent.
 */
class AccountCollectionWasStartedEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var mixed[]
     */
    private $configCollection;

    /**
     * AccountCollectionWasStartedEvent constructor.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $configCollection
     */
    public function __construct(UuidInterface $uuid, array $configCollection)
    {
        $this->uuid = $uuid;
        $this->configCollection = $configCollection;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return AccountCollectionWasStartedEvent
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'data');

        return new static(
            Uuid::fromString($data['uuid']),
            $data['data']
        );
    }

    /**
     * Serialize event.
     *
     * @return mixed[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
            'data' => $this->configCollection,
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return mixed[]
     */
    public function getConfigCollection(): array
    {
        return $this->configCollection;
    }
}
