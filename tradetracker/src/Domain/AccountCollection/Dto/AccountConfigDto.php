<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\AccountCollection\Dto;

/**
 * Class AccountConfig.
 */
class AccountConfigDto
{
    /**
     * @var string
     */
    private $apiUser;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $siteId;

    /**
     * @var string
     */
    private $country;

    /**
     * AccountConfig constructor.
     *
     * @param string $apiUser
     * @param string $apiKey
     * @param string $siteId
     * @param string $country
     */
    public function __construct(string $apiUser, string $apiKey, string $siteId, string $country)
    {
        $this->apiUser = $apiUser;
        $this->siteId = $siteId;
        $this->apiKey = $apiKey;
        $this->country = $country;
    }

    /**
     * @param string[] $config
     *
     * @return AccountConfigDto
     */
    public static function fromArray(array $config): self
    {
        return new self(
            $config['apiUser'],
            $config['apiKey'],
            $config['siteId'],
            $config['country']
        );
    }

    /**
     * @return string
     */
    public function getApiUser(): string
    {
        return $this->apiUser;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getSiteId(): string
    {
        return $this->siteId;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
