<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\AccountCollection\Repository;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface AccountCollectionRepositoryInterface.
 */
interface AccountCollectionRepositoryInterface
{
    /**
     * @param UuidInterface $uuid
     *
     * @return AccountCollectionAggregate
     */
    public function get(UuidInterface $uuid): AccountCollectionAggregate;

    /**
     * @param AccountCollectionAggregate $program
     */
    public function store(AccountCollectionAggregate $program): void;
}
