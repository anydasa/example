<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionWasAggregatedEvent.
 */
final class ProgramCollectionWasAggregatedEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var mixed[]
     */
    private $requestedData;

    /**
     * @var string
     */
    private $country;

    /**
     * ProgramCollectionWasAggregated constructor.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $requestedData
     * @param string        $country
     */
    public function __construct(UuidInterface $uuid, array $requestedData, string $country)
    {
        $this->requestedData = $requestedData;
        $this->uuid = $uuid;
        $this->country = $country;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramCollectionWasAggregatedEvent
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'requestedData');
        Assertion::keyExists($data, 'country');

        return new static(
            Uuid::fromString($data['uuid']),
            $data['requestedData'],
            $data['country']
        );
    }

    /**
     * Serialize Event.
     *
     * @return mixed[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
            'requestedData' => $this->requestedData,
            'country' => $this->country,
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return mixed[]
     */
    public function getRequestedData(): array
    {
        return $this->requestedData;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
