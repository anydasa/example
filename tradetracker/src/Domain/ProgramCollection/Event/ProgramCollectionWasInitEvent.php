<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ProgramCollectionWasInitEvent.
 */
class ProgramCollectionWasInitEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var AccountConfigDto
     */
    private $accountConfig;

    /**
     * ProgramCollectionWasInitEvent constructor.
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     */
    public function __construct(UuidInterface $uuid, AccountConfigDto $accountConfig)
    {
        $this->uuid = $uuid;
        $this->accountConfig = $accountConfig;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramCollectionWasInitEvent
     *
     * @throws ExceptionInterface
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'accountConfig');

        $uuid = Uuid::fromString($data['uuid']);
        /** @var AccountConfigDto $accountConfigDto */
        $accountConfigDto = self::getSerializer()->denormalize($data['accountConfig'], AccountConfigDto::class);

        return new static($uuid, $accountConfigDto);
    }

    /**
     * @return Serializer
     */
    private static function getSerializer(): Serializer
    {
        return new Serializer([new ObjectNormalizer()]);
    }

    /**
     * @return AccountConfigDto
     */
    public function getAccountConfig(): AccountConfigDto
    {
        return $this->accountConfig;
    }

    /**
     * Serialize Event.
     *
     * @return mixed[]
     *
     * @throws ExceptionInterface
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
            'accountConfig' => self::getSerializer()->normalize($this->accountConfig),
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}
