<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Event;

use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionDataWasRequestedEvent.
 */
class ProgramCollectionDataWasRequestedEvent implements Serializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var mixed[]
     */
    private $data;

    /**
     * ProgramCollectionDataWasRequestedEvent constructor.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $data
     */
    public function __construct(UuidInterface $uuid, array $data)
    {
        $this->uuid = $uuid;
        $this->data = $data;
    }

    /**
     * Deserialize Event.
     *
     * @param mixed[] $data
     *
     * @return ProgramCollectionDataWasRequestedEvent
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'data');

        return new static(
            Uuid::fromString($data['uuid']),
            $data['data']
        );
    }

    /**
     * Serialize Event.
     *
     * @return mixed[]
     */
    public function serialize(): array
    {
        return [
            'uuid' => $this->getUuid()->toString(),
            'data' => $this->data,
        ];
    }

    /**
     * Get Uuid.
     *
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * Get raw program collection data.
     *
     * @return mixed[]
     */
    public function getData(): array
    {
        return $this->data;
    }
}
