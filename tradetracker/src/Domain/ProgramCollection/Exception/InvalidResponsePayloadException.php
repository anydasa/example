<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Exception;

use AdgoalCommon\Base\Domain\Exception\CriticalException;

/**
 * Class InvalidResponsePayloadException.
 */
class InvalidResponsePayloadException extends CriticalException
{
}
