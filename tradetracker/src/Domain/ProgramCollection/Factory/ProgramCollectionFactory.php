<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Factory;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionFactory.
 */
class ProgramCollectionFactory
{
    /**
     * Make ProgramCollection.
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     *
     * @return ProgramCollectionAggregate
     */
    public function makeInstance(UuidInterface $uuid, AccountConfigDto $accountConfig): ProgramCollectionAggregate
    {
        return ProgramCollectionAggregate::create($uuid, $accountConfig);
    }
}
