<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Repository;

use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface ProgramCollectionRepositoryInterface.
 */
interface ProgramCollectionRepositoryInterface
{
    /**
     * Get ProgramCollection with actual state.
     *
     * @param UuidInterface $uuid
     *
     * @return ProgramCollectionAggregate
     */
    public function get(UuidInterface $uuid): ProgramCollectionAggregate;

    /**
     * Store Events for ProgramCollectionAggregate.
     *
     * @param ProgramCollectionAggregate $program
     */
    public function store(ProgramCollectionAggregate $program): void;
}
