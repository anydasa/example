<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity;

use Broadway\EventSourcing\EventSourcedAggregateRoot;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Exception\InvalidResponsePayloadException;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionAggregate.
 */
class ProgramCollectionAggregate extends EventSourcedAggregateRoot
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @var AccountConfigDto
     */
    private $accountConfig;

    /**
     * @var mixed[]
     */
    private $requestedData;

    /**
     * Create ProgramCollectionAggregate.
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     *
     * @return ProgramCollectionAggregate
     */
    public static function create(UuidInterface $uuid, AccountConfigDto $accountConfig): self
    {
        $programCollection = new self();
        $programCollection->apply(new ProgramCollectionWasInitEvent($uuid, $accountConfig));

        return $programCollection;
    }

    /**
     * Set Requested raw data.
     *
     * @param mixed[] $requestedData
     *
     * @throws InvalidResponsePayloadException
     */
    public function setRequestedData(array $requestedData): void
    {
        if (0 === count($requestedData)) {
            throw new InvalidResponsePayloadException('Response have to contains some data');
        }

        $this->apply(new ProgramCollectionDataWasRequestedEvent($this->uuid, $requestedData));
    }

    /**
     * Validate set data.
     */
    public function aggregate(): void
    {
        $event = new ProgramCollectionWasAggregatedEvent(
            $this->uuid,
            $this->requestedData,
            $this->getAccountConfig()->getCountry()
        );

        $this->apply($event);
    }

    /**
     * Validate data.
     *
     * @throws InvalidResponsePayloadException
     */
    public function validate(): void
    {
        if (0 === count($this->requestedData)) { //TODO add a real validation
            throw new InvalidResponsePayloadException('Response have to contains some data');
        }

        $this->apply(new ProgramCollectionDataWasValidatedEvent($this->uuid));
    }

    /**
     * Get uuid.
     *
     * @return string
     */
    public function uuid(): string
    {
        return $this->uuid->toString();
    }

    /**
     * Get Aggregate Root Id.
     *
     * @return string
     */
    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    /**
     * Get Account Config.
     *
     * @return AccountConfigDto
     */
    public function getAccountConfig(): AccountConfigDto
    {
        return $this->accountConfig;
    }

    /**
     * Apply ProgramCollectionWasInitEvent.
     *
     * @param ProgramCollectionWasInitEvent $event
     */
    protected function applyProgramCollectionWasInitEvent(ProgramCollectionWasInitEvent $event): void
    {
        $this->uuid = $event->getUuid();
        $this->accountConfig = $event->getAccountConfig();
    }

    /**
     * Apply ProgramCollectionDataWasRequestedEvent.
     *
     * @param ProgramCollectionDataWasRequestedEvent $event
     */
    protected function applyProgramCollectionDataWasRequestedEvent(ProgramCollectionDataWasRequestedEvent $event): void
    {
        $this->requestedData = $event->getData();
    }
}
