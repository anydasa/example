<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Domain\ProgramCollection\Query;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;

/**
 * Interface ProgramCollectionQueryInterface.
 */
interface ProgramCollectionQueryInterface
{
    /**
     * Fetch Programs from affiliate network.
     *
     * @param AccountConfigDto $accountConfig
     *
     * @return object[]
     */
    public function fetchPrograms(AccountConfigDto $accountConfig): array;
}
