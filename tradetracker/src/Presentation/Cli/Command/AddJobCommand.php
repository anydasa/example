<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Presentation\Cli\Command;

use AdgoalCommon\Task\Infrastructure\Event\Consumer\JobProgramConsumer;
use DataGate\Program\Tradetracker\Application\Command\AccountCollection\CommandFactory;
use Enqueue\Client\ProducerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 * Class AddJobCommand.
 */
class AddJobCommand extends Command
{
    /**
     * the name of the command (the part after "bin/console").
     *
     * @var string
     */
    protected static $defaultName = 'add:job';

    /**
     * Queue task producer.
     *
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddJobCommand constructor.
     *
     * @param ProducerInterface $producer
     * @param LoggerInterface   $logger
     */
    public function __construct(ProducerInterface $producer, LoggerInterface $logger)
    {
        parent::__construct();
        $this->producer = $producer;
        $this->logger = $logger;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setName('add:job')
            ->setDescription('Add new job task to queue')
            ->setHelp('This command allows to add new task for execute some job throw queue producer.');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->logger->info('<info>Affiliate network update started: </info>');

        try {
            $message = [
                'type' => CommandFactory::ACCOUNT_COLLECTION_INIT_COMMAND,
                'args' => Uuid::uuid4()->toString(),
            ];

            $output->writeln(sprintf('Add command `%s` to job queue', $message['type']));

            $this->producer->sendCommand(JobProgramConsumer::SUBSCRIBED_JOB_COMMAND, $message);
        } catch (Throwable $e) {
            $this->logger->error('Process stopped unsuccessfully');
            $this->logger->error($e->getMessage());

            return 1;
        }

        $this->logger->info('Process finish successfully');

        return 0;
    }
}
