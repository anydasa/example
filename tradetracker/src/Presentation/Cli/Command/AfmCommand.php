<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Presentation\Cli\Command;

use DataGate\Program\Tradetracker\Application\Command\AccountCollection\CommandFactory;
use Exception;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AfmCommand.
 */
class AfmCommand extends Command
{
    /**
     * @var string|null The default command name
     */
    protected static $defaultName = 'program:run';

    /**
     * @var CommandBus
     */
    protected $commandBus;

    /**
     * @var CommandFactory
     */
    private $commandFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AfmCommand constructor.
     *
     * @param CommandBus      $commandBus
     * @param CommandFactory  $commandFactory
     * @param LoggerInterface $logger
     */
    public function __construct(CommandBus $commandBus, CommandFactory $commandFactory, LoggerInterface $logger)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
        $this->commandFactory = $commandFactory;
        $this->logger = $logger;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setName('program:run')
            ->setDescription('Run AFM saga process to update affiliate programs')
            ->setHelp(
                'This command allows to run process for download, validate, map affiliate network programs abd send their to DataMaster service.'
            );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null|int
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $uuid = Uuid::uuid4();

        $output->writeln('<info>Program started: </info>');
        $output->writeln('');
        $output->writeln("Uuid: {$uuid->toString()}");
        $output->writeln('');

        $command = $this->commandFactory->makeInitAccountCollectionCommand(
            $uuid
        );
        $this->commandBus->handle($command);

        $this->logger->info('Process finish successfully');

        return null;
    }
}
