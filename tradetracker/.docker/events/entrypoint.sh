#!/usr/bin/env bash

set -e

/app/bin/console d:m:m -n -vvv
/app/bin/console enqueue:setup-broker -vvv

echo
echo 'events init process done. Ready for start up.'
echo

exec "$@"
