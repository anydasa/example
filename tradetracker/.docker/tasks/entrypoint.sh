#!/usr/bin/env bash

set -e

/app/bin/console -vvv d:m:m -n
/app/bin/console -vvv enqueue:setup-broker -c masterdata
/app/bin/console -vvv enqueue:setup-broker -c task
/app/bin/console -vvv enqueue:setup-broker -c taskevent


echo
echo 'tasks init process done. Ready for start up.'
echo

exec "$@"
