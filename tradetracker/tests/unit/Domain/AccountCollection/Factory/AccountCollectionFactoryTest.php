<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\AccountCollection\Factory;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Factory\AccountCollectionFactory;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionFactoryTest.
 */
class AccountCollectionFactoryTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\AccountCollection\Factory\AccountCollectionFactory
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfigCollection
     */
    public function AccountCollectionShouldCreateAccountCollectionAggregate(UuidInterface $uuid, array $accountConfigCollection): void
    {
        $factory = new AccountCollectionFactory();
        $aggregate = $factory->makeInstance($uuid, $accountConfigCollection);

        self::assertInstanceOf(AccountCollectionAggregate::class, $aggregate);
    }
}
