<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\AccountCollection\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionWasInitEventTest.
 */
final class AccountCollectionWasInitEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfigCollection
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid, array $accountConfigCollection): void
    {
        $event = AccountCollectionWasStartedEvent::deserialize(
            [
                'uuid' => $uuid->toString(),
                'data' => $accountConfigCollection,
            ]
        );

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
        self::assertSame($accountConfigCollection, $event->getConfigCollection());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(): void
    {
        $this->expectException(InvalidArgumentException::class);

        AccountCollectionWasStartedEvent::deserialize(
            [
                'uuid' => 'Some string',
                'data' => 'Some string',
            ]
        );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfigCollection
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid, array $accountConfigCollection): void
    {
        $event = AccountCollectionWasStartedEvent::deserialize(
            [
                'uuid' => $uuid,
                'data' => $accountConfigCollection,
            ]
        );

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
        self::assertArrayHasKey('data', $serialized);
    }
}
