<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\AccountCollection\Entity;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionAggregateTest.
 */
class AccountCollectionAggregateTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfigList
     */
    public function createTest(UuidInterface $uuid, array $accountConfigList): void
    {
        $AccountCollectionAggregate = AccountCollectionAggregate::create($uuid, $accountConfigList);

        self::assertSame($uuid->toString(), $AccountCollectionAggregate->getAggregateRootId());
        self::assertSame($uuid->toString(), $AccountCollectionAggregate->uuid());
        self::assertSame($accountConfigList, $AccountCollectionAggregate->getConfigCollection());

        $events = $AccountCollectionAggregate->getUncommittedEvents();
        self::assertCount(1, $events->getIterator(), 'Only one event should be in the buffer');

        /** @var AccountCollectionWasStartedEvent $eventPayload */
        $eventPayload = $events->getIterator()->current()->getPayload();
        self::assertInstanceOf(AccountCollectionWasStartedEvent::class, $eventPayload);
        self::assertEquals($uuid, $eventPayload->getUuid());
        self::assertEquals($accountConfigList, $eventPayload->getConfigCollection());
    }
}
