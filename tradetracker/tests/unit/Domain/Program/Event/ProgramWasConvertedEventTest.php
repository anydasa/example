<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\Program\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramWasMappedEventTest.
 */
final class ProgramWasConvertedEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList()
     *
     * @param UuidInterface $uuid
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid): void
    {
        $event = ProgramWasConvertedEvent::deserialize([
            'uuid' => $uuid->toString(),
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent::deserialize
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        ProgramWasConvertedEvent::deserialize([
            'uuid' => 'Wrong uuid',
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid): void
    {
        $event = ProgramWasConvertedEvent::deserialize([
            'uuid' => $uuid->toString(),
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
    }
}
