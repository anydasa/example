<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\Program\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramWasInitEventTest.
 */
final class ProgramWasInitEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     *
     * @throws InvalidRawProgramDataException
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $event = ProgramWasInitEvent::deserialize([
            'uuid' => $uuid,
            'rawProgramData' => $rawProgram,
            'country' => $country,
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertEquals($uuid, $event->getUuid());
        self::assertEquals($rawProgramData, $event->getRawProgramData());
        self::assertEquals($country, $event->getCountry());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent::deserialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     *
     * @throws InvalidRawProgramDataException
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $this->expectException(InvalidArgumentException::class);

        ProgramWasInitEvent::deserialize([
            'uuid' => $country,
            'rawProgramData' => $rawProgram,
            'country' => $uuid,
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     *
     * @throws InvalidRawProgramDataException
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $event = ProgramWasInitEvent::deserialize([
            'uuid' => $uuid,
            'rawProgramData' => $rawProgram,
            'country' => $country,
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
        self::assertArrayHasKey('rawProgramData', $serialized);
        self::assertArrayHasKey('country', $serialized);
    }
}
