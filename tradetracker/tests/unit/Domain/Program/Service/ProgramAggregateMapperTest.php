<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\Program\Service;

use AdgoalCommon\Datagate\Domain\Dto\ProgramResultDto;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Exception\ProgramAggregateMapException;
use DataGate\Program\Tradetracker\Domain\Program\Service\ProgramAggregateMapper;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class ProgramAggregateMapperTest extends TestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Service\ProgramAggregateMapper
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList()
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     *
     * @throws ProgramAggregateMapException
     */
    public function mapToProgramResultDtoTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $programAggregate = ProgramAggregate::create($uuid, $rawProgramData, $country);
        $programAggregate->convert();
        $mapper = new ProgramAggregateMapper();
        $programResultDto = $mapper->mapToProgramResultDto($programAggregate);

        self::assertInstanceOf(ProgramResultDto::class, $programResultDto);
    }
}
