<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\Program\Factory;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Factory\ProgramFactory;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramFactoryTest.
 *
 * @category Tests\Unit\Domain\Program\Factory
 */
class ProgramFactoryTest extends TestCase
{
    use ValueObjectMockTrait;

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Factory\ProgramFactory::makeInstance
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     */
    public function programFactoryShouldCreateProgramEntity(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $factory = new ProgramFactory();
        $aggregate = $factory->makeInstance($uuid, $rawProgramData, $country);

        self::assertInstanceOf(ProgramAggregate::class, $aggregate);
    }
}
