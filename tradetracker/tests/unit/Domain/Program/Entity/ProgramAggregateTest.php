<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\Program\Entity;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use Broadway\Domain\DomainMessage;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasSentEvent;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\Program;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionAggregateTest.
 */
class ProgramAggregateTest extends TestCase
{
    use ValueObjectMockTrait;

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     */
    public function createTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $programAggregate = ProgramAggregate::create($uuid, $rawProgramData, $country);

        self::assertSame($uuid->toString(), $programAggregate->getAggregateRootId());
        self::assertSame($uuid->toString(), $programAggregate->uuid());

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(1, $events->getIterator(), 'Only one event should be in the buffer');

        /** @var DomainMessage $event */
        $event = $events->getIterator()->current();
        self::assertInstanceOf(
            ProgramWasInitEvent::class,
            $event->getPayload()
        );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     */
    public function convertProgramTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $programAggregate = ProgramAggregate::create($uuid, $rawProgramData, $country);
        $programAggregate->convert();

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(2, $events->getIterator());

        /** @var DomainMessage $event */
        $event = $events->getIterator()->offsetGet('1');
        self::assertInstanceOf(ProgramWasConvertedEvent::class, $event->getPayload());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     */
    public function sendResultTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $programAggregate = ProgramAggregate::create($uuid, $rawProgramData, $country);
        $programAggregate->convert();
        $programAggregate->sendResult();

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(3, $events->getIterator());

        /** @var DomainMessage $event */
        $event = $events->getIterator()->offsetGet('2');
        self::assertInstanceOf(ProgramWasSentEvent::class, $event->getPayload());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate::getProgram
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawProgram
     * @param string         $country
     * @param RawProgramData $rawProgramData
     */
    public function getProgramTest(UuidInterface $uuid, array $rawProgram, string $country, RawProgramData $rawProgramData): void
    {
        $programAggregate = ProgramAggregate::create($uuid, $rawProgramData, $country);
        $programAggregate->convert();

        self::assertInstanceOf(Program::class, $programAggregate->getProgram());
    }
}
