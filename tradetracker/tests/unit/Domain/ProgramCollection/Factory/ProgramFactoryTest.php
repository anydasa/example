<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Factory;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Factory\ProgramCollectionFactory;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramFactoryTest.
 */
class ProgramFactoryTest extends TestCase
{
    use ValueObjectMockTrait;

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Factory\ProgramCollectionFactory::makeInstance
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid             Afm process unique id
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     */
    public function programFactoryShouldCreateProgramEntity(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $factory = new ProgramCollectionFactory();
        $aggregate = $factory->makeInstance($uuid, $accountConfigDto);

        self::assertInstanceOf(ProgramCollectionAggregate::class, $aggregate);
    }
}
