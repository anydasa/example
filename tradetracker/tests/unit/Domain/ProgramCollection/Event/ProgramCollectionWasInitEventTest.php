<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class ProgramCollectionWasInitEventTest.
 */
final class ProgramCollectionWasInitEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid             Afm process unique id
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     *
     * @throws ExceptionInterface
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $event = ProgramCollectionWasInitEvent::deserialize([
            'uuid' => $uuid,
            'accountConfig' => $accountConfig,
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
        self::assertEquals($accountConfigDto, $event->getAccountConfig());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent::deserialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     *
     * @throws ExceptionInterface
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $this->expectException(InvalidArgumentException::class);

        ProgramCollectionWasInitEvent::deserialize([
            'uuid' => 'string',
            'accountConfig' => $accountConfig,
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     *
     * @throws ExceptionInterface
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid, array $accountConfig): void
    {
        $event = ProgramCollectionWasInitEvent::deserialize([
            'uuid' => $uuid,
            'accountConfig' => $accountConfig,
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
        self::assertArrayHasKey('accountConfig', $serialized);
    }
}
