<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionDataWasValidatedTest.
 */
final class ProgramCollectionDataWasValidatedTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid): void
    {
        $event = ProgramCollectionDataWasValidatedEvent::deserialize([
            'uuid' => $uuid,
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent::deserialize
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        ProgramCollectionDataWasValidatedEvent::deserialize([
            'uuid' => 'string',
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid Afm process unique id
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid): void
    {
        $event = ProgramCollectionDataWasValidatedEvent::deserialize([
            'uuid' => $uuid,
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
    }
}
