<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionWasAggregatedEventTest.
 */
final class ProgramCollectionWasAggregatedEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     * @param string           $country
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response, string $country): void
    {
        $event = ProgramCollectionWasAggregatedEvent::deserialize([
            'uuid' => $uuid,
            'requestedData' => $response,
            'country' => $country,
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
        self::assertSame($response, $event->getRequestedData());
        self::assertSame($country, $event->getCountry());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent::deserialize
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        ProgramCollectionWasAggregatedEvent::deserialize([
            'uuid' => 'string',
            'requestedData' => [],
            'country' => null,
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     * @param string           $country
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response, string $country): void
    {
        $event = ProgramCollectionWasAggregatedEvent::deserialize([
            'uuid' => $uuid,
            'requestedData' => $response,
            'country' => $country,
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
        self::assertArrayHasKey('requestedData', $serialized);
        self::assertArrayHasKey('country', $serialized);
    }
}
