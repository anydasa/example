<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Event;

use Broadway\Serializer\Serializable;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionDataWasRequestedEventTest.
 */
final class ProgramCollectionDataWasRequestedEventTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function eventShouldBeDeserializableTest(UuidInterface $uuid, array $accountConfig): void
    {
        $event = ProgramCollectionDataWasRequestedEvent::deserialize([
            'uuid' => $uuid,
            'data' => $accountConfig,
        ]);

        self::assertInstanceOf(Serializable::class, $event);
        self::assertSame($uuid->toString(), $event->getUuid()->toString());
        self::assertSame($accountConfig, $event->getData());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent::deserialize
     */
    public function eventShouldFailWhenDeserializeWithWrongDataTest(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        ProgramCollectionDataWasRequestedEvent::deserialize([
            'uuid' => 'string',
            'data' => [],
        ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent::serialize
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid          Afm process unique id
     * @param mixed[]       $accountConfig
     */
    public function eventShouldBeSerializableTest(UuidInterface $uuid, array $accountConfig): void
    {
        $event = ProgramCollectionDataWasRequestedEvent::deserialize([
            'uuid' => $uuid,
            'data' => $accountConfig,
        ]);

        $serialized = $event->serialize();

        self::assertArrayHasKey('uuid', $serialized);
        self::assertArrayHasKey('data', $serialized);
    }
}
