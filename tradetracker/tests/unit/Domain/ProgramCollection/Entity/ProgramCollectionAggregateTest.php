<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Domain\ProgramCollection\Entity;

use Broadway\Domain\DomainMessage;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Exception\InvalidResponsePayloadException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionAggregateTest.
 */
class ProgramCollectionAggregateTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     */
    public function createTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $programAggregate = ProgramCollectionAggregate::create($uuid, $accountConfigDto);

        self::assertSame($uuid->toString(), $programAggregate->getAggregateRootId());
        self::assertSame($uuid->toString(), $programAggregate->uuid());
        self::assertEquals($accountConfigDto, $programAggregate->getAccountConfig());

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(1, $events->getIterator(), 'Only one event should be in the buffer');

        /** @var DomainMessage $event */
        $event = $events->getIterator()->current();
        self::assertInstanceOf(
            ProgramCollectionWasInitEvent::class,
            $event->getPayload()
        );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     *
     * @throws InvalidResponsePayloadException
     */
    public function setResponseContentTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response): void
    {
        $programAggregate = ProgramCollectionAggregate::create($uuid, $accountConfigDto);
        $programAggregate->setRequestedData($response);

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(2, $events->getIterator());

        /** @var DomainMessage $event */
        $event = $events->getIterator()->offsetGet('1');
        self::assertInstanceOf(ProgramCollectionDataWasRequestedEvent::class, $event->getPayload());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     *
     * @throws InvalidResponsePayloadException
     */
    public function validateTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response): void
    {
        $programAggregate = ProgramCollectionAggregate::create($uuid, $accountConfigDto);
        $programAggregate->setRequestedData($response);
        $programAggregate->validate();

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(3, $events->getIterator());

        /** @var DomainMessage $event */
        $event = $events->getIterator()->offsetGet('2');
        self::assertInstanceOf(ProgramCollectionDataWasValidatedEvent::class, $event->getPayload());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     *
     * @throws InvalidResponsePayloadException
     */
    public function validateInvalidTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response): void
    {
        $this->expectException(InvalidResponsePayloadException::class);

        $programAggregate = ProgramCollectionAggregate::create($uuid, $accountConfigDto);
        $programAggregate->setRequestedData([]);
        $programAggregate->validate();
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     *
     * @throws InvalidResponsePayloadException
     */
    public function aggregateTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto, array $response): void
    {
        $programAggregate = ProgramCollectionAggregate::create($uuid, $accountConfigDto);
        $programAggregate->setRequestedData($response);
        $programAggregate->validate();
        $programAggregate->aggregate();

        $events = $programAggregate->getUncommittedEvents();
        self::assertCount(4, $events->getIterator());

        /** @var DomainMessage $event */
        $event = $events->getIterator()->offsetGet('3');
        self::assertInstanceOf(ProgramCollectionWasAggregatedEvent::class, $event->getPayload());
    }
}
