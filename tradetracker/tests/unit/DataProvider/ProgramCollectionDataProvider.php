<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use Exception;
use Ramsey\Uuid\Uuid;
use TypeError;

/**
 * Class ProgramCollectionDataProvider.
 */
class ProgramCollectionDataProvider
{
    private const ACCOUNT_CONFIG = [
        'apiUser' => '11111',
        'apiKey' => '1111111111111111111111111111111111111111',
        'siteId' => '111111',
        'country' => 'DE',
    ];

    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getAccountConfig(): array
    {
        return [
            [
                Uuid::uuid4(),
                self::ACCOUNT_CONFIG,
                AccountConfigDto::fromArray(self::ACCOUNT_CONFIG),
                json_decode(file_get_contents(__DIR__ . '/resources/programsList.json'), true),
                'AT',
            ],
        ];
    }

    /**
     * $return = [
     *     0 => (mixed) wrong Uuid,
     *     1 => (mixed) wrong config,
     *     2 => (string) Exception class,
     * ].
     *
     * @return mixed[] $return
     *
     * @throws Exception
     */
    public function getAccountConfigException(): array
    {
        return [
            [Uuid::uuid4()->toString(), self::ACCOUNT_CONFIG, TypeError::class],
            [null, self::ACCOUNT_CONFIG, TypeError::class],
            [1, self::ACCOUNT_CONFIG, TypeError::class],
            [Uuid::uuid4(), 'string', TypeError::class],
            [Uuid::uuid4(), null, TypeError::class],
            [Uuid::uuid4(), 1, TypeError::class],
        ];
    }
}
