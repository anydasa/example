<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * Class ProgramDataProvider.
 */
class ProgramDataProvider
{
    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getProgramList(): array
    {
        return [
            [
                Uuid::uuid4(),
                json_decode(file_get_contents(__DIR__ . '/resources/program.json'), true),
                'AT',
                RawProgramData::fromArray(json_decode(file_get_contents(__DIR__ . '/resources/program.json'), true)),
            ],
        ];
    }
}
