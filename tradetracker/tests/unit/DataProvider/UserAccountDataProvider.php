<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use Exception;
use Ramsey\Uuid\Uuid;

/**
 * Class UserAccountDataProvider.
 */
class UserAccountDataProvider
{
    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getList(): array
    {
        return [
            [
                Uuid::uuid4(),
                [
                    [
                        'apiUser' => '11111',
                        'apiKey' => '1111111111111111111111111111111111111111',
                        'siteId' => '111111',
                        'country' => 'DE',
                    ],
                ],
            ],
        ];
    }
}
