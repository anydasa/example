<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use Exception;
use Ramsey\Uuid\Uuid;
use TypeError;

class UuidProvider
{
    /**
     * Return data with uuid fixture.
     *
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getUuidList(): array
    {
        return [
            [Uuid::uuid4()],
            [Uuid::uuid4()],
            [Uuid::uuid4()],
            [Uuid::uuid4()],
        ];
    }

    /**
     * Return data with invalid uuid fixture.
     *
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getInvalidUuidExceptionList(): array
    {
        return [
            [Uuid::uuid4()->toString(), TypeError::class],
            [2134213, TypeError::class],
            ['test', TypeError::class],
            [null, TypeError::class],
            ['', TypeError::class],
        ];
    }
}
