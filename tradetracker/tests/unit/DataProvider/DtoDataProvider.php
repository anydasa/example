<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use Exception;

/**
 * Class DtoDataProvider.
 */
class DtoDataProvider
{
    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getList(): array
    {
        return [
            [
                [
                    'networkCode' => 'TRADETRACKER',
                    'programId' => '22331',
                    'programName' => '24Option.com - AT',
                    'programStatus' => 'active',
                    'relationshipStatus' => 'joined',
                    'programUrl' => 'https://www.24option.com/de',
                    'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/22331.png',
                    'shopDeepLink' => 'https://tc.tradetracker.net/?c=22331&m=0&a=137182&r=&u=',
                ],
            ],
        ];
    }
}
