<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\DataProvider;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * Class ProgramCollectionSagaDataProvider.
 */
class ProgramCollectionSagaDataProvider
{
    private const ACCOUNT_CONFIG = [
        'apiUser' => '11111',
        'apiKey' => '1111111111111111111111111111111111111111',
        'siteId' => '111111',
        'country' => 'DE',
    ];

    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function eventProgramCollectionWasInitEventHandleRequestProgramCollectionCommandTest(): array
    {
        return [
            [Uuid::uuid4(), AccountConfigDto::fromArray(self::ACCOUNT_CONFIG)],
        ];
    }

    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function eventProgramCollectionDataWasRequestedEventHandleValidateProgramCollectionCommandTest(): array
    {
        return [
            [
                Uuid::uuid4(),
                AccountConfigDto::fromArray(self::ACCOUNT_CONFIG),
                json_decode(file_get_contents(__DIR__ . '/resources/programsList.json'), true),
            ],
        ];
    }

    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function eventProgramCollectionDataWasValidatedEventHandleAggregateProgramCollectionCommandTest(): array
    {
        return [
            [
                Uuid::uuid4(),
                AccountConfigDto::fromArray(self::ACCOUNT_CONFIG),
                json_decode(file_get_contents(__DIR__ . '/resources/programsList.json'), true),
            ],
        ];
    }

    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function eventProgramCollectionWasAggregatedEventHandleLastStepTest(): array
    {
        return [
            [
                Uuid::uuid4(),
                AccountConfigDto::fromArray(self::ACCOUNT_CONFIG),
                json_decode(file_get_contents(__DIR__ . '/resources/programsList.json'), true),
                'AT',
            ],
        ];
    }
}
