<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Infrastructure\Query;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Infrastructure\Query\Soap\ClientFactory;
use DataGate\Program\Tradetracker\Infrastructure\Query\Soap\ProgramCollectionQuery;
use Mockery;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class ProgramCollectionQueryTest extends TestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Query\Soap\ProgramCollectionQuery
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $configAccount
     * @param AccountConfigDto $accountConfigDto
     * @param mixed[]          $response
     *
     * @throws \SoapFault
     */
    public function fetchProgramsTest(UuidInterface $uuid, array $configAccount, AccountConfigDto $accountConfigDto, array $response): void
    {
        $programQuery = $this->makeProgramCollectionQuery($response);

        $programQueryResult = $programQuery->fetchPrograms($accountConfigDto);

        self::assertSame($programQueryResult, $response);
    }

    /**
     * @param mixed[] $response
     *
     * @return ProgramCollectionQuery
     */
    private function makeProgramCollectionQuery(array $response): ProgramCollectionQuery
    {
        $mockClient = Mockery::mock(\SoapClient::class, [
            'authenticate' => '',
            'getCampaigns' => $response,
        ]);

        $mockClientFactory = Mockery::mock(ClientFactory::class, ['makeInstance' => $mockClient]);

        return new ProgramCollectionQuery('', $mockClientFactory);
    }
}
