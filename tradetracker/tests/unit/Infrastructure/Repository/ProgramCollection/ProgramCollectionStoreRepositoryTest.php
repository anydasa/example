<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Infrastructure\Repository\ProgramCollection;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use AdgoalCommon\Datagate\Testing\ApplicationTestCase;
use Broadway\Domain\DateTime;
use Broadway\Domain\DomainEventStream;
use Broadway\Domain\DomainMessage;
use Broadway\Domain\Metadata;
use Broadway\EventHandling\SimpleEventBus;
use Broadway\EventHandling\TraceableEventBus;
use Broadway\EventStore\InMemoryEventStore;
use Broadway\EventStore\TraceableEventStore;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Entity\ProgramCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use DataGate\Program\Tradetracker\Infrastructure\Repository\ProgramCollection\ProgramCollectionStore;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionStoreRepositoryTest.
 */
class ProgramCollectionStoreRepositoryTest extends ApplicationTestCase
{
    use ValueObjectMockTrait;

    /**
     * @var TraceableEventBus
     */
    protected $eventBus;

    /**
     * @var TraceableEventStore
     */
    protected $eventStore;

    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->eventBus = new TraceableEventBus(new SimpleEventBus());
        $this->eventBus->trace();

        $this->eventStore = new TraceableEventStore(new InMemoryEventStore());
        $this->eventStore->trace();
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\ProgramCollection\ProgramCollectionStore::store
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     */
    public function storeProgramCollectionAggregateTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $programStoreRepository = new ProgramCollectionStore($this->eventStore, $this->eventBus);
        $programStoreRepository->store(ProgramCollectionAggregate::create($uuid, $accountConfigDto));

        self::assertCount(1, $this->eventStore->getEvents());
        self::assertInstanceOf(ProgramCollectionWasInitEvent::class, $this->eventStore->getEvents()[0]);

        self::assertCount(1, $this->eventBus->getEvents());
        self::assertInstanceOf(ProgramCollectionWasInitEvent::class, $this->eventBus->getEvents()[0]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\ProgramCollection\ProgramCollectionStore::get
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface    $uuid
     * @param mixed[]          $accountConfig
     * @param AccountConfigDto $accountConfigDto
     */
    public function getProgramCollectionAggregateFromStoreTest(UuidInterface $uuid, array $accountConfig, AccountConfigDto $accountConfigDto): void
    {
        $domain = new DomainMessage(
            $uuid,
            100,
            new Metadata([]),
            new ProgramCollectionWasInitEvent($uuid, $accountConfigDto),
            DateTime::now()
        );
        $this->eventStore->append($uuid, new DomainEventStream([$domain]));

        $programCollectionStore = new ProgramCollectionStore($this->eventStore, $this->eventBus);
        $programCollectionAggregate = $programCollectionStore->get($uuid);

        self::assertInstanceOf(ProgramCollectionAggregate::class, $programCollectionAggregate);
    }
}
