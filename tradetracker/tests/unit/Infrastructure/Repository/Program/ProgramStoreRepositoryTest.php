<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Infrastructure\Repository\Program;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use AdgoalCommon\Datagate\Testing\ApplicationTestCase;
use Broadway\Domain\DateTime;
use Broadway\Domain\DomainEventStream;
use Broadway\Domain\DomainMessage;
use Broadway\Domain\Metadata;
use Broadway\EventHandling\SimpleEventBus;
use Broadway\EventHandling\TraceableEventBus;
use Broadway\EventStore\InMemoryEventStore;
use Broadway\EventStore\TraceableEventStore;
use DataGate\Program\Tradetracker\Domain\Program\Entity\ProgramAggregate;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramStore;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramStoreRepositoryTest.
 */
class ProgramStoreRepositoryTest extends ApplicationTestCase
{
    use ValueObjectMockTrait;

    /** @var TraceableEventBus */
    protected $eventBus;

    /** @var TraceableEventStore */
    protected $eventStore;

    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->eventBus = new TraceableEventBus(new SimpleEventBus());
        $this->eventBus->trace();

        $this->eventStore = new TraceableEventStore(new InMemoryEventStore());
        $this->eventStore->trace();
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramStore::store
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList()
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawData
     * @param string         $country
     * @param RawProgramData $rawDataProgram
     */
    public function storeProgramAggregateTest(UuidInterface $uuid, array $rawData, string $country, RawProgramData $rawDataProgram): void
    {
        $programStoreRepository = new ProgramStore($this->eventStore, $this->eventBus);
        $programStoreRepository->store(ProgramAggregate::create($uuid, $rawDataProgram, $country));

        self::assertCount(1, $this->eventStore->getEvents());
        self::assertInstanceOf(ProgramWasInitEvent::class, $this->eventStore->getEvents()[0]);

        self::assertCount(1, $this->eventBus->getEvents());
        self::assertInstanceOf(ProgramWasInitEvent::class, $this->eventBus->getEvents()[0]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramStore::get
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList()
     *
     * @param UuidInterface  $uuid
     * @param mixed[]        $rawData
     * @param string         $country
     * @param RawProgramData $rawDataProgram
     */
    public function getProgramAggregateFromStoreTest(UuidInterface $uuid, array $rawData, string $country, RawProgramData $rawDataProgram): void
    {
        $domain = new DomainMessage($uuid, 100, new Metadata([]), new ProgramWasInitEvent($uuid, $rawDataProgram, $country), DateTime::now());
        $this->eventStore->append($uuid->toString(), new DomainEventStream([$domain]));

        $programStoreRepository = new ProgramStore($this->eventStore, $this->eventBus);
        $programAggregate = $programStoreRepository->get($uuid);

        self::assertInstanceOf(ProgramAggregate::class, $programAggregate);
        self::assertSame($rawDataProgram, $programAggregate->getRawData());
        self::assertSame($country, $programAggregate->getCountry());
    }
}
