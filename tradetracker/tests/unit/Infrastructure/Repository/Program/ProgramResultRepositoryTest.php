<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Infrastructure\Repository\Program;

use AdgoalCommon\Datagate\Domain\Dto\ProgramResultDto;
use AdgoalCommon\Datagate\Testing\ApplicationTestCase;
use DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramResultRepository;
use Enqueue\Client\ProducerInterface;
use Enqueue\Client\TraceableProducer;
use Mockery;
use Mockery\MockInterface;

/**
 * Class ProgramResultRepositoryTest.
 *
 * @category Tests\Unit\Infrastructure\Repository\Program
 */
class ProgramResultRepositoryTest extends ApplicationTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramResultRepository::add
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\DtoDataProvider::getList()
     *
     * @param mixed[] $resultDtoArray
     */
    public function addProgramResultDtoToMasterDataQueueTest(array $resultDtoArray): void
    {
        /** @var TraceableProducer $producer */
        $producer = $this->service(ProducerInterface::class);

        $programResult = new ProgramResultRepository($producer);
        $programResultDto = $this->makeProgramResultDtoMock($resultDtoArray);
        $programResult->add($programResultDto);
        $traces = $producer->getTopicTraces('master.data');

        self::assertCount(1, $traces);
        self::assertEquals('master.data', $traces[0]['topic']);
        self::assertEquals($resultDtoArray, $traces[0]['body']);
    }

    /**
     * Return ProgramResultDto mock object.
     *
     * @param mixed[] $dtoResult
     * @param int     $times
     *
     * @return MockInterface|ProgramResultDto
     */
    protected function makeProgramResultDtoMock(array $dtoResult, int $times = 0): MockInterface
    {
        $mock = Mockery::mock(ProgramResultDto::class);
        $mock
            ->shouldReceive('serialize')
            ->times($times)
            ->andReturn($dtoResult);

        return $mock;
    }
}
