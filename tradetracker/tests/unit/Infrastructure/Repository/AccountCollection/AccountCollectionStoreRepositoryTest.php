<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Infrastructure\Repository\AccountCollection;

use AdgoalCommon\Datagate\Testing\ApplicationTestCase;
use Broadway\Domain\DateTime;
use Broadway\Domain\DomainEventStream;
use Broadway\Domain\DomainMessage;
use Broadway\Domain\Metadata;
use Broadway\EventHandling\SimpleEventBus;
use Broadway\EventHandling\TraceableEventBus;
use Broadway\EventStore\InMemoryEventStore;
use Broadway\EventStore\TraceableEventStore;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Entity\AccountCollectionAggregate;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use DataGate\Program\Tradetracker\Infrastructure\Repository\AccountCollection\AccountCollectionStore;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionStoreRepositoryTest.
 */
class AccountCollectionStoreRepositoryTest extends ApplicationTestCase
{
    /**
     * @var TraceableEventBus
     */
    protected $eventBus;

    /**
     * @var TraceableEventStore
     */
    protected $eventStore;

    /**
     * This method is called before each test.
     */
    protected function setUp(): void
    {
        $this->eventBus = new TraceableEventBus(new SimpleEventBus());
        $this->eventBus->trace();

        $this->eventStore = new TraceableEventStore(new InMemoryEventStore());
        $this->eventStore->trace();
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\AccountCollection\AccountCollectionStore::store
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $configCollection
     */
    public function storeAccountCollectionAggregateTest(UuidInterface $uuid, array $configCollection): void
    {
        $programStoreRepository = new AccountCollectionStore($this->eventStore, $this->eventBus);
        $programStoreRepository->store(AccountCollectionAggregate::create($uuid, $configCollection));

        self::assertCount(1, $this->eventStore->getEvents());
        self::assertInstanceOf(AccountCollectionWasStartedEvent::class, $this->eventStore->getEvents()[0]);

        self::assertCount(1, $this->eventBus->getEvents());
        self::assertInstanceOf(AccountCollectionWasStartedEvent::class, $this->eventBus->getEvents()[0]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Infrastructure\Repository\AccountCollection\AccountCollectionStore::get
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $configCollection
     */
    public function getAccountCollectionAggregateFromStoreTest(UuidInterface $uuid, array $configCollection): void
    {
        $domain = new DomainMessage(
            $uuid,
            100,
            new Metadata([]),
            new AccountCollectionWasStartedEvent($uuid, $configCollection),
            DateTime::now()
        );
        $this->eventStore->append($uuid, new DomainEventStream([$domain]));

        $accountCollectionStore = new AccountCollectionStore($this->eventStore, $this->eventBus);
        $accountCollectionAggregate = $accountCollectionStore->get($uuid);

        self::assertInstanceOf(AccountCollectionAggregate::class, $accountCollectionAggregate);
    }
}
