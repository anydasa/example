<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\AccountCollection;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\AccountCollection\InitAccountCollectionCommand;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class StartPageCollectionCollectionHandlerTest.
 */
class InitAccountCollectionCollectionHandlerTest extends AbstractAccountCollectionTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\AccountCollection\InitAccountCollectionHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     */
    public function initPageCollectionCommandShouldFirePageCollectionWasStartedEventTest(UuidInterface $uuid): void
    {
        $command = new InitAccountCollectionCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(AccountCollectionWasStartedEvent::class, $events[0]->getPayload());
    }
}
