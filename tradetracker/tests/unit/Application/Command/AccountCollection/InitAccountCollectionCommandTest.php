<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\AccountCollection;

use DataGate\Program\Tradetracker\Application\Command\AccountCollection\InitAccountCollectionCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class StartPageCollectionCommandTest.
 */
class InitAccountCollectionCommandTest extends AbstractAccountCollectionTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\AccountCollection\InitAccountCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     */
    public function initProgramCollectionCommandTest(UuidInterface $uuid): void
    {
        $command = new InitAccountCollectionCommand($uuid);

        self::assertSame($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\AccountCollection\InitAccountCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed $uuid
     * @param mixed $exceptionClassName
     */
    public function initProgramCommandInvalidTypeTest($uuid, $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new InitAccountCollectionCommand($uuid);
    }
}
