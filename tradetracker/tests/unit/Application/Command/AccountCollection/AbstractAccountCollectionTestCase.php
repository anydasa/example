<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\AccountCollection;

use DataGate\Program\Tradetracker\Application\Command\AccountCollection\CommandFactory;
use DataGate\Program\Tradetracker\Tests\Unit\Application\ApplicationTestCase;

/**
 * Class AbstractPageCollectionTestCase.
 */
abstract class AbstractAccountCollectionTestCase extends ApplicationTestCase
{
    /**
     * Command factory service.
     *
     * @var string
     */
    protected $commandFactoryName = CommandFactory::class;

    /**
     * Name for testing command bus.
     *
     * @var string
     */
    protected $commandBusName = 'tactician.commandbus.command.account.collection';

    /**
     * handleStartPageCollectionCommand.
     *
     * @param string $uuid
     */
    protected function handleStartPageCollectionCommand(string $uuid): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::ACCOUNT_COLLECTION_INIT_COMMAND,
            $uuid
        );

        $this->handle($command);
    }
}
