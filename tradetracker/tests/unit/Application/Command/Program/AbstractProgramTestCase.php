<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use DataGate\Program\Tradetracker\Application\Command\Program\CommandFactory;
use DataGate\Program\Tradetracker\Tests\Unit\Application\ApplicationTestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AbstractProgramTestCase.
 */
abstract class AbstractProgramTestCase extends ApplicationTestCase
{
    /**
     * Command factory service.
     *
     * @var string
     */
    protected $commandFactoryName = CommandFactory::class;

    /**
     * Name for testing command bus.
     *
     * @var string
     */
    protected $commandBusName = 'tactician.commandbus.command.program';

    /**
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     */
    protected function handleInitProgramCommand(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::INIT_PROGRAM_COMMAND,
            $uuid,
            $rawProgramData,
            $countryCode
        );

        $this->handle($command);
    }

    /**
     * @param UuidInterface $uuid
     */
    protected function convertProgramCommand(UuidInterface $uuid): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::CONVERT_PROGRAM_COMMAND,
            $uuid
        );

        $this->handle($command);
    }
}
