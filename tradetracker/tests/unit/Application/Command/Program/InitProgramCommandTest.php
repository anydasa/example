<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use DataGate\Program\Tradetracker\Application\Command\Program\InitProgramCommand;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramCommandTest.
 */
class InitProgramCommandTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\InitProgramCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     *
     * @throws \DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException
     */
    public function initProgramCommandTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $command = new InitProgramCommand($uuid, $rawProgramData, $countryCode);
        $rawProgramDataValueObject = RawProgramData::fromArray($rawProgramData);

        self::assertEquals($uuid, $command->getUuid());
        self::assertEquals($rawProgramDataValueObject, $command->getRawProgramData());
        self::assertEquals($countryCode, $command->getCountry());
    }
}
