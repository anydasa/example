<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ConvertProgramHandlerTest.
 */
class ConvertProgramHandlerTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     */
    public function convertProgramCommandShouldFireProgramWasConvertedEventTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $this->handleInitProgramCommand($uuid, $rawProgramData, $countryCode);

        $command = new ConvertProgramCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramWasConvertedEvent::class, $events[1]->getPayload());
    }
}
