<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\Program\InitProgramCommand;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramHandlerTest.
 */
class InitProgramHandlerTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\InitProgramCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     *
     * @throws \DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException
     */
    public function initProgramCommandShouldFireProgramWasInitEventTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $command = new InitProgramCommand($uuid, $rawProgramData, $countryCode);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramWasInitEvent::class, $events[0]->getPayload());
    }
}
