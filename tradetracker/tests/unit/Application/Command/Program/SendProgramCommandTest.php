<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class SendProgramResultCommandTest.
 */
class SendProgramCommandTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getUuidList()
     *
     * @param UuidInterface $uuid
     */
    public function sendProgramCommandCommandTest(UuidInterface $uuid): void
    {
        $command = new SendProgramCommand($uuid);

        self::assertEquals($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed  $uuid
     * @param string $exceptionClassName
     */
    public function sendProgramResultCommandInvalidUuidTest($uuid, string $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new SendProgramCommand($uuid);
    }
}
