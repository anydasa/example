<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ConvertProgramCommandTest.
 */
class ConvertProgramCommandTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getUuidList()
     *
     * @param UuidInterface $uuid
     */
    public function convertProgramCommandTest(UuidInterface $uuid): void
    {
        $command = new ConvertProgramCommand($uuid);

        self::assertEquals($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed  $uuid
     * @param string $exceptionClassName
     */
    public function convertProgramCommandInvalidUuidTest($uuid, string $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new ConvertProgramCommand($uuid);
    }
}
