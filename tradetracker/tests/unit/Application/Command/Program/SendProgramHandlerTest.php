<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\Program;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasSentEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class SendProgramHandlerTest.
 */
class SendProgramHandlerTest extends AbstractProgramTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     */
    public function sendProgramHandlerShouldFireProgramWasSentEventTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $this->handleInitProgramCommand($uuid, $rawProgramData, $countryCode);
        $this->convertProgramCommand($uuid);

        $command = new SendProgramCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramWasSentEvent::class, $events[2]->getPayload());
    }
}
