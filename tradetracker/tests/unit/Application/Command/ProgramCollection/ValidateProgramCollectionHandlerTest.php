<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ValidateProgramCollectionHandlerTest.
 */
class ValidateProgramCollectionHandlerTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function validateProgramCollectionCommandShouldFireProgramCollectionDataWasValidatedEventTest(UuidInterface $uuid, array $accountConfig): void
    {
        $this->handleInitProgramCollectionCommand($uuid, $accountConfig);
        $this->handleRequestProgramCollectionCommand($uuid);

        $command = new ValidateProgramCollectionCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramCollectionDataWasValidatedEvent::class, $events[2]->getPayload());
    }
}
