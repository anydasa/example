<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AggregateResponseProgramCollectionCommandTest.
 */
class AggregateResponseProgramCollectionCommandTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getUuidList()
     *
     * @param UuidInterface $uuid
     */
    public function aggregateProgramCollectionCommandTest(UuidInterface $uuid): void
    {
        $command = new AggregateProgramCollectionCommand($uuid);

        self::assertEquals($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed $uuid
     * @param mixed $exceptionClassName
     */
    public function aggregateProgramCommandInvalidTypeTest($uuid, $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new AggregateProgramCollectionCommand($uuid);
    }
}
