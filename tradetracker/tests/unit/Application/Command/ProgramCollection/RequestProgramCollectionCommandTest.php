<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class RequestProgramCollectionCommandTest.
 */
class RequestProgramCollectionCommandTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getUuidList()
     *
     * @param UuidInterface $uuid
     */
    public function requestProgramCollectionCommandTest(UuidInterface $uuid): void
    {
        $command = new RequestProgramCollectionCommand($uuid);

        self::assertEquals($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed $uuid
     * @param mixed $exceptionClassName
     */
    public function requestProgramCommandInvalidTypeTest($uuid, $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new RequestProgramCollectionCommand($uuid);
    }
}
