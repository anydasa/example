<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\CommandFactory;
use DataGate\Program\Tradetracker\Tests\Unit\Application\ApplicationTestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AbstractProgramCollectionTestCase.
 */
abstract class AbstractProgramCollectionTestCase extends ApplicationTestCase
{
    /**
     * Command factory service.
     *
     * @var string
     */
    protected $commandFactoryName = CommandFactory::class;

    /**
     * Name for testing command bus.
     *
     * @var string
     */
    protected $commandBusName = 'tactician.commandbus.command.program.collection';

    /**
     * handleStartProgramCollectionCommand.
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    protected function handleInitProgramCollectionCommand(UuidInterface $uuid, array $accountConfig): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::INIT_PROGRAM_COLLECTION_COMMAND,
            $uuid,
            $accountConfig
        );

        $this->handle($command);
    }

    /**
     * handleValidateProgramCollectionCommand.
     *
     * @param UuidInterface $uuid
     */
    protected function handleValidateProgramCollectionCommand(UuidInterface $uuid): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::VALIDATE_PROGRAM_COLLECTION_COMMAND,
            $uuid
        );

        $this->handle($command);
    }

    /**
     * handleRequestProgramCollectionCommand.
     *
     * @param UuidInterface $uuid
     */
    protected function handleRequestProgramCollectionCommand(UuidInterface $uuid): void
    {
        $command = $this->commandFactory->makeCommandInstanceByType(
            CommandFactory::REQUEST_PROGRAM_COLLECTION_COMMAND,
            $uuid
        );

        $this->handle($command);
    }
}
