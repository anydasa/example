<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramCollectionHandlerTest.
 */
class InitProgramCollectionHandlerTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function startProgramCollectionCommandShouldFireProgramCollectionWasStartedEventTest(UuidInterface $uuid, array $accountConfig): void
    {
        $command = new InitProgramCollectionCommand($uuid, $accountConfig);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramCollectionWasInitEvent::class, $events[0]->getPayload());
    }
}
