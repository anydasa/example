<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use Ramsey\Uuid\UuidInterface;

/**
 * Class InitProgramCollectionCommandTest.
 */
class InitProgramCollectionCommandTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid          Afm process unique id
     * @param mixed[]       $accountConfig
     */
    public function initProgramCollectionCommandTest(UuidInterface $uuid, array $accountConfig): void
    {
        $command = new InitProgramCollectionCommand($uuid, $accountConfig);
        $accountConfigDto = AccountConfigDto::fromArray($accountConfig);

        self::assertEquals($uuid, $command->getUuid());
        self::assertEquals($accountConfigDto, $command->getAccountConfig());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\InitProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfigException()
     *
     * @param mixed $uuid
     * @param mixed $accountConfig
     * @param mixed $exceptionClassName
     */
    public function initProgramCommandInvalidTypeTest($uuid, $accountConfig, $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new InitProgramCollectionCommand($uuid, $accountConfig);
    }
}
