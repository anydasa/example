<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AggregateResponseProgramCollectionHandlerTest.
 */
class AggregateResponseProgramCollectionHandlerTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function aggregateResponseProgramCollectionCommandShouldFireProgramCollectionWasAggregatedEvent(UuidInterface $uuid, array $accountConfig): void
    {
        $this->handleInitProgramCollectionCommand($uuid, $accountConfig);
        $this->handleRequestProgramCollectionCommand($uuid);
        $this->handleValidateProgramCollectionCommand($uuid);

        $command = new AggregateProgramCollectionCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramCollectionWasAggregatedEvent::class, $events[3]->getPayload());
    }
}
