<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand;
use Ramsey\Uuid\UuidInterface;

/**
 * Class StartProgramCollectionCommandTest.
 */
class ValidateProgramCollectionCommandTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getUuidList()
     *
     * @param UuidInterface $uuid
     */
    public function validateProgramCollectionCommandTest(UuidInterface $uuid): void
    {
        $command = new ValidateProgramCollectionCommand($uuid);

        self::assertEquals($uuid, $command->getUuid());
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UuidProvider::getInvalidUuidExceptionList()
     *
     * @param mixed $uuid
     * @param mixed $exceptionClassName
     */
    public function validateProgramCommandInvalidTypeTest($uuid, $exceptionClassName): void
    {
        $this->expectException($exceptionClassName);
        new ValidateProgramCollectionCommand($uuid);
    }
}
