<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application\Command\ProgramCollection;

use Broadway\Domain\DomainMessage;
use Broadway\Saga\Testing\EventCollectorListener;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use Ramsey\Uuid\UuidInterface;

/**
 * Class RequestProgramCollectionHandlerTest.
 */
class RequestProgramCollectionHandlerTest extends AbstractProgramCollectionTestCase
{
    /**
     * @test
     *
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommandHandler
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionDataProvider::getAccountConfig()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function requestProgramCollectionCommandShouldFireProgramCollectionResponseContentWasRequestedEventTest(UuidInterface $uuid, array $accountConfig): void
    {
        $this->handleInitProgramCollectionCommand($uuid, $accountConfig);

        $command = new RequestProgramCollectionCommand($uuid);
        $this->handle($command);

        /** @var EventCollectorListener $eventCollector */
        $eventCollector = $this->service(EventCollectorListener::class);

        /** @var DomainMessage[] $events */
        $events = $eventCollector->popEvents();

        self::assertInstanceOf(ProgramCollectionDataWasRequestedEvent::class, $events[1]->getPayload());
    }
}
