<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application;

use AdgoalCommon\Saga\AbstractSaga;
use AdgoalCommon\Saga\Testing\ScenarioTestCase;
use DataGate\Program\Tradetracker\Application\AccountCollectionSaga;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Event\AccountCollectionWasStartedEvent;
use League\Tactician\CommandBus;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AccountCollectionSagaTest.
 */
class AccountCollectionSagaTest extends ScenarioTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\AccountCollectionSaga::handleAccountCollectionWasStartedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\UserAccountDataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $accountConfig
     */
    public function eventProgramWasStartedEventHandleLastStepTest(UuidInterface $uuid, array $accountConfig): void
    {
        $this->scenario
            ->when(new AccountCollectionWasStartedEvent($uuid, $accountConfig));

        $lastState = $this->saga->getLastState();
        self::assertTrue($lastState->isDone());
    }

    /**
     * Create the saga you want to test in this test case.
     *
     * @param CommandBus $commandBus
     *
     * @return AbstractSaga
     */
    protected function createSaga(CommandBus $commandBus): AbstractSaga
    {
        return new AccountCollectionSaga($commandBus);
    }
}
