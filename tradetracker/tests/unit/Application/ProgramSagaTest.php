<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application;

use AdgoalCommon\Base\Infrastructure\Testing\ValueObjectMockTrait;
use AdgoalCommon\Saga\AbstractSaga;
use AdgoalCommon\Saga\Testing\ScenarioTestCase;
use DataGate\Program\Tradetracker\Application\Command\Program\ConvertProgramCommand;
use DataGate\Program\Tradetracker\Application\Command\Program\SendProgramCommand;
use DataGate\Program\Tradetracker\Application\ProgramSaga;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasConvertedEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasInitEvent;
use DataGate\Program\Tradetracker\Domain\Program\Event\ProgramWasSentEvent;
use DataGate\Program\Tradetracker\Domain\Program\ValueObject\RawProgramData;
use League\Tactician\CommandBus;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramSagaTest.
 *
 * @covers \DataGate\Program\Tradetracker\Application\ProgramSaga
 */
class ProgramSagaTest extends ScenarioTestCase
{
    use ValueObjectMockTrait;

    /**
     * Create the saga you want to test in this test case.
     *
     * @param CommandBus $commandBus
     *
     * @return AbstractSaga
     */
    protected function createSaga(CommandBus $commandBus): AbstractSaga
    {
        return new ProgramSaga($commandBus);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramSaga::handleProgramWasInitEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     *
     * @throws \DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException
     */
    public function eventProgramWasInitEventHandleConvertProgramCommandTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $rawProgramDataValueObject = RawProgramData::fromArray($rawProgramData);

        $this->scenario
            ->when(new ProgramWasInitEvent($uuid, $rawProgramDataValueObject, $countryCode))
            ->then([
                new ConvertProgramCommand($uuid),
            ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramSaga::handleProgramWasConvertedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     *
     * @throws \DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException
     */
    public function eventProgramWasConvertedEventHandleSendProgramCommandTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $rawProgramDataValueObject = RawProgramData::fromArray($rawProgramData);

        $this->scenario
            ->given([
                new ProgramWasInitEvent($uuid, $rawProgramDataValueObject, $countryCode),
            ])
            ->when(new ProgramWasConvertedEvent($uuid))
            ->then([
                new SendProgramCommand($uuid),
            ]);
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramSaga::handleProgramWasSentEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramDataProvider::getProgramList
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $rawProgramData
     * @param string        $countryCode
     *
     * @throws \DataGate\Program\Tradetracker\Domain\Program\Exception\InvalidRawProgramDataException
     */
    public function eventProgramWasSentEventHandleLastStepTest(UuidInterface $uuid, array $rawProgramData, string $countryCode): void
    {
        $rawProgramDataValueObject = RawProgramData::fromArray($rawProgramData);

        $this->scenario
            ->given([
                new ProgramWasInitEvent($uuid, $rawProgramDataValueObject, $countryCode),
                new ProgramWasConvertedEvent($uuid),
            ])
            ->when(new ProgramWasSentEvent($uuid))
            ->then([]);

        $lastState = $this->saga->getLastState();
        self::assertTrue($lastState->isDone());
    }
}
