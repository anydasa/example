<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application;

use AdgoalCommon\Base\Application\Factory\CommandFactoryInterface;
use AdgoalCommon\Base\Domain\Exception\InvalidDataException;
use AdgoalCommon\Datagate\Testing\ApplicationTestCase as AbstractApplicationTestCase;
use League\Tactician\CommandBus;

/**
 * Class ApplicationTestCase.
 */
abstract class ApplicationTestCase extends AbstractApplicationTestCase
{
    /**
     * Command factory service.
     *
     * @var string
     */
    protected $commandFactoryName;

    /**
     * @var CommandFactoryInterface
     */
    protected $commandFactory;

    /**
     * Name for testing command bus.
     *
     * @var string
     */
    protected $commandBusName;

    /**
     * @var CommandBus|null
     */
    protected $commandBus;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $commandFactory = $this->service($this->commandFactoryName);

        if (!$commandFactory instanceof CommandFactoryInterface) {
            throw new InvalidDataException(
                '$commandFactory  not instance of ' . CommandFactoryInterface::class
            );
        }
        $this->setCommandFactory($commandFactory);

        $commandBus = $this->service($this->commandBusName);

        if (!$commandBus instanceof CommandBus) {
            throw new InvalidDataException('$commandBus not instance of ' . CommandBus::class);
        }
        $this->setCommandBus($commandBus);
    }

    /**
     * Set CommandBus object.
     *
     * @param CommandFactoryInterface $commandFactory
     *
     * @return ApplicationTestCase
     */
    protected function setCommandFactory(CommandFactoryInterface $commandFactory): self
    {
        $this->commandFactory = $commandFactory;

        return $this;
    }

    /**
     * Set CommandBus object.
     *
     * @param CommandBus $commandBus
     *
     * @return ApplicationTestCase
     */
    protected function setCommandBus(CommandBus $commandBus): self
    {
        $this->commandBus = $commandBus;

        return $this;
    }

    /**
     * Executes the given command and optionally returns a value.
     *
     * @param object $command
     */
    protected function handle(object $command): void
    {
        $this->commandBus->handle($command);
    }
}
