<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Unit\Application;

use AdgoalCommon\Saga\AbstractSaga;
use AdgoalCommon\Saga\Testing\ScenarioTestCase;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\AggregateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\RequestProgramCollectionCommand;
use DataGate\Program\Tradetracker\Application\Command\ProgramCollection\ValidateProgramCollectionCommand;
use DataGate\Program\Tradetracker\Application\ProgramCollectionSaga;
use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasRequestedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionDataWasValidatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasAggregatedEvent;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Event\ProgramCollectionWasInitEvent;
use League\Tactician\CommandBus;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ProgramCollectionSagaTest.
 *
 * @covers       \DataGate\Program\Tradetracker\Application\ProgramCollectionSaga
 */
class ProgramCollectionSagaTest extends ScenarioTestCase
{
    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramCollectionSaga::handleProgramCollectionWasInitEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionSagaDataProvider::eventProgramCollectionWasInitEventHandleRequestProgramCollectionCommandTest()
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     */
    public function eventProgramCollectionWasInitEventHandleRequestProgramCollectionCommandTest(
        UuidInterface $uuid,
        AccountConfigDto $accountConfig
    ): void {
        $this->scenario
            ->when(new ProgramCollectionWasInitEvent($uuid, $accountConfig))
            ->then(
                [
                    new RequestProgramCollectionCommand($uuid),
                ]
            );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramCollectionSaga::handleProgramCollectionDataWasRequestedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionSagaDataProvider::eventProgramCollectionDataWasRequestedEventHandleValidateProgramCollectionCommandTest()
     *
     * @param UuidInterface    $uuid            Afm process unique id
     * @param AccountConfigDto $accountConfig
     * @param mixed[]          $rawResponseData
     */
    public function eventProgramCollectionDataWasRequestedEventHandleValidateProgramCollectionCommandTest(
        UuidInterface $uuid,
        AccountConfigDto $accountConfig,
        array $rawResponseData
    ): void {
        $this->scenario
            ->given(
                [
                    new ProgramCollectionWasInitEvent($uuid, $accountConfig),
                ]
            )
            ->when(new ProgramCollectionDataWasRequestedEvent($uuid, $rawResponseData))
            ->then(
                [
                    new ValidateProgramCollectionCommand($uuid),
                ]
            );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramCollectionSaga::handleProgramCollectionDataWasValidatedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionSagaDataProvider::eventProgramCollectionDataWasValidatedEventHandleAggregateProgramCollectionCommandTest()
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     * @param mixed[]          $rawResponseData
     */
    public function eventProgramCollectionDataWasValidatedEventHandleAggregateProgramCollectionCommandTest(
        UuidInterface $uuid,
        AccountConfigDto $accountConfig,
        array $rawResponseData
    ): void {
        $this->scenario
            ->given(
                [
                    new ProgramCollectionWasInitEvent($uuid, $accountConfig),
                    new ProgramCollectionDataWasRequestedEvent($uuid, $rawResponseData),
                ]
            )
            ->when(new ProgramCollectionDataWasValidatedEvent($uuid))
            ->then(
                [
                    new AggregateProgramCollectionCommand($uuid),
                ]
            );
    }

    /**
     * @test
     *
     * @group        unit
     *
     * @covers       \DataGate\Program\Tradetracker\Application\ProgramCollectionSaga::handleProgramCollectionWasAggregatedEvent
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Unit\DataProvider\ProgramCollectionSagaDataProvider::eventProgramCollectionWasAggregatedEventHandleLastStepTest()
     *
     * @param UuidInterface    $uuid
     * @param AccountConfigDto $accountConfig
     * @param mixed[]          $rawResponseData
     * @param string           $country
     */
    public function eventProgramCollectionWasAggregatedEventHandleLastStepTest(
        UuidInterface $uuid,
        AccountConfigDto $accountConfig,
        array $rawResponseData,
        string $country
    ): void {
        $this->scenario
            ->given(
                [
                    new ProgramCollectionWasInitEvent($uuid, $accountConfig),
                    new ProgramCollectionDataWasRequestedEvent($uuid, $rawResponseData),
                    new ProgramCollectionDataWasValidatedEvent($uuid),
                ]
            )
            ->when(new ProgramCollectionWasAggregatedEvent($uuid, $rawResponseData, $country));

        $lastState = $this->saga->getLastState();
        self::assertTrue($lastState->isDone());
    }

    /**
     * Create the saga you want to test in this test case.
     *
     * @param CommandBus $commandBus
     *
     * @return AbstractSaga
     */
    protected function createSaga(CommandBus $commandBus): AbstractSaga
    {
        return new ProgramCollectionSaga($commandBus, $commandBus);
    }
}
