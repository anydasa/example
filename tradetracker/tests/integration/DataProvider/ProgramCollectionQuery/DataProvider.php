<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Integration\DataProvider\ProgramCollectionQuery;

use Exception;
use Ramsey\Uuid\Uuid;

class DataProvider
{
    /**
     * @return mixed[]
     *
     * @throws Exception
     */
    public function getList(): array
    {
        return [
            [
                Uuid::uuid4(),
                json_decode(file_get_contents(__DIR__ . '/programsList.json'), true),
                [
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '22331',
                        'programName' => '24Option.com - AT',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=22331&m=0&a=137182&r=&u=',
                        'programUrl' => 'https://www.24option.com/de',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/22331.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '26014',
                        'programName' => 'Adzuna.at',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=26014&m=0&a=137182&r=&u=',
                        'programUrl' => 'http://www.adzuna.at',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/26014.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '23127',
                        'programName' => 'A-Klasse Gewinnspiel (non incent) - AT',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=23127&m=0&a=137182&r=&u=',
                        'programUrl' => 'https://a-klasse-gewinnen.de',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/23127.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '26267',
                        'programName' => 'ALDI, Penny oder LIDL - Gewinnspiel',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=26267&m=0&a=137182&r=&u=',
                        'programUrl' => 'https://www.einkaufen-gratis.de/',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/26267.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '18816',
                        'programName' => 'Alensa.at',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=18816&m=0&a=137182&r=&u=',
                        'programUrl' => 'https://www.alensa.at/',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/18816.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '25580',
                        'programName' => 'Alpen Sepp - Alpengenuss',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=25580&m=0&a=137182&r=&u=',
                        'programUrl' => 'https://alpensepp.com',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/25580.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '23057',
                        'programName' => 'Amazon-Gutschein Gewinnspiel (non incent) - AT',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=23057&m=0&a=137182&r=&u=',
                        'programUrl' => 'http://technik-gewinn.de',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/23057.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '23084',
                        'programName' => 'Apple Produkt GWS (non incent) - AT',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=23084&m=0&a=137182&r=&u=',
                        'programUrl' => 'http://dein-wunschpaket.net',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_small/23084.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '20505',
                        'programName' => 'Apps-planet.de',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'http://apps-planet.de/mobil/?tt=20505_0_137182_&r=',
                        'programUrl' => 'http://apps-planet.de/',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_small/20505.png',
                        'supportedCountries' => ['DE'],
                    ],
                    [
                        'networkCode' => 'TRADETRACKER',
                        'programId' => '27516',
                        'programName' => 'Aromapflege.com',
                        'programStatus' => 'active',
                        'relationshipStatus' => 'joined',
                        'shopDeepLink' => 'https://tc.tradetracker.net/?c=27516&m=0&a=137182&r=&u=',
                        'programUrl' => 'http://www.Aromapflege.com',
                        'programLogo' => 'https://cdn.tradetracker.net/at/campaign_image_square/27516.png',
                        'supportedCountries' => ['DE'],
                    ],
                ],
            ],
        ];
    }
}
