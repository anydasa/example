<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Integration\DataProvider\ProgramCollectionQuery;

use DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto;
use DataGate\Program\Tradetracker\Domain\ProgramCollection\Query\ProgramCollectionQueryInterface;
use InvalidArgumentException;
use stdClass;

/**
 * Class ProgramCollectionQuery.
 */
class ProgramCollectionQuery implements ProgramCollectionQueryInterface
{
    /**
     * @param \DataGate\Program\Tradetracker\Domain\AccountCollection\Dto\AccountConfigDto $accountConfig
     *
     * @return stdClass[]
     */
    public function fetchPrograms(AccountConfigDto $accountConfig): array
    {
        $content = file_get_contents(__DIR__ . '/programsList.json');

        if (false === $content) {
            throw new InvalidArgumentException('Can\'t load fake data');
        }

        return json_decode($content, true);
    }
}
