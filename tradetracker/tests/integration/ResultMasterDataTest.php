<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Integration;

use DataGate\Program\Tradetracker\Application\Command\AccountCollection\CommandFactory;
use DataGate\Program\Tradetracker\Infrastructure\Repository\Program\ProgramResultRepository;
use Enqueue\Client\TraceableProducer;
use League\Tactician\CommandBus;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ResultMasterData.
 */
class ResultMasterDataTest extends KernelTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        static::bootKernel();
    }

    /**
     * @group        integration
     *
     * @dataProvider \DataGate\Program\Tradetracker\Tests\Integration\DataProvider\ProgramCollectionQuery\DataProvider::getList()
     *
     * @param UuidInterface $uuid
     * @param mixed[]       $programList
     * @param mixed[]       $expectedDtoResultList
     */
    public function testResultMasterDataTopic(
        UuidInterface $uuid,
        array $programList,
        array $expectedDtoResultList
    ): void {
        $command = $this->getCommandFactory()->makeInitAccountCollectionCommand($uuid);
        $this->getCommandBus()->handle($command);

        $traces = $this->getProducer()->getTopicTraces(ProgramResultRepository::QUEUE_MASTER_DATA);

        $this->assertCount(count($programList), $traces);

        $this->assertEquals($expectedDtoResultList[0], $traces[0]['body']);
        $this->assertEquals($expectedDtoResultList[1], $traces[1]['body']);
        $this->assertEquals($expectedDtoResultList[2], $traces[2]['body']);
        $this->assertEquals($expectedDtoResultList[3], $traces[3]['body']);
        $this->assertEquals($expectedDtoResultList[4], $traces[4]['body']);
        $this->assertEquals($expectedDtoResultList[5], $traces[5]['body']);
        $this->assertEquals($expectedDtoResultList[6], $traces[6]['body']);
        $this->assertEquals($expectedDtoResultList[7], $traces[7]['body']);
        $this->assertEquals($expectedDtoResultList[8], $traces[8]['body']);
        $this->assertEquals($expectedDtoResultList[9], $traces[9]['body']);
    }

    /**
     * @return CommandFactory
     */
    private function getCommandFactory(): CommandFactory
    {
        return static::$kernel->getContainer()->get(CommandFactory::class);
    }

    /**
     * @return CommandBus
     */
    private function getCommandBus(): CommandBus
    {
        return static::$kernel->getContainer()->get('tactician.commandbus.command.account.collection');
    }

    /**
     * @return TraceableProducer
     */
    private function getProducer(): TraceableProducer
    {
        return static::$kernel->getContainer()->get('enqueue.client.masterdata.producer');
    }
}
