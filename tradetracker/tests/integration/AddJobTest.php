<?php

declare(strict_types=1);

namespace DataGate\Program\Tradetracker\Tests\Integration;

use DataGate\Program\Tradetracker\Application\Command\AccountCollection\CommandFactory;
use Enqueue\Client\TraceableProducer;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class AddJobTest.
 */
class AddJobTest extends KernelTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        static::bootKernel();
    }

    /**
     * @group        integration
     *
     * @covers       \DataGate\Program\Tradetracker\Presentation\Cli\Command\AddJobCommand
     */
    public function testAddJob(): void
    {
        $this->runAddJobCommand();

        $traces = $this->getProducer()->getTraces();

        $this->assertCount(1, $traces);
        $this->assertEquals(CommandFactory::ACCOUNT_COLLECTION_INIT_COMMAND, $traces[0]['body']['type']);
    }

    private function runAddJobCommand(): void
    {
        $application = new Application(static::$kernel);

        $command = $application->find('add:job');
        $application->add($command);
        $commandTester = new CommandTester($command);
        $commandTester->execute(['command' => $command->getName()]);
    }

    /**
     * @return TraceableProducer
     */
    private function getProducer(): TraceableProducer
    {
        return static::$kernel->getContainer()->get('enqueue.client.task.producer');
    }
}
